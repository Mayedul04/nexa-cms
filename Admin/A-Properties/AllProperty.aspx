﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllProperty.aspx.vb" Inherits="Admin_A_YourProperty_AllYourProperty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <h1 class="page-title">All By/Sell Properties</h1>

    <div class="btn-toolbar">
        <a href="../A-Properties/AllPropertyCategory.aspx" data-toggle="modal" class="btn btn-primary" style="padding-top: 7PX;  padding-bottom: 7px;"><i class="icon-step-backward"></i> Back</a>

        <button runat="server" id="btnAddNew" class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Your Property"></asp:Label></h2>
    <p>
        <label>Language</label>
        <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true"
            DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang">
        </asp:DropDownList>
        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
    </p>
   
<div>
        <div class="well">

            <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSourceEvents" DataKeyNames="ListID">
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="ListIDLabel" runat="server" Text='<%# Eval("ListID")%>' />
                        </td>
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <asp:Image ID="imgThum" runat="server" Width="100px" ImageUrl='<%#"~/Admin/"+ Eval("SmallImage") %>' />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>

                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" Checked='<%# Eval("Status") %>'
                                Enabled="false" />
                        </td>
                        <td>
                            <a style='<%# If(IsDBNull(Eval("GalleryID")) = false, "display:block", "display:none")%>' href="../A-Gallery/AllGalleryItem.aspx?galleryId=<%# Eval("GalleryID") %>&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>&BigImageWidth=275&BigImageHeight=183"> Gallery</a>

                        </td>


                        <td>
                            <a href="../A-Banner/AllTopBanner.aspx?TName=List_Property&TID=<%# Eval("ListID")%>&t=<%# Server.UrlEncode(Eval("Title").toString()) %>&BigImageWidth=1079&BigImageHeight=434" >Banners</a><br/>
                            <%--<a href="../A-Content/AllContents.aspx?TName=List_Property&TID=<%# Eval("ListID")%>&t=<%# Server.UrlEncode(Eval("Title").toString()) & if(Eval("MasterID")="7","&ImageWidth=215&ImageHeight=171&Title=1&Image=1&Text=1&link=1",if(Eval("MasterID")="6","&ImageWidth=232&ImageHeight=154&Title=1&Image=1&Text=1&link=1", "&ImageWidth=478&ImageHeight=160&Title=0&Image=1&Text=0&link=0")) %>" >Contents</a><br/>--%>
                            <a href="../A-Collage/Collage.aspx?TName=List_Property&TID=<%# Eval("ListID")%>&t=<%# Server.UrlEncode(Eval("Title").toString()) %>" >Image Collage</a><br/>
                            <a href="../A-Download-Files/AllMediaFiles.aspx?TName=List_Property&TID=<%# Eval("ListID")%>&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>" >Download Files</a><br/>
                            <a href="../A-Contact-Address/AllLocations.aspx?TName=List_Property&TID=<%# Eval("ListID")%>&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>" >Quick Contact</a><br />
                            <a href="../A-Testimonial/AllTestimonial.aspx?TName=List_Property&TID=<%# Eval("ListID")%>&image=0&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>" >Testimonial</a>
                        </td>
                        <td>
                            <a href='<%# "Property.aspx?lId=" & Eval("ListID") & "&ParentHTMLMasterID=" & Request.QueryString("ParentHTMLMasterID") %>' title="Edit"><i class="icon-pencil"></i></a>&nbsp
                            
                            <a href='<%# "#" & Eval("ListID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("ListID")%>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">ID
                                </th>

                                <th id="Th3" runat="server">Title
                                </th>
                                <th id="Th4" runat="server">Small Image
                                </th>
                                <th id="Th5" runat="server">Sort Order
                                </th>

                                <th id="Th7" runat="server">Status
                                </th>
                                <th>Gallery
                                </th>
                                <th id="Th9" runat="server">Others
                                </th>
                                <th>Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
            </asp:ListView>


        </div>
    </div>


    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourceEvents" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [List_Property] WHERE [ListID] = @ListID" SelectCommand="SELECT List_Property.*  FROM [List_Property] where Lang=@Lang and ParentHTMLMasterID=@ParentHTMLMasterID order by List_Property.SortIndex">
        <DeleteParameters>
            <asp:Parameter Name="ListID" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
            <asp:QueryStringParameter Name="ParentHTMLMasterID" 
                QueryStringField="ParentHTMLMasterID" />
        </SelectParameters>
    </asp:SqlDataSource>

</asp:Content>

