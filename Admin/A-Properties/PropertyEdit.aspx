﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false" CodeFile="PropertyEdit.aspx.vb" Inherits="Admin_A_YourProperty_YourProperty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Buy/Sell Property</h1>
    <div class="btn-toolbar">
        <a href="../A-Property/AllProperty.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">

            <p>
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>
                    Sub Title:</label>
                <asp:TextBox ID="txtSubTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    
                </label>
            </p>
            <asp:Panel ID="pnlSmallImage" runat="server">
                <p>
                    <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlMediumImage" runat="server">
                <p>
                    <asp:Image ID="imgMediumImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnMediumImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Medium Image : (Width=<%= MediumImageWidth%>; Height=<%= MediumImageHeight%>)</label>
                    <asp:FileUpload ID="fuMediumImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlBigImage" runat="server" Visible="false">
                <p>
                    <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                    <asp:HiddenField ID="hdnBigImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                    <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
                <p>
                    <label>
                        Image Alt Text</label>
                    <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </p>
            </asp:Panel>





            <asp:Panel ID="pnlSmallDetails" runat="server">
                <p>
                    <label>
                        Small Details:</label>
                    <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                        Rows="4"></asp:TextBox>
                    <label class="red">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtSmallDetails" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"
                            runat="server" ControlToValidate="txtSmallDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                            ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlDetails" runat="server">
                <p>
                    <label>
                        Details :</label>
                    <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <script>

                        CKEDITOR.replace('<%=txtDetails.ClientID %>',
                            {
                                filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                "extraPlugins": "imagebrowser",
                                "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                            }
                        );



                    </script>
                </p>
            </asp:Panel>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>

                    <p>
                        <label>
                            Spacial Features
                        </label>
                        <asp:TextBox ID="txtSpecial" Width="400px" TextMode="MultiLine" Rows="2" runat="server"></asp:TextBox>
                        <asp:Button ID="btnSF" runat="server" Text="Add New" />
                    </p>
                    <p>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="100%"
                            DataKeyNames="FeatureID" DataSourceID="sdsFeatured" CssClass="table" 
                            GridLines="None">
                            <Columns>

                                <asp:TemplateField HeaderText="Special Feature">

                                    <ItemTemplate>
                                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Details")%>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtFeature" runat="server" Text='<%# Bind("Details")%>' Width="450"></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Sort Order" SortExpression="SortIndex">
                                    <ItemTemplate>
                                        <asp:Label ID="Label3" runat="server" Text='<%# Bind("SortIndex") %>'></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:TextBox ID="txtSort" runat="server" Text='<%# Bind("SortIndex")%>' Width="50"></asp:TextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status" SortExpression="Status">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("Status") %>' Enabled="false" />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:CheckBox ID="chkStatus" runat="server" Checked='<%# Bind("Status") %>' Enabled="true" />
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Actions
                                    </HeaderTemplate>
                                    <ItemTemplate>

                                        <asp:ImageButton ID="ImageButton1" runat="server" CommandName="Edit" ImageUrl="../assets/images/icons/edit.jpg" />
                                        &nbsp;
                                         <asp:LinkButton ID="linkDelete" CssClass="delete-icon" OnClientClick="if (!window.confirm('Do you want to delete this record?')) return false;"
                                             Text="&lt;img src='../assets/images/icons/delete.png'  alt='Delete' width='14' height='15' border='0'/&gt;"
                                             runat="server" ValidationGroup="Delete" CommandName="Delete"></asp:LinkButton>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                                            Text="Update"></asp:LinkButton>
                                        &nbsp;<asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                                            Text="Cancel"></asp:LinkButton>
                                    </EditItemTemplate>
                                    <ItemStyle CssClass="td-border" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                Sorry,there is no Data found
                            </EmptyDataTemplate>
                            <HeaderStyle BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" />
                            <RowStyle BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="sdsFeatured" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            DeleteCommand="DELETE FROM [SpecialFeatures] WHERE [FeatureID] = @FeatureID"
                            InsertCommand="INSERT INTO [SpecialFeatures] ([TableName], [TableID], [Details], [Status], [SortIndex], [Lang], [Section]) VALUES (@TableName, @TableID, @Details, @Status, @SortIndex, @Lang, @Section)"
                            SelectCommand="SELECT * FROM [SpecialFeatures] where TableName=@TableName and TableID=@TableID"
                            UpdateCommand="UPDATE [SpecialFeatures] SET  [Details] = @Details, [Status] = @Status, [SortIndex] = @SortIndex  WHERE [FeatureID] = @FeatureID">
                            <DeleteParameters>
                                <asp:Parameter Name="FeatureID" Type="Int32" />
                            </DeleteParameters>
                            <InsertParameters>
                                <asp:Parameter Name="TableName" Type="String" DefaultValue="List_Property" />
                                <asp:Parameter Name="TableID" Type="Int32" DefaultValue="-1" />
                                <asp:ControlParameter ControlID="txtSpecial" Name="Details" PropertyName="Text" Type="String" />
                                <asp:Parameter Name="Status" Type="Boolean" DefaultValue="True" />
                                <asp:Parameter Name="SortIndex" Type="Int32" DefaultValue="2" />
                                <asp:Parameter DefaultValue="en" Name="Lang" Type="String" />
                                <asp:Parameter Name="Section" DefaultValue="top" />
                            </InsertParameters>
                            <SelectParameters>
                                <asp:Parameter DefaultValue="List_Property" Name="TableName" />
                                <asp:Parameter DefaultValue="-1" Name="TableID" />
                            </SelectParameters>
                            <UpdateParameters>

                                <asp:Parameter Name="Details" Type="String" />
                                <asp:Parameter Name="Status" Type="Boolean" />
                                <asp:Parameter Name="SortIndex" Type="Int32" />
                                <asp:Parameter Name="FeatureID" Type="Int32" />
                            </UpdateParameters>
                        </asp:SqlDataSource>
                    </p>


                </ContentTemplate>
            </asp:UpdatePanel>
            <asp:Panel ID="pnlMapImage" runat="server" Visible="false">
                <p> 
                    <asp:Image ID="ImgMap" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnMap" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Map Image : (Width= 485; Height=323)</label>
                    <asp:FileUpload ID="fuMap" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
                <p>
                    <label>
                        Google Map Code
                    </label>
                    <asp:TextBox ID="txtMapcode" TextMode="MultiLine" Rows="3" Width="200px" runat="server"></asp:TextBox>
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <p>
                <label>
                    Select Gallery</label>
                <asp:DropDownList ID="ddlGallery" runat="server" AppendDataBoundItems="True" DataSourceID="sdsGallery" DataTextField="Title" DataValueField="GalleryID">
                    <asp:ListItem Value="">Select</asp:ListItem>

                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT [GalleryID], [Title] FROM [Gallery] WHERE (([CategoryID] = @CategoryID) AND ([Status] = @Status) and Lang=@Lang)">
                    <SelectParameters>
                        <asp:Parameter DefaultValue="8" Name="CategoryID" Type="Int32" />
                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        <asp:Parameter DefaultValue="en" Name="Lang" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </p>
            <asp:Panel ID="pnlLink" runat="server">
                <p>
                    <label>
                        Link:</label>
                    <asp:TextBox ID="txtLinkTextBox" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlTopDownloadFiles" runat="server">
                <p>
                    <label>
                        About: </label>
                    <asp:TextBox ID="txtAbout" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <script>

                        CKEDITOR.replace('<%=txtAbout.ClientID %>',
                            {
                                filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                "extraPlugins": "imagebrowser",
                                "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                            }
                        );



                    </script>
                </p>

                <p>
                    <label>View Floor Plans:
                        <asp:Label ID="ltrViewFloorPlans" runat="server" Text="Label"></asp:Label>
                        </label>
                    <asp:FileUpload ID="fuViewFloorPlans" runat="server"  CssClass="input-xlarge" />                
                </p>
                <p>
                    <label>Proximity Map: 
                        <asp:Label ID="ltrProximityMap" runat="server" Text="Label"></asp:Label>
                        </label>
                    <asp:FileUpload ID="fuProximityMap" runat="server" CssClass="input-xlarge"  />                
                </p>
                <p>
                    <label>Brochure: 
                        <asp:Label ID="ltrBrochure" runat="server" Text="Label"></asp:Label>
                        </label>
                    <asp:FileUpload ID="fuBrochure" runat="server" CssClass="input-xlarge"  />                
                </p>
                <p>
                    <label>Location Map: 
                        <asp:Label ID="ltrLocationMap" runat="server" Text="Label"></asp:Label>
                        </label>
                    <asp:FileUpload ID="fuLocationMap" runat="server" CssClass="input-xlarge"  />                
                </p>
                <p>
                    <label>Badge:</label>
                    <asp:TextBox ID="txtBadge" runat="server" CssClass="input-xlarge" ></asp:TextBox>
                </p>
            </asp:Panel>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>
            <asp:Panel ID="pnlFeatured" Visible="false" runat="server">
                <p>
                    <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured " TextAlign="Left" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>
            <p style=" display:none;">
                <label>
                    Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" Enabled="false" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>
        <div class="btn-toolbar">
            <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
            <div class="btn-group">

        <asp:SqlDataSource ID="sdsList" runat="server"
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [List_Property] WHERE [ListID] = @ListID"
            InsertCommand="INSERT INTO List_Property(Title, SubTitle, SmallDetails, BigDetails, SmallImage,MediumImage, BigImage, MapImage, MapCode, Link, Featured, SortIndex, Status, LastUpdated, ImageAltText, MasterID, Lang, GalleryID, ViewFloorPlans, ProximityMap, Brochure, LocationMap, Badge, About,ParentHTMLMasterID) VALUES (@Title, @SubTitle, @SmallDetails, @BigDetails, @SmallImage, @MediumImage ,@BigImage, @MapImage, @MapCode, @Link, @Featured, @SortIndex, @Status, @LastUpdated, @ImageAltText, @MasterID, @Lang, @GalleryID, @ViewFloorPlans, @ProximityMap, @Brochure, @LocationMap, @Badge, @About,@ParentHTMLMasterID)"
            SelectCommand="SELECT * FROM [List_Property]"
            
            UpdateCommand="UPDATE List_Property SET Title = @Title, SubTitle = @SubTitle, SmallDetails = @SmallDetails,MediumImage=@MediumImage, BigDetails = @BigDetails, SmallImage = @SmallImage, BigImage = @BigImage, MapImage = @MapImage, MapCode = @MapCode, Link = @Link, Featured = @Featured, SortIndex = @SortIndex, Status = @Status, LastUpdated = @LastUpdated, ImageAltText = @ImageAltText, GalleryID = @GalleryID, ViewFloorPlans = @ViewFloorPlans, ProximityMap = @ProximityMap, Brochure = @Brochure, LocationMap = @LocationMap, Badge = @Badge, About = @About, ParentHTMLMasterID=@ParentHTMLMasterID  WHERE (ListID = @ListID)">
            <DeleteParameters>
                <asp:Parameter Name="ListID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>

                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="txtSubTitle" Name="SubTitle" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails"
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails"
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage"
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnMediumImage" Name="MediumImage"
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage"
                    PropertyName="Value" Type="String" />

                <asp:ControlParameter ControlID="hdnMap" Name="MapImage" PropertyName="Value" />
                
                <asp:ControlParameter ControlID="txtMapcode" Name="MapCode" PropertyName="Text" />
                <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link"
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="chkFeatured" Name="Featured"
                    PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex"
                    PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status"
                    PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated"
                    PropertyName="Value" Type="DateTime" />

                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />

                <asp:Parameter DefaultValue="en" Name="Lang" />
                
                <asp:ControlParameter ControlID="ddlGallery" Name="GalleryID"
                    PropertyName="SelectedValue" />
                <asp:ControlParameter ControlID="ltrViewFloorPlans" Name="ViewFloorPlans" 
                    PropertyName="Text" />
                <asp:ControlParameter ControlID="ltrProximityMap" Name="ProximityMap" 
                    PropertyName="Text" />
                <asp:ControlParameter ControlID="ltrBrochure" Name="Brochure" 
                    PropertyName="Text" />
                <asp:ControlParameter ControlID="ltrLocationMap" Name="LocationMap" 
                    PropertyName="Text" />
                <asp:ControlParameter ControlID="txtBadge" Name="Badge" PropertyName="Text" />
                <asp:ControlParameter ControlID="txtAbout" Name="About" PropertyName="Text" />

                <asp:QueryStringParameter Name="ParentHTMLMasterID" 
                    QueryStringField="ParentHTMLMasterID" />

            </InsertParameters>
            <UpdateParameters>

                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="txtSubTitle" Name="SubTitle" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails"
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails"
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage"
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnMediumImage" Name="MediumImage"
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage"
                    PropertyName="Value" Type="String" />

                <asp:ControlParameter ControlID="hdnMap" Name="MapImage" PropertyName="Value" />

               
                <asp:ControlParameter ControlID="txtMapcode" Name="MapCode" PropertyName="Text" />

                <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link"
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="chkFeatured" Name="Featured"
                    PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex"
                    PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status"
                    PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated"
                    PropertyName="Value" Type="DateTime" />

                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="ddlGallery" Name="GalleryID" />
                <asp:ControlParameter ControlID="ltrViewFloorPlans" Name="ViewFloorPlans" 
                    PropertyName="Text" />
                <asp:ControlParameter ControlID="ltrProximityMap" Name="ProximityMap" 
                    PropertyName="Text" />
                <asp:ControlParameter ControlID="ltrBrochure" Name="Brochure" 
                    PropertyName="Text" />
                <asp:ControlParameter ControlID="ltrLocationMap" Name="LocationMap" 
                    PropertyName="Text" />
                <asp:ControlParameter ControlID="txtBadge" Name="Badge" PropertyName="Text" />
                <asp:ControlParameter ControlID="txtAbout" Name="About" PropertyName="Text" />
                <asp:QueryStringParameter Name="ListID" QueryStringField="lid"
                    Type="Int32" />

               
                <asp:QueryStringParameter Name="ParentHTMLMasterID" 
                    QueryStringField="ParentHTMLMasterID" />

               
            </UpdateParameters>
        </asp:SqlDataSource>


            </div>
        </div>


    </div>
</asp:Content>
