﻿
Partial Class Admin_A_Properties_AllPropertyCategory
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        HTMLs.MasterIDs = "19"
        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)

        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If
    End Sub

End Class
