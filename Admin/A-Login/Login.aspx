﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Admin_A_Login_Login" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html>
<html lang="en">
<head runat="server">
      <meta charset="utf-8">
    <titleNexa CMS</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../lib/bootstrap/css/bootstrap-responsive.css">
    <link rel="stylesheet" type="text/css" href="../stylesheets/theme.css">
    <link rel="stylesheet" href="../lib/font-awesome/css/font-awesome.css">

    <script src="../lib/jquery-1.8.1.min.js" type="text/javascript"></script>

    <!-- Demo page code -->
    
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .brand { font-family: georgia, serif; }
        .brand .first {
            color: #ccc;
            font-style: italic;
        }
        .brand .second {
            color: #fff;
            font-weight: bold;
            font-family : Arial; 
        }
    </style>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../images/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
 
</head>
<body>
   
 <div class="navbar">
        <div class="navbar-inner">
            <div class="container-fluid">
                <ul class="nav pull-right">
                    
                </ul>
                <a class="brand" href="index.html"><span class="first"><img alt="Nexa Logo" src="../images/logo20.png" /></span> <span class="second">CMS</span></a>
            </div>
        </div>
    </div>
    

    <div class="container-fluid">
        
        <div class="row-fluid">
    <div class="dialog span4">
        <div class="block">
            <div class="block-heading">Sign In</div>
            <div class="block-body">
                <form runat="server" id="form2">

                         <asp:ScriptManager ID="ScriptManager1" runat="server">
                         </asp:ScriptManager>
                    <p><asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic"  runat="server" ValidationGroup="login" ControlToValidate="txtUserID" ErrorMessage="* Required ID"></asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display ="Dynamic"  runat="server" ValidationGroup="login" ControlToValidate="txtPassword" ErrorMessage="* Required Password"></asp:RequiredFieldValidator>
                    </p>

                    <label>Username</label>
                     <asp:TextBox ID="txtUserID" runat="server" CssClass="span12" MaxLength="50"></asp:TextBox>
                    <cc1:TextBoxWatermarkExtender ID="txtUserName_TextBoxWatermarkExtender" runat="server" TargetControlID="txtUserID" WatermarkText="Login ID">
                    </cc1:TextBoxWatermarkExtender>

                    <label>Password</label>
                        <asp:TextBox ID="txtPassword" TextMode="Password" runat="server" CssClass="span12" ></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="txtPassword_TextBoxWatermarkExtender" 
                            runat="server" TargetControlID="txtPassword" WatermarkText="Password">
                        </cc1:TextBoxWatermarkExtender>
                    <%--<a href="index.html" class="btn btn-primary pull-right">Sign In</a--%>
                    <asp:Button ID="btnLogin" runat="server" Text="Sign In" ValidationGroup="login" class="btn btn-primary pull-right"/>

                    <label class="remember-me">
                            <asp:CheckBox ID="chkRemember" runat="server" Text="Remember me" /></label>
                    <div class="clearfix"></div>
                </form>
            </div>
        </div>
        <%--<p class="pull-right" style=""><a href="http://www.portnine.com" target="blank">Theme by Portnine</a></p>--%>
        
        <p><a href="reset-password.html">Forgot your password?</a></p>
    </div>
</div>


    

    

    

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    
    
    
    
    
    
    
  
</body>
</html>
