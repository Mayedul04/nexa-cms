﻿Imports System.Data.SqlClient

Partial Class Admin_UserProfile
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            LoadUser()
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnLastUpdated.Value = DateTime.Now
        lblMessage.Text = ""
        txtEmail.Text = txtEmail.Text.Trim()
        txtUserID.Text = txtUserID.Text.Trim()
        txtUserName.Text = txtUserName.Text.Trim()

        If txtEmail.Text = "" Then
            lblMessage.Text = "Email is empty"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        ElseIf Not New Regex("\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*").IsMatch(txtEmail.Text) Then
            lblMessage.Text = "Email is invalid"
            lblMessage.ForeColor = Drawing.Color.Red
            Exit Sub
        End If

        'if the password is inserted here then save it to hdnPasswort to update the database field
        If txtPassword.Text <> "" Then
            If txtPassword.Text <> txtRetypePassword.Text Then
                lblMessage.Text = "Password not matched"
                lblMessage.ForeColor = Drawing.Color.Red
                Exit Sub
            Else
                hdnPassword.Value = txtPassword.Text
            End If
        End If

        'if a new email is inserted, check whether the email is existed
        If hdhPrevEmail.Value.ToLower() <> txtEmail.Text.Trim().ToLower() Then
            If Not IsEmailExists(txtEmail.Text) Then
                lblMessage.Text = "The email is already exists. Please try another email."
                lblMessage.ForeColor = Drawing.Color.Red
                Exit Sub
            End If
        End If

        Try
            sdsAdminPanelLogin.Update()
            'lblMessage.Text = "Your account has been updated"
            'lblMessage.ForeColor = Drawing.Color.Green
            divSuccess.Visible = True
        Catch ex As Data.SqlClient.SqlException

            'lblMessage.Text = "Error: " & ex.Message
            'lblMessage.ForeColor = Drawing.Color.Red
            divError.Visible = True
        End Try

    End Sub
    Private Function IsEmailExists(ByVal email As String) As Boolean
        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim FResortName = ""
        Dim selectString = "SELECT * from AdminPanelLogin where email=@email "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("email", Data.SqlDbType.VarChar, 100).Value = email
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        Dim retVal As Boolean = False
        retVal = reader.HasRows
        conn.Close()

        Return retVal
    End Function
    Private Sub LoadUser()
        'If Not Request.Cookies("userName") Is Nothing Then
        If Not Request.QueryString("uid") Is Nothing Then
            Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
            conn.Open()
            Dim FResortName = ""
            Dim selectString = "SELECT * from AdminPanelLogin where UserID= @UserID "
            Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
            cmd.Parameters.Add("UserID", Data.SqlDbType.VarChar, 100).Value = Request.QueryString("uid")
            Dim reader As SqlDataReader = cmd.ExecuteReader()
            If reader.HasRows Then
                reader.Read()

                txtEmail.Text = reader("Email") & ""
                hdhPrevEmail.Value = txtEmail.Text
                txtPassword.Text = reader("PS_1") & ""
                hdnPassword.Value = reader("PS_1") & ""
                txtRetypePassword.Text = reader("PS_1") & ""
                txtUserName.Text = reader("Title") & ""
                txtUserID.Text = reader("UN_1") & ""
                hdnCreatedBy.Value = reader("CreatedBy") & ""
                hdnRole.Value = reader("Role") & ""
            End If
            conn.Close()
        Else
            Response.Redirect("Login.aspx")
        End If


    End Sub
End Class
