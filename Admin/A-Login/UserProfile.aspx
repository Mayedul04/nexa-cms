﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="UserProfile.aspx.vb" Inherits="Admin_UserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
            <h1 class="page-title">Profile</h1>
   <div class="btn-toolbar">
    <button runat="server" id ="btnSubmit" ValidationGroup="form"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
    <a href="../A-Login/AllUser.aspx" data-toggle="modal" class="btn">Back</a>
        <div class="btn-group">
    </div>
    </div>

    <h2><asp:Label ID="lblTabTitle" runat="server" Text="Profile"></asp:Label></h2>
   <p>
    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
   </p>
   
   
    <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
   </div>
   <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
                    </span><span class="error-right-bot"></span>
                </div>
         
               
    </div>


    <!-- content -->
    <div class="well">
           <div id="myTabContent" class="tab-content">
            
      

            <p>
                    <label>User Name :</label>
                    <asp:TextBox ID="txtUserName" CssClass="input-xlarge" runat="server" MaxLength="200"></asp:TextBox>
                    <label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" 
                        ControlToValidate="txtUserName" runat="server" ErrorMessage="* Required" 
                        ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </label>
            </p>
            <p>
                    <label>Login ID :</label>
                    <asp:TextBox ID="txtUserID" CssClass="input-xlarge" runat="server" MaxLength="50" 
                        ReadOnly="True"></asp:TextBox>
                    <label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                        ControlToValidate="txtUserID" runat="server" ErrorMessage="* Required" 
                        ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </label>
            </p>
            <p>
                    <label>Password :(Leave it empty if you don't like to change)<asp:HiddenField ID="hdnPassword" runat="server" />
                    </label>
                    &nbsp;<asp:TextBox ID="txtPassword" CssClass="input-xlarge" runat="server" MaxLength="20" 
                        TextMode="Password"></asp:TextBox>
                    
                    
            </p>
            <p>
                    <label>Retype Password :</label>
                    <asp:TextBox ID="txtRetypePassword" CssClass="input-xlarge" runat="server" 
                        MaxLength="20" TextMode="Password"></asp:TextBox>
                    <label>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToCompare="txtPassword" ControlToValidate="txtRetypePassword"  ValidationGroup="form"
                        ErrorMessage="CompareValidator" SetFocusOnError="True">* Not Matched</asp:CompareValidator>
                    </label>
            </p>
            <p>
                    <label>Email :</label>
                    <asp:TextBox ID="txtEmail" CssClass="input-xlarge" runat="server" MaxLength="100"></asp:TextBox>
                    <label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                        ControlToValidate="txtEmail" runat="server" ErrorMessage="* Required" 
                        ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </label>
                    <label>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                        ValidationGroup="form" SetFocusOnError="True"
                         runat="server" ErrorMessage="* Invalid Email" 
                        ControlToValidate="txtEmail" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ></asp:RegularExpressionValidator></label>
            </p>

                     </div> 

    </div>
    <!-- Eof content -->


            <p>
                <asp:HiddenField ID="hdhPrevEmail" runat="server" />
                <asp:HiddenField ID="hdnRole" Value="admin" runat="server" />
                <asp:HiddenField ID="hdnLastUpdated" runat="server" />
                <asp:HiddenField ID="hdnCreatedBy" runat="server" />
                <asp:SqlDataSource ID="sdsAdminPanelLogin" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                    DeleteCommand="DELETE FROM [AdminPanelLogin] WHERE [UserID] = @UserID" 
                    InsertCommand="INSERT INTO [AdminPanelLogin] ([PS_1], [UN_1], [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]) VALUES (@PS_1, @UN_1, @Title, @Email, @Role, @CreationDate, @CreatedBy, @Status)" 
                    SelectCommand="SELECT * FROM [AdminPanelLogin]" 
                    
                    UpdateCommand="UPDATE [AdminPanelLogin] SET [PS_1] = @PS_1,  [Title] = @Title, [Email] = @Email, [Role] = @Role, [CreationDate] = @CreationDate, [CreatedBy] = @CreatedBy, [Status] = @Status WHERE [UN_1] = @UN_1">
                    <DeleteParameters>
                        <asp:Parameter Name="UserID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:Parameter Name="PS_1" Type="String" />
                        <asp:Parameter Name="UN_1" Type="String" />
                        <asp:Parameter Name="Title" Type="String" />
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="Role" Type="String" />
                        <asp:Parameter Name="CreationDate" Type="DateTime" />
                        <asp:Parameter Name="CreatedBy" Type="Int32" />
                        <asp:Parameter Name="Status" Type="Boolean" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="hdnPassword" Name="PS_1" PropertyName="Value" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtUserName" Name="Title" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnRole" Name="Role" PropertyName="Value" 
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnLastUpdated" Name="CreationDate" 
                            PropertyName="Value" Type="DateTime" />
                        <asp:ControlParameter ControlID="hdnCreatedBy" Name="CreatedBy" 
                            PropertyName="Value" Type="String" />
                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                        <asp:ControlParameter ControlID="txtUserID" Name="UN_1" PropertyName="Text" 
                            Type="String" />
                    </UpdateParameters>
                </asp:SqlDataSource>
           </p>
</asp:Content>

