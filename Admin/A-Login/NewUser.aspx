﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="NewUser.aspx.vb" Inherits="Admin_A_Login_NewUser" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

            <h1 class="page-title">Edit User</h1>
            <asp:Label ID="lblMessage" runat="server"></asp:Label>  
<div class="btn-toolbar">
    <button class="btn btn-primary" id ="btnSubmit" runat="server" ><i class="icon-save"></i> Save</button>




   <%-- <a href="#myModal" data-toggle="modal" class="btn">Delete</a>--%>
  <div class="btn-group">
  </div>
</div>
<div class="well">

    <div id="myTabContent" class="tab-content">
    
  
        <label>Username</label>
         <asp:TextBox ID="txtUserName" CssClass="input-xlarge" runat="server" MaxLength="200"></asp:TextBox>
              
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" 
                        ControlToValidate="txtUserName" Display ="Dynamic"  runat="server" ErrorMessage="* Required" 
                        ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
             

        <label>Login ID</label>

         <asp:TextBox ID="txtUserID" CssClass="input-xlarge" runat="server" MaxLength="50"></asp:TextBox>
                   
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" 
                        ControlToValidate="txtUserID" runat="server" Display ="Dynamic"  ErrorMessage="* Required" 
                        ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
              


        <label>Password</label>

         <asp:TextBox ID="txtPassword" CssClass="input-xlarge" runat="server" MaxLength="20" 
                        TextMode="Password"></asp:TextBox>
              
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" 
                        ControlToValidate="txtPassword" Display ="Dynamic"  runat="server" ErrorMessage="* Required" 
                        ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    


         <label>Retype Password</label>
        <asp:TextBox ID="txtRetypePassword" CssClass="input-xlarge" runat="server" 
                        MaxLength="20" TextMode="Password"></asp:TextBox>
              
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" 
                        ControlToValidate="txtRetypePassword" Display ="Dynamic"  runat="server" ErrorMessage="* Required" 
                        ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                   
                    
                    <asp:CompareValidator ID="CompareValidator1" runat="server" 
                        ControlToCompare="txtPassword" Display ="Dynamic"  ControlToValidate="txtRetypePassword"  ValidationGroup="form"
                        ErrorMessage="CompareValidator" SetFocusOnError="True">* Not Matched</asp:CompareValidator>
                 


        <label>Email</label>
         <asp:TextBox ID="txtEmail" CssClass="input-xlarge" runat="server" MaxLength="100"></asp:TextBox>
                   
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" 
                        ControlToValidate="txtEmail" runat="server" ErrorMessage="* Required" 
                        ValidationGroup="form" SetFocusOnError="True"></asp:RequiredFieldValidator>
                   
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" 
                        ValidationGroup="form" SetFocusOnError="True"
                         runat="server" ErrorMessage="* Invalid Email" 
                        ControlToValidate="txtEmail" 
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ></asp:RegularExpressionValidator>

     <asp:HiddenField ID="hdnRole" Value="admin" runat="server" />
                <asp:SqlDataSource ID="sdsAdminPanelLogin" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                    DeleteCommand="DELETE FROM [AdminPanelLogin] WHERE [UserID] = @UserID" 
                    InsertCommand="INSERT INTO [AdminPanelLogin] ([PS_1], [UN_1], [Title], [Email], [Role], [CreationDate], [CreatedBy], [Status]) VALUES (@PS_1, @UN_1, @Title, @Email, @Role, @CreationDate, @CreatedBy, @Status)" 
                    SelectCommand="SELECT * FROM [AdminPanelLogin]" 
                    
                    UpdateCommand="UPDATE [AdminPanelLogin] SET [PS_1] = @PS_1, [UN_1] = @UN_1, [Title] = @Title, [Email] = @Email, [Role] = @Role, [CreationDate] = @CreationDate, [CreatedBy] = @CreatedBy, [Status] = @Status WHERE [UserID] = @UserID">
                    <DeleteParameters>
                        <asp:Parameter Name="UserID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:ControlParameter ControlID="txtPassword" Name="PS_1" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtUserID" Name="UN_1" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtUserName" Name="Title" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="txtEmail" Name="Email" PropertyName="Text" 
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnRole" Name="Role" PropertyName="Value" 
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnLastUpdated" DefaultValue="" 
                            Name="CreationDate" PropertyName="Value" Type="DateTime" />
                        <asp:CookieParameter CookieName="UserName" Name="CreatedBy" Type="String" />
                        <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="PS_1" Type="String" />
                        <asp:Parameter Name="UN_1" Type="String" />
                        <asp:Parameter Name="Title" Type="String" />
                        <asp:Parameter Name="Email" Type="String" />
                        <asp:Parameter Name="Role" Type="String" />
                        <asp:Parameter Name="CreationDate" Type="DateTime" />
                        <asp:Parameter Name="CreatedBy" Type="Int32" />
                        <asp:Parameter Name="Status" Type="Boolean" />
                        <asp:Parameter Name="UserID" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
                <asp:HiddenField ID="hdnLastUpdated" runat="server" />






  
  

  </div>

</div>

<div class="modal small hide fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Delete Confirmation</h3>
  </div>
  <div class="modal-body">
    
    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete the user?</p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    <button class="btn btn-danger" data-dismiss="modal">Delete</button>
  </div>
</div>

</asp:Content>

