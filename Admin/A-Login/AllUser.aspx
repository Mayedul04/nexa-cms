﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllUser.aspx.vb" Inherits="Admin_A_HTML_AllHTML" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

  <h1 class="page-title">All Users</h1>

   <div class="btn-toolbar">
    <button runat="server" id ="btnAddNew"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        <div class="btn-group">
      </div>
     </div>

    <!-- content -->

        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All Users"></asp:Label></h2>
        <div>


            <div class="well">


               <table  class ="table" >
                                        <thead>
                                           <tr>
                                            <th >
                                                #</th>
                                            <th >
                                                Title</th>
                                           
                                             <th >
                                                UID</th>                                    
                                         
                                            <th >
                                                Password</th>

                                            <th >
                                                Email</th>

                                            <th >
                                                Creation Date</th>

                                            <th style="width: 60px;">
                                                </th>
                                           </tr>
                                        </thead>
                                        <tbody>

                <asp:ListView ID="GridView1" runat="server" DataKeyNames="UserID" 
                    DataSourceID="sdsHTML">
                   <EmptyDataTemplate>
                        <table id="Table1" runat="server" style="">
                            <tr>
                                <td>
                                    No data was returned.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr style="">
                            
                            <td>
                                <asp:Label ID="UserIDLabel" runat="server" Text='<%# Eval("UserID") %>' />
                            </td>
                            <td>
                                <asp:Label ID="TitleLabel" runat="server" 
                                    Text='<%# Eval("Title") %>' />
                            </td>
                           <td>
                                <asp:Label ID="lblUID" runat="server" 
                                    Text='<%# Eval("UN_1") %>' />
                           </td>
                           
                           
                            <td>
                                <asp:Label ID="PsLabel" runat="server" Text='<%# Eval("PS_1") %>' />
                            </td>

                               <td>
                                <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>' />
                            </td>

                            <td>
                                <asp:Label ID="lblContactDate" runat="server" Text='<%# Eval("CreationDate") %>' />
                            </td>

                             <td>
                             <a href='<%# "UserProfile.aspx?uid=" & Eval("UserID") %>' title="Edit"><i class="icon-pencil"></i> </a>
                            &nbsp
                             <a href='<%# "#" & Eval("UserID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                <div class="modal small hide fade" id='<%# Eval("UserID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                  </div>
                                  <div class="modal-body">
    
                                    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                     <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                  </div>
                                </div>
                         
                             </td>


                        </tr>
                    </ItemTemplate>
                    <LayoutTemplate>
                 
                                 
                                        <tr ID="itemPlaceholder" runat="server">
                                        </tr>
                         
                              
                                   <div style="" class="paginationNew pull-right ">
                                    <asp:DataPager ID="DataPager1"  PageSize="10"   runat="server">
                                        <Fields>
                                            <asp:NextPreviousPagerField ButtonType="Link"  ShowFirstPageButton="False" ShowNextPageButton="False" ShowPreviousPageButton="True" />
                                            <asp:NumericPagerField />
                                            <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                                        </Fields>
                                    </asp:DataPager>
                                </div>
                              
                    </LayoutTemplate>
               
                </asp:ListView>

                </tbody>
                </table> 


            </div> 
          

          <asp:SqlDataSource ID="sdsHTML" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                SelectCommand="SELECT AdminPanelLogin.* FROM [AdminPanelLogin] order by UN_1"
                 DeleteCommand="delete from AdminPanelLogin where UserID=@UserID" >
                <DeleteParameters>
                    <asp:Parameter Name="UserID" />
                </DeleteParameters>
 
                </asp:SqlDataSource>

        </div>
       


  
    <!-- Eof content -->


</asp:Content>

