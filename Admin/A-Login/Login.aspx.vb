﻿Imports System.Data.SqlClient
Partial Class Admin_A_Login_Login
    Inherits System.Web.UI.Page
    Protected Sub btnLogin_Click(sender As Object, e As System.EventArgs) Handles btnLogin.Click
        Dim userID As String = txtUserID.Text.Trim()
        Dim password As String = txtPassword.Text.Trim()
        If userID.Contains("'") Or String.IsNullOrEmpty(userID) Then
            lblMessage.Text = "User ID or password Invalid"
            Exit Sub
        End If
        If password.Contains("'") Or String.IsNullOrEmpty(password) Then
            lblMessage.Text = "User ID or password Invalid"
            Exit Sub
        End If

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "select * from AdminPanelLogin where UN_1=@userID and PS_1=@password"
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("userID", Data.SqlDbType.VarChar, 50)
        cmd.Parameters.Add("password", Data.SqlDbType.VarChar, 100)
        cmd.Parameters("userID").Value = txtUserID.Text
        cmd.Parameters("password").Value = txtPassword.Text

        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            reader.Read()
            If Request.Cookies("userName") Is Nothing Then
                Response.Cookies.Add(New HttpCookie("userName", txtUserID.Text))
            Else
                Response.Cookies("userName").Value = txtUserID.Text
            End If

            If Request.Cookies("userFullName") Is Nothing Then
                Response.Cookies.Add(New HttpCookie("userFullName", reader("Title").ToString()))
            Else
                Response.Cookies("userFullName").Value = reader("Title").ToString()
            End If

            If Request.Cookies("userpass") Is Nothing Then
                Response.Cookies.Add(New HttpCookie("userpass", New Encription().EncryptTripleDES(txtPassword.Text.Trim(), "*n3x@")))
            Else
                Response.Cookies("userpass").Value = New Encription().EncryptTripleDES(txtPassword.Text.Trim(), "*n3x@")
            End If

            If Not chkRemember.Checked Then
                Response.Cookies("userName").Expires = DateTime.Now.AddDays(1)
                Response.Cookies("userFullName").Expires = DateTime.Now.AddDays(1)
                Response.Cookies("userpass").Expires = DateTime.Now.AddDays(1)
            End If

            conn.Close()
            Response.Redirect("../Default.aspx")
        Else
            lblMessage.Text = "User ID or password Invalid"
            conn.Close()
        End If

    End Sub

End Class
