﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="CommonFacts.aspx.vb" Inherits="Admin_A_CommonGallery_CommonGallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">
        <asp:Label ID="lblTitle" runat="server" Text="Interesting Facts"></asp:Label></h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnBack" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Back</button>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">

            <p>
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <asp:Panel ID="pnlSmallImage" runat="server">
                <p>
                    <label>
                        Small Image (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:Image ID="imgSmallImage" runat="server" />
                </p>



                <p>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label>
                        <asp:RequiredFieldValidator ID="rfvSmallImage" runat="server" Display="Dynamic" ControlToValidate="fuSmallImage"
                            ErrorMessage="* Required"></asp:RequiredFieldValidator>
                    </label>
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
            </asp:Panel>

            <asp:Panel ID="pnlDetails" runat="server">
                <p>
                    <label>
                        Details:</label>
                    <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <script>

                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.

                        CKEDITOR.replace('<%=txtDetails.ClientID %>',
                            {
                                filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                "extraPlugins": "imagebrowser",
                                "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                            }
                        );



                    </script>
                </p>
            </asp:Panel>



            <p>
                <label>
                    Image Alt Text</label>
                <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </p>


            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>

            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>

              <p >
                        <label>
                            Language</label>
                        <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                            DataTextField="LangFullName" DataValueField="Lang">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                        </asp:SqlDataSource>
                    </p>
            <asp:HiddenField ID="hdnTablename" runat="server" />
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>
        <div class="btn-toolbar">
            <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
            <div class="btn-group">
            </div>
        </div>

        <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [List_InterestingFacts] WHERE [ID] = @ID"
            InsertCommand="INSERT INTO [List_InterestingFacts] ([Title], [SmallImage], [BigDetails], [SortIndex], [ImageAltText], [Status], [LastUpdated],[Lang]) VALUES (@Title, @SmallImage,@BigDetails, @SortIndex, @ImageAltText,1, @LastUpdated,@Lang)"
            UpdateCommand="UPDATE [List_InterestingFacts] SET [Title] = @Title, [SmallImage] = @SmallImage, [BigDetails]=@BigDetails, [SortIndex] = @SortIndex, [ImageAltText] = @ImageAltText,  [Status] = @Status, [LastUpdated] = @LastUpdated,Lang=@Lang  WHERE [ID] = @ID">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>

                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage"
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText"
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex"
                    PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status"
                    PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated"
                    PropertyName="Value" Type="DateTime" />
               
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails"
                    PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" 
                    PropertyName="SelectedValue" />
            </InsertParameters>
            <SelectParameters>
            </SelectParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text"
                    Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage"
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText"
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex"
                    PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status"
                    PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated"
                    PropertyName="Value" Type="DateTime" />
            
                <asp:ControlParameter ControlID="txtDetails" Name="BigDetails"
                    PropertyName="Text" Type="String" />
                <asp:QueryStringParameter Name="ID" QueryStringField="cgid"
                    Type="Int32" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" 
                    PropertyName="SelectedValue" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>

</asp:Content>

