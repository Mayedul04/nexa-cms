﻿
Partial Class Admin_MasterPage_signout
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Response.Cookies("userName").Value = ""
        Response.Cookies("userName").Expires = Date.Now.AddDays(-2)
        Response.Cookies("userFullName").Value = ""
        Response.Cookies("userFullName").Expires = Date.Now.AddDays(-2)
        Response.Cookies("userpass").Value = ""
        Response.Cookies("userpass").Expires = Date.Now.AddDays(-2)

        Response.Redirect("../A-Login/Login.aspx")
    End Sub
End Class
