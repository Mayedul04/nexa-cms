﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    ViewStateEncryptionMode="Never" EnableViewStateMac="false" CodeFile="AllEvent.aspx.vb"
    Inherits="AllEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">
        All Event</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary"> <i class="icon-save"></i> Add New</button>
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        <div class="btn-group">
        </div>
    </div>
    <h2><asp:Label ID="lblTabTitle" runat="server" Text="Event List"></asp:Label></h2>
    <div>
        <div class="well">

            <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSourceEvents" DataKeyNames="EventID">
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="ListIDLabel" runat="server" Text='<%# Eval("EventID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <asp:Image ID="imgThum" runat="server" Width="100px" ImageUrl='<%#"~/Admin/"+ Eval("SmallImage") %>' />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>
                        <%--<td>
                            <asp:CheckBox ID="FeaturedCheckBox" runat="server" Checked='<%# Eval("Featured") %>' Enabled="false" />
                        </td>--%>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" Checked='<%# Eval("Status") %>'
                                Enabled="false" />
                        </td>
                        <td>
                            <%--<a href="../A-CommonGallery/AllCommonGallery.aspx?TableName=List_Event&TableMasterID=<%# Eval("MasterID") %>&BigImageWidth=205&BigImageHeight=126&t=<%#  Server.UrlEncode(Eval("Title").ToString()) %>">Add/Edit Images</a>--%>
                            <a href="../A-Download-Files/AllMediaFiles.aspx?TName=List_Event&TID=<%# Eval("EventID")%>&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>" >Download Files<br/></a>
                        </td>
                        <td>
                            <asp:Label ID="MasterIDLabel" runat="server" Text='<%# Eval("MasterID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="LangLabel" runat="server" Text='<%# Eval("Lang") %>' />
                        </td>
                        <td>
                            <a href='<%#  "Events.aspx?eventId=" & Eval("EventID")   %>' title="Edit"><i class="icon-pencil">
                            </i></a>&nbsp
                            <%--<a href='<%# "Events.aspx?eventId=" & Eval("EventID") & "&new=1"  %>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign"></i> </a>&nbsp --%>
                            <a href='<%# "#" & Eval("EventID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("EventID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">
                                    ID
                                </th>
                                
                                <th id="Th3" runat="server">
                                    Title
                                </th>
                                <th id="Th4" runat="server">
                                    Small Image
                                </th>
                                <th id="Th5" runat="server">
                                    Sort Order
                                </th>
                                <%--<th id="Th6" runat="server">
                                    Featured
                                </th>--%>
                                <th id="Th7" runat="server">
                                    Status
                                </th>
                                <th>
                                    Links
                                </th>
                                <th id="Th8" runat="server">
                                    MasterID
                                </th>
                                <th id="Th9" runat="server">
                                    Lang
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
            </asp:ListView>


        </div>
    </div>


    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourceEvents" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [List_Event] WHERE [EventID] = @EventID" SelectCommand="SELECT List_Event.*  FROM [List_Event] INNER JOIN Languages ON List_Event.Lang = Languages.Lang order by List_Event.LastUpdated Desc, List_Event.SortIndex">
        <DeleteParameters>
            <asp:Parameter Name="EventID" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
</asp:Content>
