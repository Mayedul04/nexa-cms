﻿Imports System.Data.SqlClient

Partial Class Admin_A_SEO_SEOEdit
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Request.QueryString("SEOID") <> "" Then
                pnlInsert.Visible = True

                hdnUpdateID.Value = Request.QueryString("SEOID")
                btnSubmit.InnerText = "Update"
                LoadContent(hdnUpdateID.Value)
                lblTabTitle.Text = "Update SEO"
            End If
        End If


    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        Dim retVal As Integer = 0

        hdnLastUpdated.Value = DateTime.Now
        txtSEODescription.Text = Utility.StripTagsRegexCompiled(txtSEODescription.Text)
        txtSEODescription.Text = If(txtSEODescription.Text.Length > 1000, txtSEODescription.Text.Substring(0, 1000), txtSEODescription.Text)

        txtSEOKeyWord.Text = Utility.StripTagsRegexCompiled(txtSEOKeyWord.Text)

        hdnIsRobot.Value = If(rblIsRobot.SelectedIndex = 0, True, False)

        'hdnDetails.Value = FCKeditor1.Value
        If btnSubmit.InnerText = "Update" Then
            'update
            retVal = sdsSEO.Update()
            If retVal > 0 Then
                divSuccess.Visible = True

            Else
                divError.Visible = True
            End If
        Else
            'insert
            retVal = sdsSEO.Insert()
            If retVal > 0 Then
                Response.Redirect("AllSeo.aspx")
            Else
                divError.Visible = True
            End If
        End If
    End Sub


    Protected Sub LoadContent(ByVal updateID As String)

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT [SEOID]  ,[SEOTitle] ,[SEODescription],[SEOKeyWord] ,[FocusKeyword], [SEORobot] ,[PageType] ,[PageID]  FROM [dbo].[SEO] WHERE SEOID=@SEOID "
        Dim cmd As SqlCommand = New SqlCommand(selectString, conn)
        cmd.Parameters.Add("SEOID", Data.SqlDbType.Int)
        cmd.Parameters("SEOID").Value = updateID

        Dim reader As SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            txtTitle.Text = reader("SEOTitle") & ""
            txtSEODescription.Text = reader("SEODescription") & ""
            txtSEOKeyWord.Text = reader("SEOKeyWord") & ""
            hdnIsRobot.Value = reader("SEORobot") & ""
            rblIsRobot.SelectedIndex = If(hdnIsRobot.Value.ToLower() = "true", 0, 1)
            txtPageID.Text = reader("PageID") & ""
            txtPageType.Text = reader("PageType") & ""
            txtFKey.Text = reader("FocusKeyword").ToString()
            hdnPageType.Value = reader("PageType") & ""
            hdnPageID.Value = reader("PageID") & ""

        End If
        conn.Close()
    End Sub




End Class
