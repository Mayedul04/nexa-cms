﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllSEO.aspx.vb" Inherits="Admin_A_SEO_AllSEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
    <h1 class="page-title">SEO</h1>

   <div class="btn-toolbar">
    <button runat="server" id ="btnAddNew"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        <div class="btn-group">
      </div>
     </div>

    <!-- content -->

        <h2>
            <asp:Label ID="lblTabTitle" runat="server" Text="All SEO List"></asp:Label></h2>
        <div>

 
           <div class="well">
           
          <table class="table" >

                                  
                                        <thead  >
                                           <tr>
                                            <th >
                                                #</th>
                                                                                       
                                            <th >
                                                Page Type</th>
                                            <th >
                                                Page ID</th>
                                            
                                            <th >
                                                SEO Title</th>                                    
                                         
                                            <th >
                                                Follow?</th>

                                            <th >
                                                Date </th>

                                            <th style="width: 60px;">
                                               </th>
                                        </tr>
                                            
                                        </thead>
                                        <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="SEOID" 
                    DataSourceID="sdsSEO">
                   <EmptyDataTemplate>
                        <table id="Table1" runat="server" style="">
                            <tr>
                                <td>
                                    No data was returned.</td>
                            </tr>
                        </table>
                    </EmptyDataTemplate>
                    <ItemTemplate>
                        <tr style="">

                            <td>
                                <asp:Label ID="SEOIDLabel" runat="server" Text='<%# Eval("SEOID") %>' />
                            </td>
                            
                            <td>
                                <asp:Label ID="lblPageType" runat="server" Text='<%# Eval("PageType") %>' />
                            </td>
                          
                          <td>
                                <asp:Label ID="lblPageID" runat="server" Text='<%# Eval("PageID") %>' />
                            </td>
                            <td>
                                <asp:Label ID="lblSEOTitle" runat="server" 
                                    Text='<%# Eval("SEOTitle") %>' />
                            </td>
                           
                           
                            <td>
                                <asp:Label ID="lblFollow" runat="server" Text='<%# Eval("SEORobot") %>' />
                            </td>

                            <td>
                                <asp:Label ID="LastUpdatedLabel" runat="server" 
                                    Text='<%# if(isDBNull(Eval("LastUpdated")),"",Convert.ToDateTime(Eval("LastUpdated")).Tostring("MMM dd, yyyy")) %>' />
                            </td>
                           

                             <td>
                             <a href='<%# "SEO.aspx?SEOID=" & Eval("SEOID") %>' title="Edit"><i class="icon-pencil"></i> </a>
                           &nbsp
                            
                                <a href='<%# "#" & Eval("SEOID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                <div class="modal small hide fade" id='<%# Eval("SEOID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                  </div>
                                  <div class="modal-body">
    
                                    <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                  </div>
                                  <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                     <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                  </div>
                                </div>
                         
                             </td>

                        </tr>
                    </ItemTemplate>
                    <LayoutTemplate>
                       
                                        <tr ID="itemPlaceholder"  runat="server">
                                       
                                        </tr>

                                       <div class="paginationNew pull-right">
                                        <asp:DataPager ID="DataPager1"  PageSize="100"  runat="server">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link"  ShowFirstPageButton="False" ShowNextPageButton="False"  ShowPreviousPageButton="True"  />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                                                
                                            </Fields>
                                        </asp:DataPager>
                                        </div>
                               
                    </LayoutTemplate>
               
                </asp:ListView>

                </tbody> 
                </table> 

                
          </div> 
    <!-- Eof content -->
         <asp:SqlDataSource ID="sdsSEO" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT * FROM [SEO] order by PageType" DeleteCommand="DELETE FROM SEO WHERE SEOID=@SEOID">
                <DeleteParameters>
                    <asp:Parameter Name="SEOID" Type="Int32" />
                </DeleteParameters>
            </asp:SqlDataSource>

    </div>
    <!-- Eof content -->

</asp:Content>

