﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="SEO.aspx.vb" Inherits="Admin_A_SEO_SEO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        SEO</h1>
    <div class="btn-toolbar">
        <a href="../A-SEO/AllSEO.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add SEO"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <asp:HiddenField ID="hdnUpdateID" runat="server" />
            <asp:HiddenField ID="hdnLastUpdated" runat="server" />
            <p>
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="100"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtTitle"
                        runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>
                    Focus KeyWord:</label>
                <asp:TextBox ID="txtFKey" runat="server" CssClass="input-xlarge" MaxLength="100"></asp:TextBox>
               
            </p>
            <p>
                <label>
                    Description:</label>
                <asp:TextBox ID="txtSEODescription" runat="server" CssClass="txtarea" Rows="4" TextMode="MultiLine"></asp:TextBox>
                <label class="red">
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSEODescription"
                        ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$" ErrorMessage="* character limit is 1000"
                        ValidationGroup="vendor" SetFocusOnError="True"></asp:RegularExpressionValidator>
                   <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="form" ControlToValidate="txtSEODescription"
                        runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>--%>
                </label>
            </p>
            <p>
                <label>
                    Key Words:</label>
                <asp:TextBox ID="txtSEOKeyWord" runat="server" CssClass="txtarea" Rows="4" TextMode="MultiLine"></asp:TextBox>
                <label class="red">
                </label>
            </p>
            <p>
                <%--<asp:CheckBox ID="chkIsRobot" runat="server" Text="Follow" />--%>
                <asp:RadioButtonList ID="rblIsRobot" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem>Index</asp:ListItem>
                    <asp:ListItem>No Index</asp:ListItem>
                </asp:RadioButtonList>
                <asp:HiddenField ID="hdnIsRobot" runat="server" />
            </p>
            <asp:HiddenField ID="hdnPageType" runat="server" />
            <asp:HiddenField ID="hdnPageID" runat="server" />
            <asp:Panel ID="pnlInsert" runat="server">
                <p>
                    <label>
                        Page Type:</label>
                    <asp:TextBox ID="txtPageType" runat="server" CssClass="input-xlarge" MaxLength="50"></asp:TextBox>
                </p>
                <p>
                    <label>
                        Page ID:</label>
                    <asp:TextBox ID="txtPageID" runat="server" CssClass="input-xlarge" MaxLength="100"></asp:TextBox>
                    <label class="red">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ValidationGroup="form" ControlToValidate="txtPageID"
                            runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                    </label>
                </p>
            </asp:Panel>
            <p>
                <asp:SqlDataSource ID="sdsSEO" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    DeleteCommand="DELETE FROM [SEO] WHERE [SEOID] = @SEOID" InsertCommand="INSERT INTO SEO(SEOTitle, SEODescription, SEOKeyWord, SEORobot, PageType, PageID, LastUpdated, FocusKeyword) VALUES (@SEOTitle, @SEODescription, @SEOKeyWord, @SEORobot, @PageType, @PageID, @LastUpdated,@FocusKeyword)"
                    SelectCommand="SELECT * FROM [SEO]  order by PageType" UpdateCommand="UPDATE SEO SET SEOTitle = @SEOTitle, SEODescription = @SEODescription, SEOKeyWord = @SEOKeyWord, SEORobot = @SEORobot, PageType = @PageType, PageID = @PageID, LastUpdated = @LastUpdated, FocusKeyword=@FocusKeyword WHERE (SEOID = @SEOID)">
                    <DeleteParameters>
                        <asp:Parameter Name="SEOID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:ControlParameter ControlID="txtTitle" Name="SEOTitle" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtFKey" Name="FocusKeyword" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtSEODescription" Name="SEODescription" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtSEOKeyWord" Name="SEOKeyWord" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnIsRobot" Name="SEORobot" PropertyName="Value"
                            Type="Boolean" />
                        <asp:ControlParameter ControlID="txtPageType" Name="PageType" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtPageID" Name="PageID" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="hdnLastUpdated" Name="LastUpdated" PropertyName="Value" />
                    </InsertParameters>
                    <SelectParameters>
                    </SelectParameters>
                    <UpdateParameters>
                        <asp:ControlParameter ControlID="txtTitle" Name="SEOTitle" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtFKey" Name="FocusKeyword" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtSEODescription" Name="SEODescription" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtSEOKeyWord" Name="SEOKeyWord" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnIsRobot" Name="SEORobot" PropertyName="Value"
                            Type="Boolean" />
                        <asp:ControlParameter ControlID="txtPageType" Name="PageType" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtPageID" Name="PageID" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="hdnLastUpdated" Name="LastUpdated" PropertyName="Value" />
                        <asp:ControlParameter ControlID="hdnUpdateID" Name="SEOID" PropertyName="Value" Type="Int32" />
                    </UpdateParameters>
                </asp:SqlDataSource>
            </p>
        </div>
        <div class="btn-toolbar">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                <i class="icon-save"></i>Add New</button>
            <div class="btn-group">
            </div>
        </div>
    </div>
    <!-- Eof content -->
</asp:Content>
