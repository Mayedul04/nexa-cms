﻿
Partial Class Admin_A_Contact_AllContact
    Inherits System.Web.UI.Page

    Protected Sub btnDownload_Click(sender As Object, e As System.EventArgs) Handles btnDownload.ServerClick
        Utility.DownloadCSV(Utility.EncodeTitle("Contact " & DateTime.Now, "-") & ".csv", sdsContact.SelectCommand, Response)
    End Sub
End Class
