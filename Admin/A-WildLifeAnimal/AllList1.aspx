﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllList1.aspx.vb" Inherits="Admin_A_List1_AllList1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        </h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i> Add New</button>
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="Label1" runat="server" Text="All Animals Category"></asp:Label></h2>
    <div>
         <p >
            <label>
                Language</label>
            <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true"
                DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang">
            </asp:DropDownList>
            <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
            </asp:SqlDataSource>
        </p>
        <div class="well">
            <asp:ListView ID="ListView1" runat="server" DataSourceID="sdsList1" DataKeyNames="ListID">
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="ListIDLabel" runat="server" Text='<%# Eval("ListID") %>' />
                        </td>
                        
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <img width="120px" src='<%# "../" & Eval("BigImage") %>' />
                        </td>
                        <%--<td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>--%>
                        <%--<td>
                            <asp:CheckBox ID="FeaturedCheckBox" runat="server" Checked='<%# Eval("Featured") %>'
                                Enabled="false" />
                        </td>--%>
                        <%--<td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" Checked='<%# Eval("Status") %>'
                                Enabled="false" />
                        </td>--%>
                        <td>
                            <a style =<%# If(IsDBNull(Eval("GalleryID")) = false, "display:block", "display:none")%> href="../A-Gallery/AllGalleryItem.aspx?galleryId=<%# Eval("GalleryID") %>">Image Gallery</a>
                            <%--<a href="AllCommonGallery.aspx?TableName=Animal&TableMasterID=<%# Eval("MasterID") %>&t=<%# Server.UrlEncode(Eval("Title").ToString()) & "&lang=" & Eval("Lang") %>&BigImageWidth=841&BigImageHeight=254">Image Gallery</a>--%>
                        </td>

                        <td>
                            
                            <a style =<%# If(IsDBNull(Eval("VGalleryID")) = false, "display:block", "display:none")%> href="../A-Video/AllGalleryItem.aspx?galleryId=<%# Eval("VGalleryID") %>">Video Gallery</a>
                            <%--<a href="AllCommonVideo.aspx?TableName=Animal&TableMasterID=<%# Eval("MasterID") %>&SmallImageWidth=478&SmallImageHeight=233&t=<%# Server.UrlEncode(Eval("Title").ToString()) & "&lang=" & Eval("Lang") %>">Video Gallery</a>--%>
                        </td>

                        <%--<td>
                            <a href="AllCommonFacts.aspx?TableName=Animal&TableMasterID=<%# Eval("MasterID") %>&SmallImageWidth=478&SmallImageHeight=233&t=<%# Server.UrlEncode(Eval("Title").ToString()) & "&lang=" & Eval("Lang") %>">Interesting Facts</a>
                        </td>

                        <td>
                            <a href="AllTestimonial.aspx?TableName=Animal&TableMasterID=<%# Eval("MasterID") %>&SmallImageWidth=478&SmallImageHeight=233&t=<%# Server.UrlEncode(Eval("Title").ToString()) & "&lang=" & Eval("Lang") %>">Testimonial</a>
                        </td>--%>

                        <td><a href='<%# "AllList1Child.aspx?l1id=" & Eval("ListID") & "&lang=" & Eval("Lang") %>' title="Products">Animals </a></td>
                        <td>
                            <asp:Label ID="MasterIDLabel" runat="server" Text='<%# Eval("MasterID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="LangLabel" runat="server" Text='<%# Eval("Lang") %>' />
                        </td>
                        <td>
                            <a href='<%#  "List1.aspx?l1id=" & Eval("ListID") %>' title="Edit"><i class="icon-pencil">
                            </i></a>&nbsp
                            <a href='<%# "List1.aspx?l1id=" & Eval("ListID") & "&new=1" %>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign"></i> </a>&nbsp 
                            <a href='<%# "#" & Eval("ListID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("ListID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">
                                    ID
                                </th>
                                
                                <th id="Th3" runat="server">
                                    Title
                                </th>
                                <th id="Th4" runat="server">
                                    Image
                                </th>
                                <%--<th id="Th5" runat="server">
                                    SortIndex
                                </th>--%>
                               <%-- <th id="Th6" runat="server">
                                    Featured
                                </th>--%>
                                <%--<th id="Th7" runat="server">
                                    Status
                                </th>--%>
                                <th>
                                    Images Gallery
                                </th>
                                 <th>
                                    Video Gallery
                                </th>
                                <%--<th>Interesting Facts</th>

                                <th>Testimonial</th>--%>


                                <th>Animal List</th>
                                <th id="Th8" runat="server">
                                    MasterID
                                </th>
                                <th id="Th9" runat="server">
                                    Lang
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
            </asp:ListView>
            <asp:SqlDataSource ID="sdsList1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                SelectCommand="SELECT List1.ListID, List1.Title, List1.SmallDetails, List1.BigDetails, List1.SmallImage, List1.BigImage, List1.Link, List1.Featured, List1.SortIndex, List1.Status, List1.LastUpdated,List1.MasterID, List1.Lang , List1.GalleryID, List1.VGalleryID  FROM List1  INNER JOIN Languages ON List1.Lang = Languages.Lang where List1.Lang = @Lang  ORDER BY List1.MasterID DESC, Languages.SortIndex " 
                DeleteCommand="DELETE FROM List1 WHERE (ListID = @ListID)">
                <DeleteParameters>
                    <asp:Parameter Name="ListID" />
                </DeleteParameters>
                   <SelectParameters>
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue"
                    DefaultValue="en" />
            </SelectParameters>
            </asp:SqlDataSource>
        </div>
    </div>
</asp:Content>

