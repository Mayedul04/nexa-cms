﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false" CodeFile="CommonVideoEdit.aspx.vb" Inherits="Admin_A_CommonGallery_CommonGalleryEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="block">
    <div class="block-heading">Common Video Gallery</div>
    <div class="block-body">

            <h1 class="page-title">
            <asp:Label ID="lblTitle" runat="server" Text="Common Gallery"></asp:Label>
       </h1>
    <%--<div class="btn-toolbar">
        <a href="../A-CommonGallery/AllCommonGallery.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>--%>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
          <div id="myTabContent" class="tab-content">
            
            <p>
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <asp:Panel ID="pnlSmallImage" Visible="True"  runat="server">
                <p>
                    <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlBigImage" Visible ="false"  runat="server">
                <p>
                    <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                    <asp:HiddenField ID="hdnBigImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                    <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
                <p>
                    <label>
                        Image Alt Text</label>
                    <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </p>
            </asp:Panel>
            
                <p>
                 <asp:Image ID="imgVideoImageURL" runat="server" Width="100px"/>
                <label>
                   Video:(Vimeo/Youtube)(Width=<%= videoWidth%>; Height=<%= videoHeight%>)
                </label>
                 <asp:TextBox ID="txtVideoOriginalURL" runat="server" CssClass="txt"></asp:TextBox>
                    
                    <asp:HiddenField ID="hdnVideoEmbedCode" runat="server" />
                    <asp:HiddenField ID="hdnVideoImageURL" runat="server" />
                 <label class="red">
                    <asp:RegularExpressionValidator Display="Dynamic"  ID="RegularExpressionValidator2" 
                        ValidationGroup="form" ControlToValidate="txtVideoOriginalURL" 
                        SetFocusOnError="true" runat="server" ErrorMessage="* Invalid URL" 
                        ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>
                </label>
    
            </p>


            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>
            
            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>
            <asp:HiddenField ID="hdnTablename" runat="server" />
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>
        <div class="btn-toolbar">
            <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
            <button runat="server" id ="btnSubmit" ValidationGroup="form"  class="btn btn-primary" ><i class="icon-save"></i> Add New</button>
            <div class="btn-group">
            </div>
        </div>

        <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        
            DeleteCommand="DELETE FROM [CommonGallery] WHERE [CommonGalleryID] = @CommonGalleryID" 
            InsertCommand="INSERT INTO [CommonGallery] ([Title], [SmallImage], [BigImage], [ImageAltText], [Status], [LastUpdated], [TableName], [TableMasterID],[VideoOriginalURL], [VideoImageURL], [VideoEmbedCode]) VALUES (@Title, @SmallImage, @BigImage, @ImageAltText,1, @LastUpdated, @TableName, @TableMasterID,@VideoOriginalURL, @VideoImageURL, @VideoEmbedCode)" 
            UpdateCommand="UPDATE [CommonGallery] SET [Title] = @Title, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [ImageAltText] = @ImageAltText,  [Status] = @Status, [LastUpdated] = @LastUpdated, [TableName] = @TableName, [TableMasterID] = @TableMasterID,[VideoOriginalURL]=@VideoOriginalURL, [VideoImageURL]=@VideoImageURL, [VideoEmbedCode]=@VideoEmbedCode  WHERE [CommonGalleryID] = @CommonGalleryID">
            <DeleteParameters>
                <asp:Parameter Name="CommonGalleryID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                    
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                    Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" 
                    PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" 
                    PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                    PropertyName="Value" Type="DateTime" />
                <asp:ControlParameter ControlID="hdnTablename" Name="TableName" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="TableMasterID" 
                    PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="txtVideoOriginalURL" Name="VideoOriginalURL" 
                                PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnVideoImageURL" Name="VideoImageURL" 
                                PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnVideoEmbedCode" Name="VideoEmbedCode" 
                                PropertyName="Value" Type="String" />
                    
            </InsertParameters>
            <SelectParameters>
            </SelectParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                    Type="String" />
                <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" 
                    PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" 
                    PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" 
                    PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                    PropertyName="Value" Type="DateTime" />
                <asp:ControlParameter ControlID="hdnTablename" Name="TableName" 
                    PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnMasterID" Name="TableMasterID" 
                    PropertyName="Value" Type="Int32" />
                  <asp:ControlParameter ControlID="txtVideoOriginalURL" Name="VideoOriginalURL" 
                                PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="hdnVideoImageURL" Name="VideoImageURL" 
                                PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnVideoEmbedCode" Name="VideoEmbedCode" 
                                PropertyName="Value" Type="String" />
                <asp:QueryStringParameter Name="CommonGalleryID" QueryStringField="cgid" 
                    Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>

    </div>

    </div>
</div>

</asp:Content>

