﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false"
    CodeFile="List1ChildEdit.aspx.vb" Inherits="Admin_A_List1_List1Child" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="block">
        <div class="block-heading">Product</div>
        <div class="block-body">
            <h1 class="page-title">Animal</h1>
            <div class="btn-toolbar">
                <%--<button runat="server" id="btnBack" class="btn btn-primary">
            Back</button>--%>
                <div class="btn-group">
                </div>
            </div>
            <h2>
                <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
            <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
            </div>
            <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
                </div>
            </div>
            <!-- content -->
            <div class="well">
                <div id="myTabContent" class="tab-content">
                    <p style="display: none">
                        <label>
                            Style Group</label>
                        <asp:DropDownList ID="ddlCategory" runat="server" AppendDataBoundItems="true" DataSourceID="sdsCollection" DataTextField="Title" DataValueField="ListID">
                            <asp:ListItem Text="Select" Value=""></asp:ListItem>
                        </asp:DropDownList>

                        <asp:SqlDataSource ID="sdsCollection" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" SelectCommand="SELECT DISTINCT [ListID], [Title] FROM [List]"></asp:SqlDataSource>
                    </p>
                    <p>
                        <label>
                            Title:</label>
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                        <label class="red">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                                ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                        </label>
                    </p>
                    <asp:Panel ID="pnlSmallImage" runat="server">
                        <p>
                            <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                            <asp:HiddenField ID="hdnSmallImage" runat="server" />
                        </p>
                        <p>
                            <label>
                                Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                            <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                            <label class="red">
                            </label>
                        </p>
                        <p>
                            <label>
                                Quick Facts:</label>
                            <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <script>

                                // Replace the <textarea id="editor1"> with a CKEditor
                                // instance, using default configuration.

                                CKEDITOR.replace('<%=txtSmallDetails.ClientID%>',
                                    {
                                        filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                        "extraPlugins": "imagebrowser",
                                        "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                                    }
                        );



                            </script>

                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlSound" runat="server">
                        <p>
                            <asp:Literal ID="ltSound" runat="server"></asp:Literal>

                            <asp:HiddenField ID="hdnAnimalSound" runat="server" />
                        </p>
                        <p>
                            <label>
                                Animal Sound :
                            </label>
                            <asp:FileUpload ID="fuAnimalSound" runat="server" CssClass="input-xlarge" />
                            <label class="red">
                            </label>
                        </p>
                        <p>
                            <asp:CheckBox ID="chlDelSound" runat="server" TextAlign="Left" Text="Delete Sound " />
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlBigImage" runat="server">
                        <p>
                            <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                            <asp:HiddenField ID="hdnBigImage" runat="server" />
                        </p>
                        <p>
                            <label>
                                Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                            <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                            <label class="red">
                            </label>
                        </p>
                        <p>
                            <label>
                                Image Alt Text</label>
                            <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                        </p>
                        <p>
                            <label>
                                Details:</label>
                            <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <script>

                                // Replace the <textarea id="editor1"> with a CKEditor
                                // instance, using default configuration.

                                CKEDITOR.replace('<%=txtDetails.ClientID %>',
                                    {
                                        filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                        "extraPlugins": "imagebrowser",
                                        "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                                    }
                        );



                            </script>
                        </p>
                    </asp:Panel>



                    <asp:Panel ID="pnlLink" Visible="false" runat="server">
                        <p>
                            <label>
                                BG Class:</label>
                            <asp:TextBox ID="txtLinkTextBox" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />
                        </p>
                    </asp:Panel>
                    <p>
                        <label>
                            Sort Order:
                        </label>
                        <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                        <label class="red">
                            <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                                MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                                ValidationGroup="form"></asp:RangeValidator>
                        </label>
                    </p>
                    <asp:Panel ID="pnlFeatured" Visible="false" runat="server">
                        <p>
                            <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured " TextAlign="Left" />
                            <label class="red">
                            </label>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlPGallery" Visible="false" runat="server">
                        <p>
                            <label>Photo Gallery Image (Width=165; Height=225)</label>
                            <asp:Image ID="ImgPGal" runat="server" />
                        </p>
                        <p>
                            <asp:FileUpload ID="fuPGal" runat="server" />
                            <asp:HiddenField ID="hdnPGal" runat="server" />
                        </p>
                        <p>
                            <label>
                                Select Photo Gallery 
                            </label>
                            <asp:DropDownList ID="ddlPGallery" runat="server" DataSourceID="sdsPGallery" AppendDataBoundItems="true" CssClass="input-xlarge"
                                DataTextField="Title" DataValueField="GalleryID">
                                <asp:ListItem Value="">Select Album</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sdsPGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                SelectCommand="SELECT DISTINCT [GalleryID], [Title] FROM [Gallery] WHERE (([Status] = @Status) AND ([Lang] = @Lang)) order by Title ASC">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    <asp:Parameter DefaultValue="en" Name="Lang" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </p>



                    </asp:Panel>
                    <asp:Panel ID="pnlVGallery" Visible="false" runat="server">
                        <p>
                            <label>Video Gallery Image (Width=165; Height=225)</label>
                            <asp:Image ID="ImgVGal" runat="server" />
                        </p>
                        <p>
                            <asp:FileUpload ID="fuVGallery" runat="server" />
                            <asp:HiddenField ID="hdnVGal" runat="server" />
                        </p>
                        <p>
                            <label>
                                Select Video Gallery 
                            </label>
                            <asp:DropDownList ID="ddlVGallery" runat="server" DataSourceID="sdsVGallery" AppendDataBoundItems="true" CssClass="input-xlarge"
                                DataTextField="Title" DataValueField="GalleryID">
                                <asp:ListItem Value="">Select Album</asp:ListItem>
                            </asp:DropDownList>
                            <asp:SqlDataSource ID="sdsVGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                SelectCommand="SELECT DISTINCT [GalleryID], [Title] FROM [Video] WHERE (([Status] = @Status) AND ([Lang] = @Lang)) order by Title ASC">
                                <SelectParameters>
                                    <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                                    <asp:Parameter DefaultValue="en" Name="Lang" Type="String" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </p>
                    </asp:Panel>
                    <p>
                        <asp:CheckBox ID="chkNewArrival" Visible="false" Checked="true" runat="server" TextAlign="Left" Text="New Arrival? " />
                        <label class="red">
                        </label>
                    </p>
                    <p>
                        <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                        <label class="red">
                        </label>
                    </p>

                    <p style="display: none;">
                        <label>
                            Language</label>
                        <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                            DataTextField="LangFullName" DataValueField="Lang">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
                    </p>
                    <asp:HiddenField ID="hdnMasterID" runat="server" />
                    <asp:HiddenField ID="hdnUpdateDate" runat="server" />
                </div>
                <div class="btn-toolbar">
                    <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                        <i class="icon-save"></i>Add New</button>
                    <div class="btn-group">
                    </div>
                </div>
                <asp:SqlDataSource ID="sdsList1_Child" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    DeleteCommand="DELETE FROM [List1_Child] WHERE [ChildID] = @ChildID" InsertCommand="INSERT INTO [List1_Child] ( ListID, [StyleID], [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [Link], [Featured], [SortIndex], [Status], [LastUpdated],ImageAltText, MasterID, Lang, NewArrival,Sound , GalleryID, VGalleryID, PGalleryImage, VGalleryImage) VALUES ( @ListID, @StyleID, @Title, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @Link, @Featured, @SortIndex, @Status, @LastUpdated,@ImageAltText, @MasterID, @Lang, @NewArrival,@Sound, GalleryID, VGalleryID, PGalleryImage, VGalleryImage)"
                    SelectCommand="SELECT * FROM [List1_Child]" UpdateCommand="UPDATE [List1_Child] SET ListID=@ListID, [StyleID]=@StyleID, [Title] = @Title, [SmallDetails] = @SmallDetails, [BigDetails] = @BigDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [Link] = @Link, [Featured] = @Featured, [SortIndex] = @SortIndex, [Status] = @Status, [LastUpdated] = @LastUpdated,ImageAltText=@ImageAltText, MasterID=@MasterID,Lang=@Lang, NewArrival=@NewArrival,Sound=@Sound, GalleryID=@GalleryID, VGalleryID=@VGalleryID, PGalleryImage=@PGalleryImage, VGalleryImage=@VGalleryImage  WHERE [ChildID] = @ChildID">
                    <DeleteParameters>
                        <asp:Parameter Name="ChildID" Type="Int32" />
                    </DeleteParameters>
                    <InsertParameters>
                        <asp:QueryStringParameter Name="ListID" QueryStringField="l1id" Type="Int32" />
                        <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnAnimalSound" Name="Sound" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                            Type="Boolean" />
                        <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                            Type="Boolean" />
                        <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value"
                            Type="DateTime" />
                        <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                        <asp:QueryStringParameter Name="Lang" QueryStringField="Lang" />
                        <asp:ControlParameter ControlID="chkNewArrival" Name="NewArrival" PropertyName="Checked" />
                        <asp:ControlParameter ControlID="ddlCategory" Name="StyleID" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlPGallery" Name="GalleryID" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlVGallery" Name="VGalleryID" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="hdnPGal" Name="PGalleryImage" PropertyName="Value" />
                        <asp:ControlParameter ControlID="hdnVGal" Name="VGalleryImage" PropertyName="Value" />
                    </InsertParameters>
                    <UpdateParameters>
                        <asp:QueryStringParameter Name="ListID" QueryStringField="l1id" Type="Int32" />
                        <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                        <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="hdnAnimalSound" Name="Sound" PropertyName="Value"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link" PropertyName="Text"
                            Type="String" />
                        <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                            Type="Boolean" />
                        <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                            Type="Int32" />
                        <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                            Type="Boolean" />
                        <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value"
                            Type="DateTime" />
                        <asp:QueryStringParameter Name="ChildID" QueryStringField="l1cid" Type="Int32" />
                        <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                        <asp:QueryStringParameter Name="Lang" QueryStringField="Lang" />
                        <asp:ControlParameter ControlID="chkNewArrival" Name="NewArrival" PropertyName="Checked" />
                        <asp:ControlParameter ControlID="ddlCategory" Name="StyleID" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlPGallery" Name="GalleryID" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="ddlVGallery" Name="VGalleryID" PropertyName="SelectedValue" />
                        <asp:ControlParameter ControlID="hdnPGal" Name="PGalleryImage" PropertyName="Value" />
                        <asp:ControlParameter ControlID="hdnVGal" Name="VGalleryImage" PropertyName="Value" />
                    </UpdateParameters>
                </asp:SqlDataSource>

            </div>




        </div>
    </div>

</asp:Content>
