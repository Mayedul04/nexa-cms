﻿Imports System.Data.SqlClient

Partial Class Admin_A_List1_AllList1Child
    Inherits System.Web.UI.Page

    Protected Sub btnAddNew_Click(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("List1Child.aspx?l1id=" & Request.QueryString("l1id") & "&Lang=" & Request.QueryString("Lang"))
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)
        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If
    End Sub

    Protected Sub chkNew_CheckedChanged(sender As Object, e As EventArgs)
        Dim a As Integer
        For Each row As ListViewDataItem In ListView1.Items
            Dim chkBox = DirectCast(row.FindControl("chkNew"), CheckBox)
            Dim proID = DirectCast(row.FindControl("hdnid"), HiddenField).Value
            If chkBox.Checked Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlString As String
                sqlString = "Update List1_Child Set NewArrival=1 where ChildID=@ChildID"
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = sqlString
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                sqlcomm.Parameters.Add("ChildID", Data.SqlDbType.Int, 32).Value = proID
                a = sqlcomm.ExecuteNonQuery()
                sqlcomm.Dispose()
                If a = 1 Then
                    ListView1.DataBind()
                End If
                sqlConn.Close()
            Else
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlString As String
                sqlString = "Update List1_Child Set NewArrival=0 where ChildID=@ChildID"
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = sqlString
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                sqlcomm.Parameters.Add("ChildID", Data.SqlDbType.Int, 32).Value = proID
                a = sqlcomm.ExecuteNonQuery()
                sqlcomm.Dispose()
                If a = 1 Then
                    ListView1.DataBind()
                End If
                sqlConn.Close()
            End If
        Next
    End Sub
End Class
