﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllTestimonial.aspx.vb" Inherits="Admin_A_CommonGallery_AllCommonGallery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1 class="page-title">
        All Testimonials</h1>
    <div class="btn-toolbar">
         <button runat="server" id="btnAddNew" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
        <button runat="server" id="btnBack" visible="false"  validationgroup="form" class="btn btn-primary"><i class="icon-save"></i> Back</button>
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></h2>
    <div>
        <div class="well">
            
            <p style="display:none;">
                <span>Table Name:</span>
                <asp:DropDownList ID="ddTableName" runat="server" Enabled ="false" >
                        <asp:ListItem Text="ARABIAN WILDLIFE" Value="ARABIAN_WILDLIFE" ></asp:ListItem>
                        <asp:ListItem Text="Carlos Favourites" Value="List_CarlosFavour" ></asp:ListItem>
                    </asp:DropDownList>
            </p>

            <asp:Label ID="lblError" runat="server"></asp:Label>
            <asp:HiddenField ID="hdnImageName" runat="server" />
            <asp:HiddenField ID="hdnTitle" runat="server" />
            <asp:HiddenField ID="hdnLastUpdated" runat="server" />
            <p>&nbsp;</p>
            <asp:ListView ID="ListView1" runat="server" DataKeyNames="ID" 
                DataSourceID="SqlDataSourceGallery">
                
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="IDLabel" runat="server" 
                                Text='<%# Eval("ID")%>' />
                        </td>
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("TestimonialBy") %>' />
                        </td>
                        
                        <td>
                            <asp:Label ID="LastUpdatedLabel" runat="server" 
                                Text='<%# Eval("LastUpdated","{0:dd MMM yyyy}") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" 
                                Checked='<%# Eval("Status") %>' Enabled="false" />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                            </td>
                        <td>
                            <asp:Label ID="TableNameLabel" runat="server" Text='<%# Eval("Lang") %>' />
                        </td>
                       <%-- <td>
                            <asp:Label ID="TableMasterIDLabel" runat="server" 
                                Text='<%# Eval("TableMasterID") %>' />
                        </td>--%>
                        
                        <td>
                            <a href='<%# "Testimonial.aspx?cgid=" & Eval("ID") & "&smallImageWidth=478&smallImageHeight=233"%>' title="Edit"><i class="icon-pencil"></i></a>&nbsp;
                            <a href='<%# "Testimonial.aspx?cgid=" & Eval("ID") & "&new=1"%>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign"></i> </a>&nbsp
                            <a href='<%# "#" & Eval("ID")%>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("ID")%>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    
                                <table ID="itemPlaceholderContainer"  border="0" style="" class="table">
                                    <thead>
                                    <tr id="Tr1" runat="server" style="">
                                        <th id="Th1" runat="server">
                                           ID</th>
                                        <th id="Th2" runat="server">
                                            Title</th>
                                       <th id="Th4" runat="server">
                                            By</th>
                                        
                                        <th id="Th6" runat="server">
                                            Last Updated</th>
                                         <th id="Th3" runat="server">
                                            Status</th>
                                         <th id="Th5" runat="server">
                                            Sort order</th>
                                        <th>Language</th>
                                        <%--<th id="Th7" runat="server">
                                            TableName</th>
                                        <th id="Th8" runat="server">
                                            TableMasterID</th>
                                        --%>
                                    </tr>
                                    </thead>
                                    
                                    <tr ID="itemPlaceholder" runat="server">
                                    </tr>
                                    <div class="paginationNew pull-right">
                                        <asp:DataPager ID="DataPager1" runat="server">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" 
                                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" 
                                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                            </Fields>
                                        </asp:DataPager>
                                    </div>
                                    
                                </table>
                            
                </LayoutTemplate>
            </asp:ListView>
            

            <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        
                DeleteCommand="DELETE FROM [List_Testimonial] WHERE [ID] = @ID" 
                SelectCommand="SELECT * FROM [List_Testimonial] ">
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                </DeleteParameters>
                

            </asp:SqlDataSource>
                    
        </div>
    </div>

</asp:Content>

