﻿
Partial Class Admin_A_List1_List1
    Inherits System.Web.UI.Page

    Protected smallImageWidth As String = "255", smallImageHeight As String = "138", bigImageWidth As String = "555", bigImageHeight As String = "299", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Utility.GetDimentionSetting("List1", "AnimalCategory", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)

        If Not IsPostBack Then
            If Not Request.QueryString("Small") Is Nothing Then
                pnlSmallImage.Visible = False
                pnlSmallDetails.Visible = False
            End If
            If Not Request.QueryString("Big") Is Nothing Then
                pnlBigImage.Visible = False
                pnlDetails.Visible = False
            End If
            If Not Request.QueryString("Link") Is Nothing Then
                pnlLink.Visible = False
            End If
           
            If Not Request.QueryString("pg") Is Nothing Then
                If Request.QueryString("pg") = "1" Then
                    pnlPGallery.Visible = True
                    ddlVGallery.DataBind()
                End If
            End If
            If Not Request.QueryString("vg") Is Nothing Then
                If Request.QueryString("vg") = "1" Then
                    pnlVGallery.Visible = True
                    ddlPGallery.DataBind()
                End If
            End If
            If Not String.IsNullOrEmpty(Request.QueryString("l1id")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update Animal Category"
                LoadContent(Request.QueryString("l1id"))
            Else

            End If
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now

        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If
        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If

        If fuPhotoAlbumImage.FileName <> "" Then
            hdnPhotoAlbumImage.Value = Utility.AddImage(fuPhotoAlbumImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Photo", Utility.EncodeTitle(txtTitle.Text, "-") & "-Photo"), Server)
        End If
        If hdnPhotoAlbumImage.Value <> "" Then
            imgPhotoAlbumImage.Visible = True
            imgPhotoAlbumImage.ImageUrl = "../" & hdnPhotoAlbumImage.Value
        End If

        If fuVideoAlbumImage.FileName <> "" Then
            hdnVideoAlbumImage.Value = Utility.AddImage(fuVideoAlbumImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Video", Utility.EncodeTitle(txtTitle.Text, "-") & "-Photo"), Server)
        End If
        If hdnVideoAlbumImage.Value <> "" Then
            imgVideoAlbumImage.Visible = True
            imgVideoAlbumImage.ImageUrl = "../" & hdnVideoAlbumImage.Value
        End If




        If String.IsNullOrEmpty(Request.QueryString("l1id")) Or Request.QueryString("new") = 1 Then
            If Request.QueryString("new") <> "1" Then
                hdnMasterID.Value = GetMasterID()
            End If
            If sdsList1.Insert() > 0 Then
                InsertIntoSEO()
                Response.Redirect("AllList1.aspx")
            Else
                divError.Visible = True
            End If
        Else
            If sdsList1.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM   List1 "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function

    Protected Sub InsertIntoSEO()
        '; INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID], [Lang]) VALUES (@Title, @SmallDetails, @SmallDetails,1, 'HTMLChild', @PageID, @Lang)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID],Lang) SELECT top 1  List1.Title,List1.SmallDetails,List1.SmallDetails,1,'AnimalCategory', List1.ListID,List1.Lang  FROM List1 order by List1.ListID  desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub


    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        *   FROM   List1 where ListID=@ListID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtTitle.Text = reader("Title") & ""
            'ddlCategory.SelectedValue = reader("StyleID").ToString()
            txtSmallDetails.Text = reader("SmallDetails") & ""
            txtDetails.Text = reader("BigDetails") & ""
            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""
            hdnPhotoAlbumImage.Value = reader("PhotoAlbumImage") & ""
            hdnVideoAlbumImage.Value = reader("VideoAlbumImage") & ""


            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If

            If hdnPhotoAlbumImage.Value <> "" Then
                imgPhotoAlbumImage.Visible = True
                imgPhotoAlbumImage.ImageUrl = "../" & hdnPhotoAlbumImage.Value
            End If

            If hdnVideoAlbumImage.Value <> "" Then
                imgVideoAlbumImage.Visible = True
                imgVideoAlbumImage.ImageUrl = "../" & hdnVideoAlbumImage.Value
            End If


            ddlBG.SelectedValue = reader("Link") & ""
            Boolean.TryParse(reader("Featured") & "", chkFeatured.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            txtImgAlt.Text = reader("ImageAltText") & ""
            hdnMasterID.Value = reader("MasterID") & ""
            ddlLang.SelectedValue = reader("Lang") & ""
            If IsDBNull(reader("GalleryID")) = False Then
                ddlPGallery.SelectedValue = reader("GalleryID").ToString()
            End If
            If IsDBNull(reader("VGalleryID")) = False Then
                ddlVGallery.SelectedValue = reader("VGalleryID").ToString
            End If
        End If
        conn.Close()
    End Sub

End Class
