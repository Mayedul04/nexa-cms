﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllContents.aspx.vb" Inherits="Admin_A_Content_AllContents" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-title">
        Contents for <%= Request.QueryString("t") %></h1>
    <div class="btn-toolbar"  >
         <button runat="server" id="Button1" class="btn btn-primary">
            <i class="icon-step-backward"></i> Back</button>
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-plus-sign"></i> Add New</button>
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Contents"></asp:Label></h2>
    <div>
        
        <div class="well">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            Content ID
                        </th>
                        <th>
                            Title
                        </th>
                        <th>
                            Image
                        </th>
                        <th>
                            Sort Order
                        </th>
                        <th>Status</th>
                        <th style="width: 60px;">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="ContentID" DataSourceID="sdsContent">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="HTMLID" runat="server" Text='<%# Eval("ContentID") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                                </td>
                                <td>
                                    <%--<asp:TextBox ID="TextBox1"  TextMode="MultiLine" Rows="3" Width="300px" Text='<%# Eval("DetailText")%>' runat="server"></asp:TextBox>--%>
                                    <img src='<%# "../" & Eval("Image") %>' width="120" />
                                </td>
                                <td>
                                    <asp:Label ID="lblMasterID" runat="server" Text='<%# Eval("SortIndex") %>' />
                                </td>
                                <td>
                                    <asp:CheckBox ID="StatusCheckBox" runat="server" Checked='<%# Eval("Status") %>'
                                Enabled="false" />
                                </td>
                                <td>
                                
                                    <a href='<%# "Content.aspx?cid=" & Eval("ContentID") & "&TName="& Request.QueryString("TName") &"&TID="& Request.QueryString("TID") &"&t="& Request.QueryString("t") &"&ImageWidth="& Request.QueryString("ImageWidth") &"&ImageHeight="& Request.QueryString("ImageHeight") & "&Title="& Request.QueryString("Title") & "&Image="& Request.QueryString("Image") & "&Text=" & Request.QueryString("Text") & "&link=" & Request.QueryString("link") & "&category=" & Request.QueryString("category") %>' title="Edit"><i class="icon-pencil"></i></a>
                                     &nbsp
                                    <a href='<%# "#" & Eval("ContentID") %>' data-toggle="modal"><i class="icon-remove"></i></a>&nbsp 
                                    <div class="modal small hide fade" id='<%# Eval("ContentID")%>' tabindex="-1" role="dialog"
                                        aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                ×</button>
                                            <h3 id="myModalLabel">
                                                Delete Confirmation</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p class="error-text">
                                                <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">
                                                Cancel</button>
                                            <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                                Text="Delete" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <div style="" class="paginationNew pull-right ">
                                <asp:DataPager ID="DataPager1" PageSize="50" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                            ShowPreviousPageButton="True" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                            ShowPreviousPageButton="False" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
        <asp:SqlDataSource ID="sdsContent" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT Contents.* FROM [Contents] where TableID=@TID and TableName=@TName"
            DeleteCommand="delete from Contents where ContentID=@ContentID">
            <DeleteParameters>
                <asp:Parameter Name="ContentID" />
            </DeleteParameters>
            <SelectParameters>
                <asp:QueryStringParameter DefaultValue="" Name="TID" QueryStringField="TID" />
                <asp:QueryStringParameter Name="TName" QueryStringField="TName" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>

