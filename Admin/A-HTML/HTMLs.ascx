﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="HTMLs.ascx.vb" Inherits="Admin_HTMLs" %>

    <h3>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Texts"></asp:Label></h3>
    <div>
        
        <div class="well">
            <table class="table">
                <thead>
                    <tr>
                        <th>
                            HTML ID
                        </th>
                        <th>
                            Title
                        </th>
                        <th>
                            Details
                        </th>
                        <th>
                            Master ID
                        </th>
                      <th>
                            Language
                        </th>
                        <th style="width: 60px;">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="HtmlID" DataSourceID="sdsHTML">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>
                                        No data was returned.
                                    </td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="HTMLID" runat="server" Text='<%# Eval("HTMLID") %>' />
                                </td>
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                                </td>
                                <td>
                                    <asp:TextBox ID="TextBox1"  TextMode="MultiLine" Rows="3" Width="300px" Text='<%# Eval("SmallDetails") %>' runat="server"></asp:TextBox>
                                    
                                </td>
                                <td>
                                    <asp:Label ID="lblMasterID" runat="server" Text='<%# Eval("MasterID") %>' />
                                </td>
                              <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("Lang") %>' />
                                </td>
                                <td>
                                    <a href="../A-Banner/AllTopBanner.aspx?TName=HTML&TID=<%# Eval("MasterID")%>&t=<%# Server.UrlEncode(Eval("Title")) %>&BigImageWidth=1349&BigImageHeight=542">Banners</a>
                                </td>
                                <td>
                                    <a href='<%# "../A-HTML/HTML.aspx?hid=" & Eval("HTMLID") %>' title="Edit"><i class="icon-pencil"></i></a>
                                     &nbsp
                                    <a href='<%# "#" & Eval("HTMLID") %>' data-toggle="modal"><i class="icon-remove"></i></a>&nbsp 
                                    <div class="modal small hide fade" id='<%# Eval("HtmlID") %>' tabindex="-1" role="dialog"
                                        aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                ×</button>
                                            <h3 id="myModalLabel">
                                                Delete Confirmation</h3>
                                        </div>
                                        <div class="modal-body">
                                            <p class="error-text">
                                                <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">
                                                Cancel</button>
                                            <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                                Text="Delete" />
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <tr id="itemPlaceholder" runat="server">
                            </tr>
                            <div style="" class="paginationNew pull-right ">
                                <asp:DataPager ID="DataPager1" PageSize="50" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                            ShowPreviousPageButton="True" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                            ShowPreviousPageButton="False" />
                                    </Fields>
                                </asp:DataPager>
                            </div>
                        </LayoutTemplate>
                    </asp:ListView>
                </tbody>
            </table>
        </div>
        <asp:SqlDataSource ID="sdsHTML" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT HTML.* FROM [HTML] inner join Languages on HTML.Lang=Languages.Lang   order by MasterID, Languages.SortIndex"
            DeleteCommand="delete from HTML where HtmlID=@HtmlID">
            <DeleteParameters>
                <asp:Parameter Name="HtmlID" />
            </DeleteParameters>
            <SelectParameters>
                
               <%-- <asp:Parameter Name="MasterIDs"  DbType="String" DefaultValue=""/>--%>
            </SelectParameters>
        </asp:SqlDataSource>
    </div>