﻿
Partial Class Admin_A_Stay_ResortEdit
    Inherits System.Web.UI.Page
    Protected smallImageWidth As String = "275", smallImageHeight As String = "183", bigImageWidth As String = "1270", bigImageHeight As String = "500", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("lid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i>  Update"
                lblTabTitle.Text = "Update Resort"
                LoadContent(Request.QueryString("lid"))
                sdsFeatured.SelectParameters("TableID").DefaultValue = Request.QueryString("lid")
            Else

            End If
        End If
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now

        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If
        If fuMap.FileName <> "" Then
            hdnMap.Value = Utility.AddImage(fuMap, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Map", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If


        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If

        If hdnMap.Value <> "" Then
            ImgMap.Visible = True
            ImgMap.ImageUrl = "../" & hdnMap.Value
        End If


        If String.IsNullOrEmpty(Request.QueryString("lid")) Then

            hdnMasterID.Value = GetMasterID()


            If sdsList.Insert() > 0 Then
                InsertIntoSEO()
                UpdateFeaters(GetLastID("en"), "-1")
                sdsList.InsertParameters("Lang").DefaultValue = "ar"
                If sdsList.Insert > 0 Then
                    InsertIntoSEO()
                    UpdateFeaters(GetLastID("ar"), "-2")
                    Response.Redirect("AllResorts.aspx")
                Else
                    divError.Visible = True
                End If

            Else
                divError.Visible = True
            End If
        Else
            If sdsList.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    Protected Sub InsertIntoSEO()
        '; INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID], [Lang]) VALUES (@Title, @SmallDetails, @SmallDetails,1, 'HTMLChild', @PageID, @Lang)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID],Lang) SELECT top 1  List_Stay.Title,List_Stay.SmallDetails,List_Stay.SmallDetails,1,'List_Stay', List_Stay.ListID,Lang  FROM List_Stay order by List_Stay.ListID  desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub
    Protected Function GetLastID(ByVal lang As String) As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT Max(ListID) as MaxLID  FROM   List_Stay where Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = lang

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxLID") & "" = "", "1", reader("MaxLID") & "")
        End If
        conn.Close()
        Return retVal
    End Function
    Protected Function GetReletiveID(ByVal listid As String, ByVal lang As String) As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  dbo.List_Stay.ListID as RelArID FROM dbo.List_Stay INNER JOIN dbo.List_Stay AS List_Stay_1 ON dbo.List_Stay.MasterID = List_Stay_1.MasterID WHERE (List_Stay_1.ListID = @RelID) AND (dbo.List_Stay.Lang =@Lang)"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("RelID", Data.SqlDbType.Int, 32).Value = listid
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = lang
        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = reader("RelArID").ToString()
        End If
        conn.Close()
        Return retVal
    End Function
    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM   List_Stay "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function

    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT *  FROM   List_Stay where ListID=@ListID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtTitle.Text = reader("Title") & ""
            txtSubTitle.Text = reader("SubTitle").ToString()
            txtSmallDetails.Text = reader("SmallDetails") & ""
            txtDetails.Text = reader("BigDetails") & ""
            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""
            txtMapcode.Text = reader("MapCode").ToString()
            hdnMap.Value = reader("MapImage").ToString()
            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If
            If hdnMap.Value <> "" Then
                ImgMap.Visible = True
                ImgMap.ImageUrl = "../" & hdnMap.Value
            End If

            txtBLink.Text = reader("BookingLink").ToString()
            txtLinkTextBox.Text = reader("Link") & ""
            Boolean.TryParse(reader("Featured") & "", chkFeatured.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            txtImgAlt.Text = reader("ImageAltText") & ""
            hdnMasterID.Value = reader("MasterID") & ""
            If IsDBNull(reader("GalleryID")) = False Then
                ddlGallery.SelectedValue = reader("GalleryID").ToString
            End If
            ddlLang.SelectedValue = reader("Lang").ToString()
            txtTripAdv.Text = reader("TripAdvisorCode").ToString()
        End If
        conn.Close()
    End Sub

    Protected Sub btnSF_Click(sender As Object, e As EventArgs) Handles btnSF.Click
        If Request.QueryString("lid") Is Nothing Then
            If sdsFeatured.Insert > 0 Then
                sdsFeatured.InsertParameters("Lang").DefaultValue = "ar"
                sdsFeatured.InsertParameters("TableID").DefaultValue = "-2" '-2 for arabic
                If sdsFeatured.Insert > 0 Then
                    sdsFeatured.SelectParameters("TableID").DefaultValue = "-1" '-1 for english
                    GridView1.DataBind()
                End If
            End If
        Else
            sdsFeatured.InsertParameters("TableID").DefaultValue = Request.QueryString("lid")
            sdsFeatured.InsertParameters("Lang").DefaultValue = ddlLang.SelectedValue
            If sdsFeatured.Insert > 0 Then
                sdsFeatured.InsertParameters("Lang").DefaultValue = If(ddlLang.SelectedValue = "en", "ar", "en")
                sdsFeatured.InsertParameters("TableID").DefaultValue = GetReletiveID(Request.QueryString("lid"), If(ddlLang.SelectedValue = "en", "ar", "en"))
                If sdsFeatured.Insert > 0 Then
                    sdsFeatured.SelectParameters("TableID").DefaultValue = Request.QueryString("lid")
                    GridView1.DataBind()
                End If
            End If
        End If

    End Sub
    Public Sub UpdateFeaters(ByVal tableid As String, ByVal tempid As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim updatestring As String = "UPDATE [dbo].[SpecialFeatures]  SET [TableID] = @TableID WHERE [TableID]=@TempID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(updatestring, conn)
        cmd.CommandText = updatestring
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = tableid
        cmd.Parameters.Add("TempID", Data.SqlDbType.Int, 32).Value = tempid
        cmd.ExecuteNonQuery()
        conn.Close()
    End Sub
End Class
