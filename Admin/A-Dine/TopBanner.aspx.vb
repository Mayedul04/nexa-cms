﻿Imports System.Data.OleDb
Imports System.Drawing.Imaging
Imports System.Data.SqlClient

Partial Class TopBanner
    Inherits System.Web.UI.Page
    Protected smallImageWidth As String = "", smallImageHeight As String = "", bigImageWidth As String = "", bigImageHeight As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Utility.GetDimentionSetting("Banner", "Advanture", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, "", "")

        If IsPostBack = False Then
            If Request.QueryString("bannerId") <> "" Then
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = "SELECT * FROM Advanture_Banner where BannerID=" & Request.QueryString("bannerId")
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
                reader.Read()
                ddSection.SelectedValue = reader("MasterID").ToString()
                txtTitle.Text = reader("Title").ToString()
                txtSmallDetails.Text = reader("SmallDetails").ToString()
                imgSmallImage.ImageUrl = "~/Admin/" + reader("SmallImage").ToString()
                hdnSmallImage.Value = reader("SmallImage").ToString()
                imgBigImage.ImageUrl = "~/Admin/" + reader("BigImage").ToString()
                hdnBigImage.Value = reader("BigImage").ToString()
                txtSortIndex.Text = reader("SortIndex").ToString()
                chkStatus.Checked = reader("Status").ToString()
                lbLastUpdated.Text = reader("LastUpdated").ToString()
                txtImgAlt.Text = reader("ImageAltText") & ""
                ddlLang.SelectedValue = reader("Lang").ToString()
                sqlConn.Close()
                imgSmallImage.Visible = True
                imgBigImage.Visible = True
                'rfvSmallImage.Enabled = False
                rfvBigImage.Enabled = False
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
            End If

        End If
        If Request.QueryString("section") <> "" Then
            ddSection.SelectedValue = Request.QueryString("section")
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        Try
            lbLastUpdated.Text = DateTime.Now()
            If fuSmallImage.FileName <> "" Then
                hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
                imgSmallImage.ImageUrl = "~/Admin/" + hdnSmallImage.Value
                imgSmallImage.Visible = True
            End If
            If fuBigImage.FileName <> "" Then
                hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
                imgBigImage.ImageUrl = "~/Admin/" + hdnBigImage.Value
                imgBigImage.Visible = True
            End If

            Dim a As Integer
            If Request.QueryString("bannerId") <> "" Then
                a = SqlDataSourceTopBanner.Update()
            Else
                a = SqlDataSourceTopBanner.Insert()
                Response.Redirect("AllTopBanner.aspx")
            End If

            If a > 0 Then
                divSuccess.Visible = True
                divError.Visible = False
            Else
                divSuccess.Visible = False
                divError.Visible = True
            End If
        Catch ex As Exception
            divSuccess.Visible = False
            divError.Visible = True
        End Try
    End Sub


End Class
