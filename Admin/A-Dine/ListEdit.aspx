﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false"
    CodeFile="ListEdit.aspx.vb" Inherits="Admin_A_List_List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
<div class="block">
    <div class="block-heading">Style Group</div>
    <div class="block-body">
    <h1 class="page-title">
        List</h1>
    <div class="btn-toolbar">
        <a href="../A-AdvantureActivity/AllList.aspx" data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
       <div id="myTabContent" class="tab-content">
          
            <p>
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <asp:Panel ID="pnlSmallImage"  runat="server">
                <p>
                    <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                    <asp:HiddenField ID="hdnSmallImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                    <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlBigImage"  runat="server">
                <p>
                    <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                    <asp:HiddenField ID="hdnBigImage" runat="server" />
                </p>
                <p>
                    <label>
                        Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                    <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
                <p>
                    <label>
                        Image Alt Text</label>
                    <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                </p>
            </asp:Panel>





            <asp:Panel ID="pnlSmallDetails" runat="server">
                <p>
                    <label>
                        Small Details:</label>
                    <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                        Rows="4"></asp:TextBox>
                    <label class="red">
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" ControlToValidate="txtSmallDetails" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>--%>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic"
                            runat="server" ControlToValidate="txtSmallDetails" ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$"
                            ErrorMessage="* character limit is 1000" ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </label>
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlDetails" Visible="false" runat="server">
                <p>
                    <label>
                        Details 1:</label>
                    <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                    <script>

                        // Replace the <textarea id="editor1"> with a CKEditor
                        // instance, using default configuration.

                        CKEDITOR.replace('<%=txtDetails.ClientID %>',
                            {
                                filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                "extraPlugins": "imagebrowser",
                                "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                            }
                        );



                    </script>
                </p>
            </asp:Panel>

     

             <p>
                    <label>
                        BG Class</label>
                    <asp:DropDownList ID="ddlClass" runat="server">
                     <asp:ListItem>bluebg</asp:ListItem>
                     <asp:ListItem>orangebg</asp:ListItem>
                     <asp:ListItem>purplebg</asp:ListItem>
                     <asp:ListItem>greenbg</asp:ListItem>
                    
                 </asp:DropDownList>
                </p>


            <asp:Panel ID="pnlLink"  runat="server">
                <p>
                    <label>
                        Link:</label>
                    <asp:TextBox ID="txtLinkTextBox" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />
                </p>
            </asp:Panel>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>
            <asp:Panel ID="pnlFeatured" Visible="false" runat="server">
                <p>
                    <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured " TextAlign="Left" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>
            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>
            <p style="display:none;">
                <label>
                    Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]">
                </asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnMasterID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>
        <div class="btn-toolbar">
            <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
            <button runat="server" id ="btnSubmit" ValidationGroup="form"  class="btn btn-primary" ><i class="icon-save"></i> Add New</button>
            <div class="btn-group">
            </div>
        </div>

          <asp:SqlDataSource ID="sdsList" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                DeleteCommand="DELETE FROM [List_Dine] WHERE [ListID] = @ListID" 
                InsertCommand="INSERT INTO [List_Dine] ( [Title], [SmallDetails], [BigDetails], [SmallImage], [BigImage], [Link], [Featured], [SortIndex], [Status], [LastUpdated],ImageAltText,MasterID, Lang,BG) VALUES (  @Title, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @Link, @Featured, @SortIndex, @Status, @LastUpdated,@ImageAltText,@MasterID, @Lang,@BG)" 
                SelectCommand="SELECT * FROM [List_Dine]" 
                UpdateCommand="UPDATE [List_Dine] SET   [Title] = @Title, [SmallDetails] = @SmallDetails, [BigDetails] = @BigDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [Link] = @Link, [Featured] = @Featured, [SortIndex] = @SortIndex, [Status] = @Status, [LastUpdated] = @LastUpdated, ImageAltText=@ImageAltText, MasterID=@MasterID, Lang=@Lang,BG=@BG  WHERE [ListID] = @ListID">
                <DeleteParameters>
                    <asp:Parameter Name="ListID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="ddlCategory" Name="Category" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                        PropertyName="Text" Type="String" />
                     <asp:ControlParameter ControlID="ddlClass" Name="BG" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                        PropertyName="Value" Type="String" />
                   
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkFeatured" Name="Featured" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" 
                        PropertyName="Text" Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                        PropertyName="Value" Type="DateTime" />
                    <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                </InsertParameters>
                <UpdateParameters>
                
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" 
                        Type="String" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" 
                        PropertyName="Text" Type="String" />
                      <asp:ControlParameter ControlID="ddlClass" Name="BG" 
                        PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" 
                        PropertyName="Value" Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" 
                        PropertyName="Value" Type="String" />
                     
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link" 
                        PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="chkFeatured" Name="Featured" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" 
                        PropertyName="Text" Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" 
                        PropertyName="Checked" Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" 
                        PropertyName="Value" Type="DateTime" />
                    <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                    <asp:QueryStringParameter Name="ListID" QueryStringField="lid" 
                        Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

        
        
    </div>
    </div>
</div>


</asp:Content>
