﻿
Partial Class Admin_A_List_List
    Inherits System.Web.UI.Page

    Protected smallImageWidth As String = "255", smallImageHeight As String = "363", bigImageWidth As String = "433", bigImageHeight As String = "217", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Utility.GetDimentionSetting("List_Dine", "All", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("lid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i>  Update"
                lblTabTitle.Text = "Update List"
                LoadContent(Request.QueryString("lid"))
            Else

            End If
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now

        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If


        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If
      

        If String.IsNullOrEmpty(Request.QueryString("lid")) Or Request.QueryString("new") = 1 Then
            If Request.QueryString("new") <> "1" Then
                hdnMasterID.Value = GetMasterID()
            End If

            If sdsList.Insert() > 0 Then
                InsertIntoSEO()
                ' Response.Redirect("AllList.aspx")
            Else
                divError.Visible = True
            End If
        Else
            If sdsList.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    Protected Sub InsertIntoSEO()
        '; INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID], [Lang]) VALUES (@Title, @SmallDetails, @SmallDetails,1, 'HTMLChild', @PageID, @Lang)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID],Lang) SELECT top 1  List_Advanture_Activity.Title,List_Advanture_Activity.SmallDetails,List_Advanture_Activity.SmallDetails,1,'List_Advanture_Activity', List_Advanture_Activity.ListID,Lang  FROM List_Advanture_Activity order by List_Advanture_Activity.ListID  desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM   List_Advanture_Activity "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function

    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT        ListID, Title, SmallDetails, BigDetails, SmallImage, BigImage, Link, Featured, SortIndex, Status, LastUpdated,ImageAltText ,Lang,MasterID ,BG    FROM   List_Dine where ListID=@ListID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ListID", Data.SqlDbType.Int)
        cmd.Parameters("ListID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtTitle.Text = reader("Title") & ""
            txtSmallDetails.Text = reader("SmallDetails") & ""
            txtDetails.Text = reader("BigDetails") & ""
            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""

            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If

           

            txtLinkTextBox.Text = reader("Link") & ""
            Boolean.TryParse(reader("Featured") & "", chkFeatured.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            txtImgAlt.Text = reader("ImageAltText") & ""
            hdnMasterID.Value = reader("MasterID") & ""
            ddlLang.SelectedValue = reader("Lang") & ""

            ddlClass.SelectedValue = reader("BG").ToString

        End If
        conn.Close()
    End Sub


End Class
