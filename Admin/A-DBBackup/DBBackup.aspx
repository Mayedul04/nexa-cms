﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    CodeFile="DBBackup.aspx.vb"  Inherits="Admin_DBBackup" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
 <h1 class="page-title">Database Backup</h1>
   <div class="btn-toolbar">
    <button runat="server" id ="btnSubmit" ValidationGroup="form"  class="btn btn-primary"><i class="icon-save"></i> Add New</button>
        <div class="btn-group">
    </div>
    </div>

    <h2><asp:Label ID="lblTabTitle" runat="server" Text="Add Database Backup"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
   </div>
   <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
                    </span><span class="error-right-bot"></span>
                </div>
    </div>

    <!-- content -->
     <p>
                    <label>DBName:</label>
                    <asp:TextBox ID="txtDBName" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form" 
                        ControlToValidate="txtDBName" runat="server" ErrorMessage="* Requred" 
                        SetFocusOnError="True"></asp:RequiredFieldValidator>
                </p>
    <div class="well">
        

                

                    <asp:Panel ID="pnlGrid" runat="server"  ScrollBars="Horizontal">
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="true" AutoGenerateColumns="true"
                            CellPadding="4" EnableModelValidation="True" GridLines="None" CellSpacing="1">
                            <HeaderStyle CssClass="heading" />
                            <RowStyle CssClass="first" />
                            <AlternatingRowStyle CssClass="alt" />
                        </asp:GridView>
                    </asp:Panel>
             
         

    </div>
    <!-- Eof content -->        
</asp:Content>
