﻿
Partial Class Admin_A_YourHome_AllYourHome
    Inherits System.Web.UI.Page


    Protected Sub btnAddNew_ServerClick(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("YourHome.aspx")
    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString
            Response.Cookies("backurlAdmin").Expires = Date.Today.AddDays(+1)
            ddlLang.DataBind()
            HTMLs.MasterIDs = "6"
            'HTMLs1.Lang = ddlLang.SelectedValue
        End If

    End Sub

End Class
