﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    ViewStateEncryptionMode="Never" EnableViewStateMac="false" CodeFile="AllTopBanner.aspx.vb"
    Inherits="AllTopBanner" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">
        Banners</h1>
    <div class="btn-toolbar">
        
        <button runat="server" id="btnBack" visible="false" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Back</button>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot">
            </span><span class="error-right-bot"></span>
        </div>
    </div>
    <div class="well">
         <p>
                <label>Multiple image upload (Big Image (min) Width=<%= Request.QueryString("BigImageWidth")%>; Big Image (min) Height=<%= Request.QueryString("BigImageHeight")%>)</label>
            </p>
            <asp:AjaxFileUpload ID="AjaxFileUploadBigImage" runat="server" ThrobberID="MyThrobber" AllowedFileTypes="jpg,jpeg,gif,png" />
            <asp:Image
                ID="MyThrobber"
                ImageUrl="ajax-loader.gif"
                Style="display: None"
                runat="server" />
            <asp:Label ID="lblError" runat="server"></asp:Label>
            <asp:HiddenField ID="hdnImageName" runat="server" />
            <asp:HiddenField ID="hdnTitle" runat="server" />
            <asp:HiddenField ID="hdnLastUpdated" runat="server" />
            <p>&nbsp;</p>
        
        <asp:ListView ID="ListView1" runat="server" DataKeyNames="BannerID"
                DataSourceID="SqlDataSourceGallery">

                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="BannerIDLabel" runat="server"
                                Text='<%# Eval("BannerID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <img src='<%# "../" & Eval("BigImage")  %>' width="250px" />

                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server"
                                Checked='<%# Eval("Status") %>' Enabled="false" />
                        </td>
                        <td>
                            <asp:Label ID="LastUpdatedLabel" runat="server"
                                Text='<%# Eval("LastUpdated","{0:dd MMM yyyy}") %>' />
                        </td>
                       
                        <td>
                            <a href='<%#  "TopBanner.aspx?bannerId=" & Eval("BannerID") & "&smallImageWidth=0&smallImageHeight=0&BigImageWidth=" & Request.QueryString("BigImageWidth") & "&BigImageHeight=" & Request.QueryString("BigImageHeight")  %>' title="Edit"><i class="icon-pencil"></i></a>&nbsp;
                            <a href='<%# "#" & Eval("BannerID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("BannerID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>

                    <table id="itemPlaceholderContainer" border="0" style="" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">Banner ID</th>
                                <th id="Th2" runat="server">Title</th>
                                <th id="Th3" runat="server">Banner Image</th>
                                <th id="Th4" runat="server">Sort Order</th>
                                <th id="Th5" runat="server">Status</th>
                                <th id="Th6" runat="server">Last Updated</th>
                               
                                <th></th>
                            </tr>
                        </thead>

                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True"
                                        ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True"
                                        ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>

                    </table>

                </LayoutTemplate>
            </asp:ListView>
    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [Banner] WHERE [BannerID] = @BannerID" InsertCommand="INSERT INTO [Banner] ([TableName],[TableID], [Title], [SmallImage], [BigImage], [ImageAltText], [Status],  [LastUpdated],[Lang]) VALUES (@TableName, @TableID, @Title,  @SmallImage, @BigImage, @ImageAltText,   @Status, @LastUpdated,@Lang)"
        SelectCommand="SELECT * FROM [Banner] where TableName=@TName and TableID=@TID">
        <DeleteParameters>
            <asp:Parameter Name="BannerID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:CookieParameter CookieName="TableName" Name="TableName" Type="String" />
            <asp:ControlParameter ControlID="hdnTitle" Name="Title" PropertyName="Value" Type="String" />
           
            <asp:ControlParameter ControlID="hdnImageName" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnImageName" Name="BigImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnTitle" Name="ImageAltText" PropertyName="Value"
                Type="String" />
           
            <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
            <asp:ControlParameter ControlID="hdnLastUpdated" Name="LastUpdated" PropertyName="Value" Type="DateTime" />
            <asp:QueryStringParameter DefaultValue="en" Name="Lang" QueryStringField="lang" Type="String" />
            <asp:CookieParameter CookieName="TableID" Name="TableID" Type="Int32" />
        </InsertParameters>
        
        <SelectParameters>
            <asp:QueryStringParameter Name="TName" QueryStringField="TName" />
            <asp:QueryStringParameter Name="TID" QueryStringField="TID" />
        </SelectParameters>
        
    </asp:SqlDataSource>
</asp:Content>
