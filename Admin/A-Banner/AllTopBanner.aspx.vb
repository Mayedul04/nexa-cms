﻿Imports System.Drawing.Imaging
Imports System.Data.SqlClient
Partial Class AllTopBanner
    Inherits System.Web.UI.Page

    Protected smallImageWidth As String = "1600", smallImageHeight As String = "446", bigImageWidth As String = "1600", bigImageHeight As String = "446"
    Protected Sub btnBack_Click(sender As Object, e As System.EventArgs) Handles btnBack.ServerClick
        If Request.Cookies("backurlAdmin") IsNot Nothing Then
            Response.Redirect(Request.Cookies("backurlAdmin").Value)
        End If

    End Sub

    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load




        If Not IsPostBack Then


            If Request.QueryString("contextKey") = "" Then
                'ddTableName.SelectedValue = Request.QueryString("TableName")

                lblTitle.Text = "List of image for '" & Request.QueryString("t") & "'"
                ' ddTableName.SelectedValue = Request.QueryString("TableName")


                If Request.Cookies("TableName") Is Nothing Then
                    Response.Cookies.Add(New HttpCookie("TableName", Request.QueryString("TName")))
                Else
                    Response.Cookies("TableName").Value = Request.QueryString("TName")

                End If
                If Request.Cookies("TableID") Is Nothing Then
                    Response.Cookies.Add(New HttpCookie("TableID", Request.QueryString("TID")))
                Else
                    Response.Cookies("TableID").Value = Request.QueryString("TID")
                End If
            End If

        End If
    End Sub

    Protected Sub AjaxFileUploadBigImage_UploadComplete(sender As Object, e As AjaxControlToolkit.AjaxFileUploadEventArgs) Handles AjaxFileUploadBigImage.UploadComplete

        Dim filePath As String = "~/Admin/Content/" & e.FileName
        lblError.Text &= filePath & "<br />"
        ' Save upload file to the file system
        AjaxFileUploadBigImage.SaveAs(Server.MapPath(filePath))
        e.DeleteTemporaryData()
        hdnImageName.Value = "Content/" & e.FileName
        hdnTitle.Value = e.FileName.Remove(e.FileName.LastIndexOf("."))
        hdnLastUpdated.Value = DateTime.Now
        'hdnTableMasterID.Value = Request.QueryString("TableMasterID")
        SqlDataSourceGallery.Insert()

    End Sub

    Protected Sub AjaxFileUploadBigImage_UploadCompleteAll(sender As Object, e As AjaxControlToolkit.AjaxFileUploadCompleteAllEventArgs) Handles AjaxFileUploadBigImage.UploadCompleteAll
        ListView1.DataBind()
    End Sub

End Class
