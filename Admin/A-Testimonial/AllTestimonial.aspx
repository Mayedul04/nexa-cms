﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllTestimonial.aspx.vb" Inherits="Admin_A_CommonGallery_AllCommonGallery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1 class="page-title">
        All Testimonials for <%= Request.QueryString("t")%></h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnBack"  validationgroup="form" class="btn btn-primary"><i class="icon-step-backward"></i> Back</button>
         <button runat="server" id="btnAddNew" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
        
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></h2>
    <div>
        <div class="well">
            
            <asp:ListView ID="ListView1" runat="server" DataKeyNames="ID" 
                DataSourceID="SqlDataSourceGallery">
                
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="IDLabel" runat="server" 
                                Text='<%# Eval("ID")%>' />
                        </td>
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("BigDetails") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("TestimonialBy") %>' />
                        </td>
                        
                        <td>
                            <asp:Label ID="LastUpdatedLabel" runat="server" 
                                Text='<%# Eval("LastUpdated","{0:dd MMM yyyy}") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" 
                                Checked='<%# Eval("Status") %>' Enabled="false" />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                            </td>
                        <%--<td>
                            <asp:Label ID="TableNameLabel" runat="server" Text='<%# Eval("TableName") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TableMasterIDLabel" runat="server" 
                                Text='<%# Eval("TableMasterID") %>' />
                        </td>--%>
                        
                        <td>
                            <a href='<%# "Testimonial.aspx?cgid=" & Eval("ID") & "&smallImageWidth=478&smallImageHeight=233&image=0&TName=" & Request.QueryString("TName") & "&TID="& Request.QueryString("TID") &"&t=" & Request.QueryString("t") %>' title="Edit"><i class="icon-pencil"></i></a>&nbsp;
                            <a href='<%# "#" & Eval("ID")%>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("ID")%>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    
                                <table ID="itemPlaceholderContainer"  border="0" style="" class="table">
                                    <thead>
                                    <tr id="Tr1" runat="server" style="">
                                        <th id="Th1" runat="server">
                                           ID</th>
                                        <th id="Th2" runat="server">
                                            Testimonials</th>
                                       <th id="Th4" runat="server">
                                            By</th>
                                        
                                        <th id="Th6" runat="server">
                                            Last Updated</th>
                                         <th id="Th3" runat="server">
                                            Status</th>
                                         <th id="Th5" runat="server">
                                            Sort order</th>
                                        <%--<th id="Th7" runat="server">
                                            TableName</th>
                                        <th id="Th8" runat="server">
                                            TableMasterID</th>
                                        --%>
                                    </tr>
                                    </thead>
                                    
                                    <tr ID="itemPlaceholder" runat="server">
                                    </tr>
                                    <div class="paginationNew pull-right">
                                        <asp:DataPager ID="DataPager1" runat="server">
                                            <Fields>
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="True" 
                                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                                <asp:NumericPagerField />
                                                <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="True" 
                                                    ShowNextPageButton="False" ShowPreviousPageButton="False" />
                                            </Fields>
                                        </asp:DataPager>
                                    </div>
                                    
                                </table>
                            
                </LayoutTemplate>
            </asp:ListView>
            

            <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        
                DeleteCommand="DELETE FROM [List_Testimonial] WHERE [ID] = @ID" 
                
                SelectCommand="SELECT * FROM [List_Testimonial] WHERE (([TableName] = @TableName) AND ([TableMasterID] = @TableMasterID)) ORDER BY [ID] DESC" 
                InsertCommand="INSERT INTO [List_Testimonial] ([Title], [SmallImage], [ImageAltText], [BigDetails], [TestimonialBy], [TestimonialDate], [SortIndex], [Status], [LastUpdated], [TableName], [TableMasterID]) VALUES (@Title, @SmallImage, @ImageAltText, @BigDetails, @TestimonialBy, @TestimonialDate, @SortIndex, @Status, @LastUpdated, @TableName, @TableMasterID)" 
                UpdateCommand="UPDATE [List_Testimonial] SET [Title] = @Title, [SmallImage] = @SmallImage, [ImageAltText] = @ImageAltText, [BigDetails] = @BigDetails, [TestimonialBy] = @TestimonialBy, [TestimonialDate] = @TestimonialDate, [SortIndex] = @SortIndex, [Status] = @Status, [LastUpdated] = @LastUpdated, [TableName] = @TableName, [TableMasterID] = @TableMasterID WHERE [ID] = @ID">
                <DeleteParameters>
                    <asp:Parameter Name="ID" Type="Int32" />
                </DeleteParameters>
                

                <InsertParameters>
                    <asp:Parameter Name="Title" Type="String" />
                    <asp:Parameter Name="SmallImage" Type="String" />
                    <asp:Parameter Name="ImageAltText" Type="String" />
                    <asp:Parameter Name="BigDetails" Type="String" />
                    <asp:Parameter Name="TestimonialBy" Type="String" />
                    <asp:Parameter Name="TestimonialDate" Type="DateTime" />
                    <asp:Parameter Name="SortIndex" Type="Int32" />
                    <asp:Parameter Name="Status" Type="Boolean" />
                    <asp:Parameter Name="LastUpdated" Type="DateTime" />
                    <asp:Parameter Name="TableName" Type="String" />
                    <asp:Parameter Name="TableMasterID" Type="Int32" />
                </InsertParameters>
                <SelectParameters>
                    <asp:QueryStringParameter DefaultValue="TName" Name="TableName" 
                        QueryStringField="TName" Type="String" />
                    <asp:QueryStringParameter DefaultValue="" Name="TableMasterID" 
                        QueryStringField="TID" Type="Int32" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Title" Type="String" />
                    <asp:Parameter Name="SmallImage" Type="String" />
                    <asp:Parameter Name="ImageAltText" Type="String" />
                    <asp:Parameter Name="BigDetails" Type="String" />
                    <asp:Parameter Name="TestimonialBy" Type="String" />
                    <asp:Parameter Name="TestimonialDate" Type="DateTime" />
                    <asp:Parameter Name="SortIndex" Type="Int32" />
                    <asp:Parameter Name="Status" Type="Boolean" />
                    <asp:Parameter Name="LastUpdated" Type="DateTime" />
                    <asp:Parameter Name="TableName" Type="String" />
                    <asp:Parameter Name="TableMasterID" Type="Int32" />
                    <asp:Parameter Name="ID" Type="Int32" />
                </UpdateParameters>
                

            </asp:SqlDataSource>
                    
        </div>
    </div>

</asp:Content>

