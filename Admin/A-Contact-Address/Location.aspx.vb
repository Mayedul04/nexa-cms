﻿
Partial Class Admin_A_Partner_Partner
    Inherits System.Web.UI.Page


    Protected smallImageWidth As String = "", smallImageHeight As String = "", bigImageWidth As String = "", bigImageHeight As String = "", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("lid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i>  Update"
                lblTabTitle.Text = "Update Address"
                LoadContent(Request.QueryString("lid"))
            Else
                hdnTableID.Value = Request.QueryString("TID").ToString()
                hdnTableName.Value = Request.QueryString("TName").ToString()
            End If
        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now
        'hdnDetails.Value = FC
        If String.IsNullOrEmpty(Request.QueryString("lid")) Then
            If sdsAdd.Insert() > 0 Then
                Response.Redirect("AllLocations.aspx?TName=" & Request.QueryString("TName") & "&TID=" & Request.QueryString("TID") & "&t=" & Request.QueryString("t"))
                'sdsAdd.InsertParameters("Lang").DefaultValue = "ar"
                'If sdsAdd.Insert() > 0 Then
                '    Response.Redirect("AllLocations.aspx?TName=" & Request.QueryString("TName") & "&TID=" & Request.QueryString("TID") & "&t=" & Request.QueryString("t"))
                'Else
                '    divError.Visible = True
                'End If
            Else
                divError.Visible = True
            End If
        Else
            If sdsAdd.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    

    

    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM   ContactDetails where ContactID=@ContactID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("ContactID", Data.SqlDbType.Int)
        cmd.Parameters("ContactID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtTitle.Text = reader("Heading") & ""
            txtWebsite.Text = reader("WebSite") & ""
           
            txtDetails.Text = reader("Details").ToString()


            '  Boolean.TryParse(reader("HeadOffice") & "", chkHeadOffice.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)

            hdnTableName.Value = reader("TableName").ToString()
            hdnTableID.Value = reader("TableID") & ""
            ddlLang.SelectedValue = reader("Lang") & ""
            ', Email, Phone, Mobile, Fax, Address
            EmailTextBox.Text = reader("Email") & ""
            PhoneTextBox.Text = reader("Phone") & ""

            txtIntPhone.Text = reader("IntPhone") & ""


        End If
        conn.Close()
    End Sub



End Class
