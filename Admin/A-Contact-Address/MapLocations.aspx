﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="MapLocations.aspx.vb" Inherits="Admin_A_Contact_Address_MapLocations" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-title">Map Locations</h1>

   <div class="btn-toolbar">
        <button runat="server" id="Button1" class="btn btn-primary">
            <i class="icon-step-backward"></i> Back</button>
        <button runat="server" id ="btnAddNew"  class="btn btn-primary"><i class="icon-plus"></i> Add New</button>
        <div class="btn-group"></div>
    </div>

    <!-- content -->

    <h2><asp:Label ID="lblTabTitle" runat="server" Text="All Map Locations"></asp:Label></h2>
    <p>
        <label>
            Language</label>
        <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true"
            DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang">
        </asp:DropDownList>
        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
    </p>
    <div>

         

        <div class="well">
           
           <asp:ListView ID="ListView1" runat="server" DataSourceID="sdsMap" 
                DataKeyNames="LocationID">
                
                <ItemTemplate>
                    <tr style="">
                       
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <asp:Image ID="imgThum" runat="server" Width="100px" ImageUrl='<%#"~/Admin/"+ Eval("SmallImage") %>' />
                           
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("Category") %>' />
                        </td>
                        <td>
                            <asp:CheckBox ID="chkStatus" runat="server" Checked='<%# Eval("Status") %>' />
                        </td>
                       
                       
                      
                        <td>
                            <a href='<%# "MapLocationEdit.aspx?mid=" & Eval("LocationID")%>' title="Edit"><i class="icon-pencil">
                            </i></a>&nbsp
                            <%--<a href='<%# "MapLocationEdit.aspx?masid=" & Eval("MasterID")%>' title="Edit">Add Arabic</a>&nbsp--%>
                            <a href='<%# "#" & Eval("LocationID")%>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("LocationID")%>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                
                                <th id="Th2" runat="server">
                                    Title</th>
                                <th id="Th5" runat="server">
                                    Image</th>
                                <th id="Th7" runat="server">
                                    Category</th>
                                
                                <th id="Th9" runat="server">
                                    Status</th>
                               
                                
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
                
            </asp:ListView>
            <asp:SqlDataSource ID="sdsMap" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                
                SelectCommand="SELECT * FROM [MapLocations] where Lang=@Lang" 
                
                DeleteCommand="DELETE FROM [MapLocations] WHERE [LocationID] = @LocationID" 
                InsertCommand="INSERT INTO [MapLocations] ([Title], [Details], [Link], [Status], [Category], [SmallImage],[Lang]) VALUES (@Title, @Details, @Link, @Status, @Category, @SmallImage,@Lang)" 
                UpdateCommand="UPDATE [MapLocations] SET [Title] = @Title, [Details] = @Details, [Link] = @Link, [Status] = @Status, [Category] = @Category, [SmallImage] = @SmallImage, [Lang]=@Lang WHERE [LocationID] = @LocationID" >
                <DeleteParameters>
                    <asp:Parameter Name="LocationID" Type="Int32" />
                </DeleteParameters>
               
                <InsertParameters>
                    <asp:Parameter Name="Title" Type="String" />
                    <asp:Parameter Name="Details" Type="String" />
                    <asp:Parameter Name="Link" Type="String" />
                    <asp:Parameter Name="Status" Type="Boolean" />
                    <asp:Parameter Name="Category" Type="String" />
                    <asp:Parameter Name="SmallImage" Type="String" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="Title" Type="String" />
                    <asp:Parameter Name="Details" Type="String" />
                    <asp:Parameter Name="Link" Type="String" />
                    <asp:Parameter Name="Status" Type="Boolean" />
                    <asp:Parameter Name="Category" Type="String" />
                    <asp:Parameter Name="SmallImage" Type="String" />
                    <asp:Parameter Name="LocationID" Type="Int32" />
                </UpdateParameters>
               
            </asp:SqlDataSource>
                                
        </div> 
    <!-- Eof content -->

            
</div>
    <!-- Eof content -->

    


           
</asp:Content>

