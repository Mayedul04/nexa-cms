﻿
Partial Class Admin_A_Contact_Address_MapLocationEdit
    Inherits System.Web.UI.Page
    Protected smallImageWidth As String = "275", smallImageHeight As String = "183", bigImageWidth As String = "", bigImageHeight As String = "", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load


        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("mid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i>  Update"
                lblTabTitle.Text = "Update Map"
                LoadContent(Request.QueryString("mid"))

            End If
            If Not Request.QueryString("masid") Is Nothing Then
                LoadEnContent()
            End If
        End If

    End Sub
    Private Sub LoadEnContent()
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM   MapLocations where MasterID=@MasterID and Lang=@Lang"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = Request.QueryString("masid")
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtTitle.Text = reader("Title") & ""
            txtWebsite.Text = reader("Link") & ""
            txtDetails.Text = reader("Details").ToString()
            hdnSmallImage.Value = reader("SmallImage") & ""
            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            ddCategory.SelectedValue = reader("Category").ToString()
            txtposition.Text = reader("Coordinates").ToString()
            hdnMasterID.Value = reader("MasterID")
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            ddlLang.SelectedValue = "ar"



        End If
        conn.Close()
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick

        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtTitle.Text <> "", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
        End If
        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        If String.IsNullOrEmpty(Request.QueryString("mid")) Then
            If sdsMap.Insert() > 0 Then
                divSuccess.Visible = True
                Response.Redirect("MapLocations.aspx")
            Else
                divError.Visible = True
            End If
        Else
            If sdsMap.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub





    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT * FROM   MapLocations where LocationID=@LocationID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("LocationID", Data.SqlDbType.Int)
        cmd.Parameters("LocationID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()

            txtTitle.Text = reader("Title") & ""
            txtWebsite.Text = reader("Link") & ""
            txtDetails.Text = reader("Details").ToString()
            hdnSmallImage.Value = reader("SmallImage") & ""
            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            ddCategory.SelectedValue = reader("Category").ToString()
            txtposition.Text = reader("Coordinates").ToString()
            
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            ddlLang.SelectedValue = reader("Lang").ToString()
            hdnMasterID.Value = reader("MasterID")
           


        End If
        conn.Close()
    End Sub
End Class
