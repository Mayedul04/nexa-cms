﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false"
    CodeFile="LocationEdit.aspx.vb" Inherits="Admin_A_Partner_Partner" %>

<%--<%@ Register Assembly="FredCK.FCKeditorV2" Namespace="FredCK.FCKeditorV2" TagPrefix="FCKeditorV2" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">Contact Details</h1>
    <div class="btn-toolbar" style=" display:none">
        <a href="../A-Contact-Address/AllLocations.aspx?TName=<%= Request.QueryString("TName") %>&TID=<%= Request.QueryString("TID") %>&t=<%= Request.QueryString("t") %>"  data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add New"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <label class="success-left-top"></label>
            <label class="success-right-top"></label>
            <label
                class="success-left-bot">
            </label>
            <label class="success-right-bot"></label>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <label class="error-left-top"></label>
            <label class="error-right-top"></label>
            <label class="error-left-bot">
            </label>
            <label class="error-right-bot"></label>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            
            <p>
                <label>
                    Name:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            <p>
                <label>
                    Email:</label>
                <asp:TextBox ID="EmailTextBox" runat="server" class="txt" MaxLength="200" />
                <label class="red">
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="EmailTextBox"
                        ErrorMessage="RequiredFieldValidator" SetFocusOnError="True" ValidationGroup="form">*</asp:RequiredFieldValidator>--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="EmailTextBox"
                        ErrorMessage="RegularExpressionValidator" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ValidationGroup="form">*</asp:RegularExpressionValidator>
                </label>
            </p>
            <p>
                <label>
                    Website:</label>
                <asp:TextBox ID="txtWebsite" runat="server" class="txt" MaxLength="200" />
                <label class="red">
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator33" runat="server" ControlToValidate="EmailTextBox"
                        ErrorMessage="RequiredFieldValidator" SetFocusOnError="True" ValidationGroup="form">*</asp:RequiredFieldValidator>--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtWebsite"
                        ErrorMessage="RegularExpressionValidator" ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"
                        ValidationGroup="form">*</asp:RegularExpressionValidator>
                </label>
            </p>
            <p>
                <label>
                    Phone:
                </label>
                <asp:TextBox ID="PhoneTextBox" runat="server" class="txt" MaxLength="30" />
                <label>
                </label>
            </p>
           
            <p>
                <label>
                    International Phone:
                </label>
                <asp:TextBox ID="txtIntPhone" runat="server" class="txt" MaxLength="30" />
                <label>
                </label>
            </p>
             <p>
                 <label>
                   All Together:
                </label>
                <asp:TextBox ID="txtDetails" runat="server" class="txt" TextMode="MultiLine" />
                <script>

                    // Replace the <textarea id="editor1"> with a CKEditor
                    // instance, using default configuration.

                    CKEDITOR.replace('<%=txtDetails.ClientID %>',
                                    {
                                        filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                        "extraPlugins": "imagebrowser",
                                        "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                                    }
                                            );

                    </script>
                <label>
                </label>
            </p>
             <p style="display:none;">
                 <label>
                    Head Office :
                </label>
                <asp:CheckBox ID="chkHeadOffice" Checked="true" runat="server" TextAlign="Left" Text="Make Head Office " />
                <label class="red">
                </label>
            </p>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>
            
            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>
            <p style="display:none;">
                <label>Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnTableID" runat="server" />
            <asp:HiddenField ID="hdnTableName" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>
        <div class="btn-toolbar">
            <%--<asp:Button ID="btnSubmit" runat="server" Text="<i class='icon-save'></i> Add New" validationgroup="form" class="btn btn-primary" />--%>
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
            <div class="btn-group">
            </div>
        </div>

        <asp:SqlDataSource ID="sdsAdd" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            InsertCommand="INSERT INTO [ContactDetails] ([Heading], [Phone], [IntPhone], [Email], [Website], [Status], [SortIndex], [TableName], [TableID], [Lang],[Details]) VALUES (@Heading, @Phone, @IntPhone, @Email, @Website, @Status, @SortIndex, @TableName, @TableID, 'en',@Details)"
            UpdateCommand="UPDATE [ContactDetails] SET [Heading] = @Heading, [Phone] = @Phone, [IntPhone] = @IntPhone, [Email] = @Email, [Website] = @Website, [Status] = @Status, [SortIndex] = @SortIndex, [Lang] =@Lang,[Details]=@Details  WHERE [ContactID] = @ContactID">
           
            <InsertParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Heading" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="PhoneTextBox" Name="Phone" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="EmailTextBox" Name="Email" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtWebsite" Name="Website" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="hdnTableName" Name="TableName" PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnTableID" Name="TableID" PropertyName="Value" Type="Int32" />
                <asp:ControlParameter ControlID="txtIntPhone" Name="IntPhone" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="Details" PropertyName="Text" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="txtTitle" Name="Heading" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="PhoneTextBox" Name="Phone" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="EmailTextBox" Name="Email" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="txtWebsite" Name="Website" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
                <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" Type="Int32" />
                <asp:ControlParameter ControlID="chkHeadOffice" Name="HeadOffice" PropertyName="Checked" Type="Boolean" />
                <asp:QueryStringParameter Name="ContactID" QueryStringField="lid" Type="Int32" />
                <asp:ControlParameter ControlID="txtIntPhone" Name="IntPhone" PropertyName="Text" Type="String" />
                <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" Type="String" />
                <asp:ControlParameter ControlID="txtDetails" Name="Details" PropertyName="Text" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>


</asp:Content>
