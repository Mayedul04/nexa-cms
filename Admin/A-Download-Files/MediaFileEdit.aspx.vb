﻿
Partial Class Admin_A_Media_Files_MediaFileEdit
    Inherits System.Web.UI.Page
    Protected smallImageWidth As String = "370", smallImageHeight As String = "213", bigImageWidth As String = "555", bigImageHeight As String = "310", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            If Not String.IsNullOrEmpty(Request.QueryString("fid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update File"
                LoadContent(Request.QueryString("fid"))
            Else
                hdnTableName.Value = Request.QueryString("TName").ToString()
                hdnTableID.Value = Request.QueryString("TID")
            End If

        End If

    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now


         If fufile.FileName <> "" Then
            hdnFilename.Value = Utility.AddOtherFiles(fufile, Utility.EncodeTitle(txtTitle.Text, "-") & "-File", Server)
        End If

        If hdnFilename.Value <> "" Then
            txtFileName.Text = "../" & hdnFilename.Value
        End If



        'hdnDetails.Value = FCKeditor1.Value

        If String.IsNullOrEmpty(Request.QueryString("fid")) Then
           
            If sdsFiles.Insert() > 0 Then
                sdsFiles.InsertParameters("Lang").DefaultValue = "ar"
                If sdsFiles.Insert > 0 Then
                    Response.Redirect("AllMediaFiles.aspx")
                Else
                    divError.Visible = True
                End If

            Else
                divError.Visible = True
            End If
        Else
            If sdsFiles.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    




    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT      *   FROM   MediaFiles where AlbumID=@AlbumID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("AlbumID", Data.SqlDbType.Int)
        cmd.Parameters("AlbumID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
             txtTitle.Text = reader("Title") & ""
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            hdnFilename.Value = reader("TableName").ToString()
            hdnTableID.Value = reader("TableID") & ""
            ddlLang.SelectedValue = reader("Lang") & ""
            hdnFilename.Value = reader("FileName").ToString()
            txtFileName.Text = hdnFilename.Value

        End If
        conn.Close()
    End Sub
End Class
