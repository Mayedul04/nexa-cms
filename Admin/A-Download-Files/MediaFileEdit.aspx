﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false" CodeFile="MediaFileEdit.aspx.vb" Inherits="Admin_A_Media_Files_MediaFileEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <h1 class="page-title">Media File</h1>
   
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add News"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <p>
                <label>
                    Title:</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                <label class="red">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form"
                        ControlToValidate="txtTitle" runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                </label>
            </p>
            
            <asp:Panel ID="pnlUploadFile" runat="server">
                <p>
                    <asp:TextBox ID="txtFileName" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" ReadOnly="true" />
                    <asp:HiddenField ID="hdnFilename" runat="server" />
                </p>
                <p>
                    <label>
                        Upload File  : </label>
                    <asp:FileUpload ID="fufile" runat="server" CssClass="input-xlarge" />
                    <label class="red">
                    </label>
                </p>
            </asp:Panel>

            
         
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                        MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                        ValidationGroup="form"></asp:RangeValidator>
                </label>
            </p>

            <p>
                <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                <label class="red">
                </label>
            </p>

            <p style=" display:none;">
                <label>
                    Language</label>
                <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                    DataTextField="LangFullName" DataValueField="Lang">
                </asp:DropDownList>
                <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
            </p>
            <asp:HiddenField ID="hdnTableName" runat="server" />
            <asp:HiddenField ID="hdnTableID" runat="server" />
            <asp:HiddenField ID="hdnUpdateDate" runat="server" />
        </div>

        <div class="btn-toolbar">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                <i class="icon-save"></i>Add New</button>

            <div class="btn-group">
            </div>
        </div>
    </div>
  <asp:SqlDataSource ID="sdsFiles" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [MediaFiles] WHERE [FileID] = @FileID" 
        InsertCommand="INSERT INTO [MediaFiles] ([Title], [TableName], [TableID], [FileName], [LastUpdated],  [Lang], [Status], [SortIndex]) VALUES (@Title, @TableName, @TableID,  @FileName, @LastUpdated,  @Lang, @Status, @SortIndex)" 
        UpdateCommand="UPDATE [MediaFiles] SET [Title] = @Title,  [FileName] = @FileName, [LastUpdated] = @LastUpdated,  [Status] = @Status, [SortIndex] = @SortIndex WHERE [FileID] = @FileID">
        <DeleteParameters>
            <asp:Parameter Name="FileID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnFilename" Name="FileName" PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value" Type="DateTime" />
            <asp:ControlParameter ControlID="hdnTableID" Name="TableID" PropertyName="Value" Type="Int32" />
            <asp:Parameter DefaultValue="en" Name="Lang" Type="String" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" Type="Int32" />
            <asp:ControlParameter ControlID="hdnTableName" Name="TableName" PropertyName="Value" DefaultValue="" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="hdnFilename" Name="FileName" PropertyName="Value" Type="String" />
            <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value" Type="DateTime" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked" Type="Boolean" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text" Type="Int32" />
            <asp:QueryStringParameter Name="FileID" QueryStringField="fid" Type="Int32" />
        </UpdateParameters>
     </asp:SqlDataSource>
    <!-- Eof content -->
</asp:Content>

