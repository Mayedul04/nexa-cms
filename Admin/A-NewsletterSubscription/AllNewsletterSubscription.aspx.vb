﻿
Partial Class Admin_A_NewsletterSubscription_AllNewsletterSubscription
    Inherits System.Web.UI.Page

    Protected Sub btnDownload_Click(sender As Object, e As System.EventArgs) Handles btnDownload.Click
        Utility.DownloadCSV(Utility.EncodeTitle("News Subscription " & DateTime.Now, "-") & ".csv", "SELECT * FROM [NewsletterSubscription] order by SubscriptionDate desc", Response)
    End Sub
End Class
