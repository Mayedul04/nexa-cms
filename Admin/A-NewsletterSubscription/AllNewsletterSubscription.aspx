﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllNewsletterSubscription.aspx.vb" Inherits="Admin_A_NewsletterSubscription_AllNewsletterSubscription" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1 class="page-title">Latest News Subscription Report</h1>

   <div class="btn-toolbar">
        <asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>
        <div class="btn-group">
        </div>
    </div>

    <!-- content -->

    <h2>
        <asp:Label ID="Label1" runat="server" Text="All  Subscriptions"></asp:Label></h2>
    <div>
        <div class="well">

            <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource1" 
                DataKeyNames="NewSubID">
                
                <EmptyDataTemplate>
                    <table runat="server" style="">
                        <tr>
                            <td>
                                No data was returned.</td>
                        </tr>
                    </table>
                </EmptyDataTemplate>
                
                <ItemTemplate>
                    <tr style="">
                        <td style="display:none " >
                            <asp:Label ID="NewSubIDLabel" runat="server" Text='<%# Eval("NewSubID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("FirstName")%>' />
                        </td>
                        <td>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("LastName")%>' />
                        </td>
                       
                        <td>
                            <asp:Label ID="EmailLabel" runat="server" Text='<%# Eval("Email") %>' />
                        </td>
                         <td>
                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("Mobile")%>' />
                        </td>
                         <td>
                            <asp:Label ID="Label5" runat="server" Text='<%# Eval("Phone")%>' />
                        </td>
                         <td>
                            <asp:Label ID="Label6" runat="server" Text='<%# Eval("Nationality")%>' />
                        </td>
                        <td>
                            <asp:Label ID="SubscriptionDateLabel" runat="server" 
                                Text='<%# Eval("SubscriptionDate", "{0:dd MMM yyyy hh:mm:ss tt}") %>' />
                        </td>
                        <td>

                            <a href='<%# "#" & Eval("NewSubID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                            <div class="modal small hide fade" id='<%# Eval("NewSubID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
    
                                <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete"  runat="server" Text="Delete" />
                                  
                                </div>
                            </div>
                         
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    
                                <table class="table">
                                    <thead>
                                        <tr id="Tr1" runat="server" style="">
                                            <th id="Th1" runat="server">
                                                First Name</th>
                                            <th id="Th4" runat="server">
                                                Last Name</th>
                                            <th id="Th5" runat="server">
                                                Email</th>

                                            <th id="Th2" runat="server">
                                                Mobile</th>
                                            <th id="Th6" runat="server">
                                                Phone</th>
                                             <th id="Th7" runat="server">
                                                Nationality</th>

                                            <th id="Th3" runat="server">
                                                Subscription Date</th>
                                            <th></th>
                                        </tr>
                                    
                                    </thead>
                                    <tr ID="itemPlaceholder" runat="server">
                                    </tr>
                                    
                                <div class="paginationNew pull-right">
                                <asp:DataPager ID="DataPager1" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link"  ShowFirstPageButton="False" ShowNextPageButton="False"  ShowPreviousPageButton="True"  />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />
                                                
                                    </Fields>
                                </asp:DataPager>
                                </div>
                            
                                </table>
                                
                            
                </LayoutTemplate>
                
            </asp:ListView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>" 
                DeleteCommand="DELETE FROM [NewsletterSubscription] WHERE [NewSubID] = @NewSubID" 
                SelectCommand="SELECT * FROM [NewsletterSubscription] order by SubscriptionDate desc">
                <DeleteParameters>
                    <asp:Parameter Name="NewSubID" Type="Int32" />
                </DeleteParameters>
               
            </asp:SqlDataSource>

        </div>
    </div>


    
</asp:Content>

