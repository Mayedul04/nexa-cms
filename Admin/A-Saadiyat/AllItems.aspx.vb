﻿
Partial Class Admin_A_Saadiyat_AllItems
    Inherits System.Web.UI.Page
    Protected Sub btnAddNew_ServerClick(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("Item.aspx")
    End Sub
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString
            Response.Cookies("backurlAdmin").Expires = Date.Today.AddDays(+1)
            ddlLang.DataBind()
            HTMLs.MasterIDs = "8,9,10,11,12,13,14,30,31"

        End If

    End Sub
End Class
