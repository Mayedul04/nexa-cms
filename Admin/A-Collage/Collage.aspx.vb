﻿
Partial Class Admin_A_Collage_Collage
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        

        If IsPostBack = False Then
            If Request.QueryString("contextKey") = "" Then
                If Not Request.Cookies("CollageID") Is Nothing Then
                    Response.Cookies("CollageID").Value = ""
                    Response.Cookies("CollageID").Expires = Date.Now.AddHours(-1)
                End If
                GetCollageID()
                Response.Cookies("Sorting").Value = 1
                Response.Cookies("Sorting").Expires = Date.Now.AddHours(+1)
            End If

        End If

    End Sub
    Public Sub GetCollageID()

        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT ID, Templete FROM Collages where TableName=@TableName and TableID=@TableID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("TableID", Data.SqlDbType.Int, 32).Value = Request.QueryString("TID")
        cmd.Parameters.Add("TableName", Data.SqlDbType.NVarChar, 100).Value = Request.QueryString("TName")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            Response.Cookies("CollageID").Value = reader("ID").ToString()
            Response.Cookies("CollageID").Expires = Date.Now.AddHours(+1)
            rbtTemplate.SelectedValue = reader("Templete").ToString()
        
        End If

    End Sub
    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.ServerClick
        Response.Cookies("CollageID").Value = ""
        Response.Cookies("CollageID").Expires = Date.Now.AddHours(-1)
        Response.Redirect(Request.Cookies("backurlAdmin").Value)
    End Sub
    Protected Sub AjaxFileUploadBigImage_UploadComplete(sender As Object, e As AjaxControlToolkit.AjaxFileUploadEventArgs) Handles AjaxFileUploadBigImage.UploadComplete
        Dim filePath As String = "~/Admin/Content/" & e.FileName
        lblError.Text &= filePath & "<br />"
        ' Save upload file to the file system
        AjaxFileUploadBigImage.SaveAs(Server.MapPath(filePath))
        e.DeleteTemporaryData()
        hdnImageName.Value = "Content/" & e.FileName
        hdnTitle.Value = e.FileName.Remove(e.FileName.LastIndexOf("."))

        If Not Request.Cookies("CollageID") Is Nothing Then
            If CollageItem.Insert() > 0 Then
                Dim count As Integer = Request.Cookies("Sorting").Value
                Response.Cookies("Sorting").Value = count + 1
                ListView1.DataBind()
            End If
        End If

    End Sub
    
    Protected Sub AjaxFileUploadBigImage_UploadCompleteAll(sender As Object, e As AjaxControlToolkit.AjaxFileUploadCompleteAllEventArgs) Handles AjaxFileUploadBigImage.UploadCompleteAll
        ListView1.DataBind()
    End Sub

    Protected Sub rbtTemplate_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rbtTemplate.SelectedIndexChanged
        If Not Request.Cookies("CollageID") Is Nothing Then
            sdsCollage.Update()
        Else
            sdsCollage.Insert()
            GetCollageID()
        End If
    End Sub
End Class
