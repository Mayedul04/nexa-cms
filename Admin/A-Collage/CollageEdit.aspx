﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false" CodeFile="CollageEdit.aspx.vb" Inherits="Admin_A_Collage_CollageEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h1 class="page-title">Collage Items</h1>
    <div class="btn-toolbar">
       

        <div class="btn-group">
        </div>
    </div>
    <div>
       
        <div class="well">
            Please Selcet one of the Template
            <br />
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                     
                    <asp:RadioButtonList ID="rbtTemplate" runat="server" RepeatDirection="Vertical" TextAlign="RIGHT" RepeatLayout="Flow" CellSpacing="10" AutoPostBack="True">
                        <asp:ListItem Value="1">
                <img src="../assets/images/images/layout1.png" width="400" style="padding:5px" />
                        </asp:ListItem>
                        
                        <asp:ListItem Value="3">
                <img src="../assets/images/images/layout2.png" width="400" style="padding:5px" />
                        </asp:ListItem>
                        <asp:ListItem Value="4">
                <img src="../assets/images/images/layout3.png" width="400" style="padding:5px" />
                        </asp:ListItem>
                        <asp:ListItem Value="2">
                                Slider
                        </asp:ListItem>
                    </asp:RadioButtonList>
                </ContentTemplate>
            </asp:UpdatePanel>

            <p>
                <label>Multiple image upload (Please follow the sequence of the image and Dimentions)</label>
               
            </p>
            <asp:AjaxFileUpload ID="AjaxFileUploadBigImage" runat="server" ThrobberID="MyThrobber" AllowedFileTypes="jpg,jpeg,gif,png" />
            <asp:Image
                ID="MyThrobber"
                ImageUrl="ajax-loader.gif"
                Style="display: None"
                runat="server" />
            <asp:Label ID="lblError" runat="server"></asp:Label>
            <asp:HiddenField ID="hdnImageName" runat="server" />
            <asp:HiddenField ID="hdnTitle" runat="server" />
            <asp:HiddenField ID="hdnSort" runat="server"  />
            <asp:HiddenField runat="server" ID="hdnCollageID" Value="-1"></asp:HiddenField>
            <p>&nbsp;</p>
            <asp:ListView ID="ListView1" runat="server" DataKeyNames="IteamID"
                DataSourceID="CollageItem">

                <EmptyDataTemplate>
                    No data was returned.
                </EmptyDataTemplate>

                <ItemTemplate>
                    <li class="thumbnail">
                        
                        <img  src='<%# If(IsDBNull(Eval("CollageImage")), "../Content/Noimages.jpg", "../" & Eval("CollageImage"))%>' width="88%" />

                        
                        <br />
                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Edit"><i class="icon-pencil"></i></asp:LinkButton>
                        
                        &nbsp
                              <a href='<%# "#" & Eval("IteamID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                        <div class="modal small hide fade" id='<%# Eval("IteamID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Delete Confirmation</h3>
                            </div>
                            <div class="modal-body">

                                <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server" Text="Delete" />

                            </div>
                        </div>
                    </li>





                </ItemTemplate>
                <EditItemTemplate>
                    <li class="thumbnail">
                        <img  src='<%# If(IsDBNull(Eval("CollageImage")), "../Content/Noimages.jpg", "../" & Eval("CollageImage"))%>' width="60%" />
                        <br/>
                        Sort Order

                        <asp:TextBox ID="TextBox1" Text='<%# Bind("SortIndex") %>' runat="server"></asp:TextBox>
                        <br/>
                        <asp:LinkButton ID="LinkButton2" runat="server" CommandName="Update">Update</asp:LinkButton>
                        &nbsp
                        <asp:LinkButton ID="LinkButton3" runat="server" CommandName="Cancel">Cancel</asp:LinkButton>
                        </li>
                </EditItemTemplate>
                <LayoutTemplate>
                    <ul id="itemPlaceholderContainer" runat="server" class="thumbnails gallery">
                        <li runat="server" id="itemPlaceholder" />
                    </ul>
                    <div style="">
                    </div>
                </LayoutTemplate>

            </asp:ListView>
        </div>
        <asp:SqlDataSource ID="sdsCollage" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" DeleteCommand="DELETE FROM [Collages] WHERE [ID] = @ID" InsertCommand="INSERT INTO [Collages] ([TableName], [TableID], [Templete], [Status]) VALUES (@TableName, @TableID, @Templete, @Status)" SelectCommand="SELECT * FROM [Collages]" UpdateCommand="UPDATE [Collages] SET  [Templete] = @Templete  WHERE [ID] = @ID">
            <DeleteParameters>
                <asp:Parameter Name="ID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:QueryStringParameter Name="TableName" QueryStringField="TName" Type="String" />
                <asp:QueryStringParameter Name="TableID" QueryStringField="TID" Type="Int32" />
                <asp:ControlParameter ControlID="rbtTemplate" Name="Templete" PropertyName="SelectedValue" Type="Int32" />
                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
            </InsertParameters>
            <UpdateParameters>
                <asp:ControlParameter ControlID="rbtTemplate" Name="Templete" PropertyName="SelectedValue" Type="Int32" />
                <asp:CookieParameter CookieName="CollageID" Name="ID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
        <asp:SqlDataSource ID="CollageItem" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [CollageIteams] WHERE [IteamID] = @IteamID"
            InsertCommand="INSERT INTO [CollageIteams] ([CollageID], [Title], [CollageImage], [Status], [SortIndex]) VALUES (@CollageID, @Title, @CollageImage, @Status, @SortIndex)"
            SelectCommand="SELECT * FROM [CollageIteams] where CollageID=@CollageID order by SortIndex"
            UpdateCommand="UPDATE [CollageIteams] SET  [SortIndex] = @SortIndex WHERE [IteamID] = @IteamID">
            <DeleteParameters>
                <asp:Parameter Name="IteamID" Type="Int32" />
            </DeleteParameters>
            <InsertParameters>
                <asp:CookieParameter CookieName="CollageID" Name="CollageID" Type="Int32" />
                <asp:ControlParameter ControlID="hdnTitle" Name="Title" PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="hdnImageName" Name="CollageImage" PropertyName="Value" Type="String" />
                <asp:Parameter DefaultValue="True" Name="Status" Type="Boolean" />
                <asp:CookieParameter CookieName="Sorting" DefaultValue="" Name="SortIndex" Type="Int32" />
            </InsertParameters>
            <SelectParameters>
                <asp:CookieParameter CookieName="CollageID" Name="CollageID" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="SortIndex" Type="Int32" />
                <asp:Parameter Name="IteamID" Type="Int32" />
            </UpdateParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>

