﻿
Partial Class Admin_A_News_News
    Inherits System.Web.UI.Page


    Protected smallImageWidth As String = "165", smallImageHeight As String = "99", bigImageWidth As String = "555", bigImageHeight As String = "310", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        '  Utility.GetDimentionSetting("News", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, videoWidth, videoHeight)
        If Not IsPostBack Then

            
            If Not String.IsNullOrEmpty(Request.QueryString("nid")) Then
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
                lblTabTitle.Text = "Update News"
                LoadContent(Request.QueryString("nid"))
                ''ddlType.DataBind()
                If ddlLang.SelectedValue = "ar" Then
                    btnSubmit.InnerHtml = "<i class='icon-save'></i>  Update Arabic"
                    LoadEnImages()
                End If
            Else
                If Request.QueryString("cat").ToString() = "1" Then
                    ddlCat.SelectedValue = "Featured"
                    chkFeatured.Checked = True
                    pnlNewsType.Visible = False
                    pnlPublisher.Visible = True
                    pnlDetails.Visible = True
                    pnlFile.Visible = True
                ElseIf Request.QueryString("cat").ToString() = "2" Then
                    ddlCat.SelectedValue = "Press"
                    pnlFile.Visible = True
                    pnlNewsType.Visible = False
                    pnlDetails.Visible = False
                    pnlLink.Visible = False
                    pnlPublisher.Visible = True
                Else
                    ddlCat.SelectedValue = "News"
                    pnlNewsType.Visible = True
                    pnlPublisher.Visible = True
                    pnlDetails.Visible = True
                End If
            End If
            
        End If

    End Sub
    Protected Sub ddlType_DataBound(sender As Object, e As System.EventArgs) Handles ddlType.DataBound
        ddlType.Items.Insert(0, New ListItem("<- Select ->", ""))
        '  ddlType.Items.Add("Other")
    End Sub
    Private Sub LoadEnImages()
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT BigImage, SmallImage, Link from List_News where MasterID=@MasterID and Lang=@Lang "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("MasterID", Data.SqlDbType.Int)
        cmd.Parameters("MasterID").Value = hdnMasterID.Value
        cmd.Parameters.Add("Lang", Data.SqlDbType.NVarChar, 50).Value = "en"

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()


            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""

            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If



            txtLinkTextBox.Text = reader("Link") & ""

          


        End If
        conn.Close()
    End Sub
    Protected Sub btnSubmit_Click(sender As Object, e As System.EventArgs) Handles btnSubmit.ServerClick
        hdnUpdateDate.Value = DateTime.Now

        If fuFile.FileName <> "" Then
            hdnFile.Value = Utility.UploadFile(fuFile, "File-")
            lblFile.Text = hdnFile.Value
        End If

        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
        End If
        If hdnSmallImage.Value <> "" Then
            imgSmallImage.Visible = True
            imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
        End If
        If hdnBigImage.Value <> "" Then
            imgBigImage.Visible = True
            imgBigImage.ImageUrl = "../" & hdnBigImage.Value
        End If


        'hdnDetails.Value = FCKeditor1.Value

        If String.IsNullOrEmpty(Request.QueryString("nid")) Or Request.QueryString("new") = 1 Then
            If Request.QueryString("new") <> "1" Then
                hdnMasterID.Value = GetMasterID()
            End If

            If sdsNews.Insert() > 0 Then
                InsertIntoSEO()
                Response.Redirect("AllNews.aspx?cat=" & Request.QueryString("cat"))
            Else
                divError.Visible = True
            End If
        Else
            If sdsNews.Update() > 0 Then
                divSuccess.Visible = True
            Else
                divError.Visible = False
            End If
        End If

    End Sub

    Protected Function GetMasterID() As String
        Dim retVal As String = ""
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT (Max( MasterID)+1) as MaxMasterID  FROM   List_News "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)


        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            retVal = If(reader("MaxMasterID") & "" = "", "1", reader("MaxMasterID") & "")
        End If
        conn.Close()

        Return retVal
    End Function

    Protected Sub InsertIntoSEO()
        '; INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID], [Lang]) VALUES (@Title, @SmallDetails, @SmallDetails,1, 'HTMLChild', @PageID, @Lang)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "INSERT INTO [SEO] ([SEOTitle], [SEODescription], [SEOKeyWord], [SEORobot], [PageType], [PageID],lang) SELECT top 1  List_News.Title,List_News.SmallDetails,List_News.SmallDetails,1,'News', List_News.NewsID,List_News.Lang   FROM List_News   order by List_News.NewsID  desc "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)

        cmd.CommandText = selectString
        cmd.ExecuteNonQuery()

        conn.Close()
    End Sub


    Private Sub LoadContent(id As String)
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT  *  FROM   List_News where NewsID=@NewsID  "
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("NewsID", Data.SqlDbType.Int)
        cmd.Parameters("NewsID").Value = id

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()

        If reader.HasRows Then
            reader.Read()
            ddlCat.SelectedValue = reader("Category").ToString()
            txtTitle.Text = reader("Title") & ""
            txtSmallDetails.Text = reader("SmallDetails") & ""
            txtDetails.Text = reader("BigDetails") & ""
            hdnSmallImage.Value = reader("SmallImage") & ""
            hdnBigImage.Value = reader("BigImage") & ""
            hdnFile.Value = reader("FileUploaded").ToString()
            If hdnSmallImage.Value <> "" Then
                imgSmallImage.Visible = True
                imgSmallImage.ImageUrl = "../" & hdnSmallImage.Value
            End If
            If hdnBigImage.Value <> "" Then
                imgBigImage.Visible = True
                imgBigImage.ImageUrl = "../" & hdnBigImage.Value
            End If

            If hdnFile.Value <> "" Then
                lblFile.Text = hdnFile.Value
                lblFile.Visible = True
            End If
            txtLinkTextBox.Text = reader("Link") & ""
            'hdnLink.Value = reader("Link") & ""
            Boolean.TryParse(reader("Featured") & "", chkFeatured.Checked)
            txtSortIndex.Text = reader("SortIndex") & ""
            Boolean.TryParse(reader("Status") & "", chkStatus.Checked)

            Try
                ddlType.SelectedValue = reader("NewsType").ToString()
                txtCategory.Text = reader("NewsType").ToString()
            Catch ex As Exception

            End Try
            hdnUpdateDate.Value = reader("LastUpdated") & ""
            txtImgAlt.Text = reader("ImageAltText") & ""
            hdnMasterID.Value = reader("MasterID") & ""
            ddlLang.SelectedValue = reader("Lang") & ""
            txtNewsDate.Text = reader("ReportDate") & ""
            txtPublisher.Text = reader("Publisher").ToString()
        End If
        conn.Close()
    End Sub



    Protected Sub ddlCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCat.SelectedIndexChanged
        'If ddlCat.SelectedValue = "Press-Release" Then
        '    pnlFile.Visible = True
        '    pnlDetails.Visible = False
        '    pnlLink.Visible = True
        'Else
        '    pnlDetails.Visible = True
        '    pnlFile.Visible = False
        'End If
        If ddlCat.SelectedValue = "Featured" Then

            chkFeatured.Checked = True
            pnlNewsType.Visible = False
            pnlPublisher.Visible = True
            pnlDetails.Visible = True
        ElseIf ddlCat.SelectedValue = "Press" Then

            pnlFile.Visible = True
            pnlNewsType.Visible = False
            pnlDetails.Visible = False
            pnlLink.Visible = False
            pnlPublisher.Visible = True
        Else

            pnlNewsType.Visible = True
            pnlPublisher.Visible = True
            pnlDetails.Visible = True
        End If
    End Sub
End Class
