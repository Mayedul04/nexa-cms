﻿
Partial Class Admin_A_News_AllNews
    Inherits System.Web.UI.Page

    Protected Sub btnAddNew_Click(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("News.aspx?cat=3")
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Not Request.QueryString("cat") Is Nothing Then
                If Request.QueryString("cat").ToString() = "1" Then
                    sdsNews.SelectParameters.Item(1).DefaultValue = "Featured"
                Else
                    sdsNews.SelectParameters.Item(1).DefaultValue = "Press"
                End If
            End If
          
        End If
    End Sub
    
End Class
