﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllNews.aspx.vb" Inherits="Admin_A_News_AllNews" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">News</h1>

    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
        <div class="btn-group">
        </div>
    </div>

    <!-- content -->

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All News"></asp:Label></h2>
    <div>

        <p>
            <label>Language</label>
            <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true" DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang"></asp:DropDownList>
            <asp:SqlDataSource ID="sdsLang" runat="server"
                ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
        </p>

        <div class="well">

            <table class="table">

                <thead>
                    <tr>
                        <th>Title</th>

                        <th>Image</th>
                      
                        
                      
                        <th>Report Date </th>

                        <th>Type</th>

                        <th>Status</th>
                        <th>Sort Order</th>
                        <th style="width: 60px;"></th>
                    </tr>

                </thead>
                <tbody>
                    <asp:ListView ID="GridView1" runat="server" DataKeyNames="NewsID"
                        DataSourceID="sdsNews">
                        <EmptyDataTemplate>
                            <table id="Table1" runat="server" style="">
                                <tr>
                                    <td>No data was returned.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <ItemTemplate>
                            <tr style="">
                                <td>
                                    <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                                </td>
                                <td>
                                    <img src='<%# "../"& Eval("SmallImage") %>' alt="Edit" width="100" />
                                </td>
                                
                             

                                <td>
                                    <asp:Label ID="LastUpdatedLabel" runat="server"
                                        Text='<%# Convert.ToDateTime(Eval("ReportDate")).ToString("MMM dd, yyyy")%>' />
                                </td>
                              
                               
                                <td>
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("NewsType")%>' />
                                </td>
                                <td>
                                    <asp:Label ID="SortIndex" runat="server" Text='<%# Eval("Sortindex") %>' />
                                </td>
                                <td>
                                    <a href='<%# "News.aspx?nid=" & Eval("NewsID") %>' title="Edit"><i class="icon-pencil"></i></a>
                                    &nbsp
                              <a href='<%# "News.aspx?nid=" & Eval("NewsID")& "&new=1" %>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign"></i></a>
                                    &nbsp
                                <a href='<%# "#" & Eval("NewsID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                                    <div class="modal small hide fade" id='<%# Eval("NewsID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h3 id="myModalLabel">Delete Confirmation</h3>
                                        </div>
                                        <div class="modal-body">

                                            <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                            <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server" Text="Delete" />

                                        </div>
                                    </div>

                                </td>


                            </tr>
                        </ItemTemplate>
                        <LayoutTemplate>

                            <tr id="itemPlaceholder" runat="server">
                            </tr>

                            <div class="paginationNew pull-right">
                                <asp:DataPager ID="DataPager1" PageSize="5" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False" ShowPreviousPageButton="True" />
                                        <asp:NumericPagerField />
                                        <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True" ShowPreviousPageButton="False" />



                                    </Fields>
                                </asp:DataPager>
                            </div>

                        </LayoutTemplate>

                    </asp:ListView>

                </tbody>
            </table>


        </div>
        <!-- Eof content -->

        <asp:SqlDataSource ID="sdsNews" runat="server"
            ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT List_News.NewsID, List_News.Title, List_News.Category, List_News.NewsType, List_News.SmallDetails, List_News.BigDetails, List_News.SmallImage, List_News.BigImage, List_News.Link, List_News.Featured, List_News.SortIndex, List_News.Status, List_News.ReportDate, List_News.MasterID, List_News.Lang  FROM List_News INNER JOIN Languages ON List_News.Lang = Languages.Lang where List_News.Lang=@Lang and Category=@Category ORDER BY List_News.MasterID DESC, Languages.SortIndex "
            DeleteCommand="DELETE FROM List_News WHERE (NewsID = @NewsID)">
            <DeleteParameters>
                <asp:Parameter Name="NewsID" />
            </DeleteParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="ddlLang" Name="Lang"
                    PropertyName="SelectedValue" />
                <asp:Parameter DefaultValue="News" Name="Category" />
            </SelectParameters>
        </asp:SqlDataSource>

    </div>
    <!-- Eof content -->
</asp:Content>

