﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false"
    CodeFile="NewsEdit.aspx.vb" Inherits="Admin_A_News_News" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
     <script>
         $(document).ready(function () {

             $('.HowDidYouHearUs').change(function () {
                 // alert($("select.HowDidYouHearUs option:selected").val());
                 if ($("select.HowDidYouHearUs option:selected").val() == "Other") {
                     $(".other-info").removeAttr("style");
                     $(".other-textbox").val("");
                 }
                 else {
                     $(".other-info").attr("style", "display:none;");
                     $(".other-textbox").val($("select.HowDidYouHearUs option:selected").val());
                 }
             });

         });
    </script>
    <div class="block">

        <div class="block-body">


            <%--<h1 class="page-title">News</h1>--%>


            <h2>
                <asp:Label ID="lblTabTitle" runat="server" Text="Add News"></asp:Label></h2>
            <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
            </div>
            <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
                </div>
            </div>

            <!-- content -->
            <div class="well">
                <div id="myTabContent" class="tab-content">

                    <p>
                        <label>Title:</label>
                        <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge" MaxLength="400"></asp:TextBox>
                        <label class="red">
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" ValidationGroup="form" ControlToValidate="txtTitle"
                                runat="server" ErrorMessage="* Requred"></asp:RequiredFieldValidator>
                        </label>
                    </p>
                    <p style="display: none;">
                        <label>Category:</label>
                        <asp:DropDownList ID="ddlCat" runat="server" AutoPostBack="true">
                            <asp:ListItem Value="Featured">Featured News</asp:ListItem>
                            <asp:ListItem Value="Press">Press Releases</asp:ListItem>
                            <asp:ListItem Value="News">News</asp:ListItem>
                        </asp:DropDownList>
                    </p>
                    <asp:Panel ID="pnlSmall" Visible="false" runat="server">
                        <p>
                            <asp:Image ID="imgSmallImage" runat="server" Width="100px" Visible="false" />
                            <asp:HiddenField ID="hdnSmallImage" runat="server" />
                        </p>
                        <p>
                            <label>Upload Small Image : (Width=<%= smallImageWidth%>; Height=<%= smallImageHeight%>)</label>
                            <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                            <label class="red">
                            </label>
                        </p>
                        <p>
                            <label>Small Details:</label>
                            <asp:TextBox ID="txtSmallDetails" runat="server" TextMode="MultiLine" CssClass="input-xlarge"
                                Rows="4"></asp:TextBox>
                            <label class="red">
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Display="Dynamic" runat="server" ControlToValidate="txtSmallDetails"
                                    ValidationExpression="^[\s\S\w\W\d\D]{0,1000}$" ErrorMessage="* character limit is 1000"
                                    ValidationGroup="form" SetFocusOnError="True"></asp:RegularExpressionValidator>
                            </label>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlNewsType" runat="server">
                        <p>
                            <label>News Type:</label>
                            <asp:DropDownList ID="ddlType" runat="server"
                                CssClass="HowDidYouHearUs input-xlarge">
                                <asp:ListItem>General</asp:ListItem>
                                <asp:ListItem>Development</asp:ListItem>
                                <asp:ListItem>Environmental</asp:ListItem>
                                <asp:ListItem>Educational</asp:ListItem>
                                <asp:ListItem>Residential</asp:ListItem>
                                <asp:ListItem>CSR</asp:ListItem>
                            </asp:DropDownList>
                            
                            <asp:TextBox ID="txtCategory" runat="server" Style="display: none" CssClass="input-xlarge other-textbox other-info"></asp:TextBox>



                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlBig" Visible="false" runat="server">
                        <p>
                            <asp:Image ID="imgBigImage" runat="server" Width="140px" Visible="false" />
                            <asp:HiddenField ID="hdnBigImage" runat="server" />
                        </p>
                        <p>
                            <label>Upload Big Image :(Width=<%= bigImageWidth%>; Height=<%= bigImageHeight%>)</label>
                            <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                            <label class="red">
                            </label>
                        </p>
                        <p>
                            <label>Image Alt Text</label>
                            <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
                        </p>
                        <p>
                            <label>Details:</label>
                            <asp:TextBox ID="txtDetails" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <script>

                                // Replace the <textarea id="editor1"> with a CKEditor
                                // instance, using default configuration.

                                CKEDITOR.replace('<%=txtDetails.ClientID %>',
                                    {
                                        filebrowserImageUploadUrl: '../ckeditor/Upload.ashx', //path to “Upload.ashx”
                                        "extraPlugins": "imagebrowser",
                                        "imageBrowser_listUrl": '<%= "http://" & Context.Request.Url.Host & If(Context.Request.Url.Host = "localhost", ":" & Context.Request.Url.Port, "") & Context.Request.Url.AbsolutePath.Remove(Context.Request.Url.AbsolutePath.ToLower().IndexOf("/admin/")) & "/Admin/ckeditor/Browser.ashx" %>'
                                    }
                                        );

                            </script>
                        </p>
                    </asp:Panel>

                    <asp:Panel ID="pnlLink" Visible="false" runat="server">
                        <p>
                            <label>Link:</label>

                            <asp:TextBox ID="txtLinkTextBox" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />


                        </p>
                    </asp:Panel>
                    <p style="display: none;">
                        <asp:CheckBox ID="chkFeatured" runat="server" Text="Featured" TextAlign="Left" />
                        <label class="red">
                        </label>
                    </p>
                    <asp:Panel ID="pnlFile" runat="server">
                        <p>
                            <asp:Label ID="lblFile" Visible="false" runat="server" Text=""></asp:Label>
                            <asp:HiddenField ID="hdnFile" runat="server" />
                        </p>
                        <p>
                            <label>
                                Upload File(PDF) :</label>
                            <asp:FileUpload ID="fuFile" runat="server" CssClass="input-xlarge" />
                            <label class="red">
                            </label>
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlPublisher" Visible="false" runat="server">
                        <p>
                            <label>
                                Publisher: 
                            </label>
                            <asp:TextBox ID="txtPublisher" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />
                        </p>
                    </asp:Panel>
                    <asp:Panel ID="pnlFooter" Visible="false" runat="server">
                        <p>
                            <label>News Date:</label>

                            <asp:TextBox ID="txtNewsDate" runat="server" Text="" CssClass="input-xlarge" MaxLength="400" />

                            <cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txtNewsDate" runat="server">
                            </cc1:CalendarExtender>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="form" runat="server" ControlToValidate="txtNewsDate" Display="Dynamic" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                        </p>

                        <p>
                            <label>Sort Order: </label>
                            <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                            <label class="red">
                                <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" SetFocusOnError="true"
                                    MinimumValue="1" MaximumValue="999999" runat="server" ErrorMessage="* range from 1 to 999999"
                                    ValidationGroup="form"></asp:RangeValidator>
                            </label>
                        </p>



                        <p>
                            <asp:CheckBox ID="chkStatus" Checked="true" runat="server" TextAlign="Left" Text="Status " />
                            <label class="red">
                            </label>
                        </p>

                    </asp:Panel>
                    <p>
                        <label>Language</label>
                        <asp:DropDownList ID="ddlLang" runat="server" DataSourceID="sdsLang" CssClass="input-xlarge"
                            DataTextField="LangFullName" DataValueField="Lang">
                        </asp:DropDownList>
                        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
                    </p>
                    <asp:HiddenField ID="hdnMasterID" runat="server" />
                    <asp:HiddenField ID="hdnUpdateDate" runat="server" />


                </div>
                <div class="btn-toolbar">
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary"><i class="icon-save"></i>Add New</button>

                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <!-- Eof content -->
            <asp:SqlDataSource ID="sdsNews" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                DeleteCommand="DELETE FROM [List_News] WHERE [NewsID] = @NewsID" InsertCommand="INSERT INTO List_News(Title, Category, SmallDetails, BigDetails, SmallImage, BigImage, Link, Featured, SortIndex, Status, LastUpdated, ImageAltText, MasterID, Lang,ReportDate, Publisher, FileUploaded) VALUES (@Title, @Category, @SmallDetails, @BigDetails, @SmallImage, @BigImage, @Link, @Featured, @SortIndex, @Status, @LastUpdated, @ImageAltText, @MasterID, @Lang,@ReportDate,@Publisher,@FileUploaded)"
                SelectCommand="SELECT * FROM [List_News]" UpdateCommand="UPDATE List_News SET Title = @Title, Category=@Category, SmallDetails = @SmallDetails, BigDetails = @BigDetails, SmallImage = @SmallImage, BigImage = @BigImage, Link = @Link, Featured = @Featured, SortIndex = @SortIndex, Status = @Status, Publisher=@Publisher, LastUpdated = @LastUpdated, ImageAltText = @ImageAltText, MasterID = @MasterID, Lang = @Lang, ReportDate=@ReportDate, FileUploaded=@FileUploaded , NewsType=@NewsType WHERE (NewsID = @NewsID)">
                <DeleteParameters>
                    <asp:Parameter Name="NewsID" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                        Type="Boolean" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                        Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="txtNewsDate" Name="ReportDate" PropertyName="Text"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="ddlCat" Name="Category" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="hdnFile" Name="FileUploaded" PropertyName="Value" />
                    <asp:ControlParameter ControlID="txtPublisher" Name="Publisher" PropertyName="Text" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtDetails" Name="BigDetails" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtLinkTextBox" Name="Link" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked"
                        Type="Boolean" />
                    <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                        Type="Int32" />
                    <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                        Type="Boolean" />
                    <asp:ControlParameter ControlID="hdnUpdateDate" Name="LastUpdated" PropertyName="Value"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                        Type="String" />
                    <asp:ControlParameter ControlID="txtNewsDate" Name="ReportDate" PropertyName="Text"
                        Type="DateTime" />
                    <asp:ControlParameter ControlID="txtPublisher" Name="Publisher" PropertyName="Text" />
                    <asp:ControlParameter ControlID="hdnMasterID" Name="MasterID" PropertyName="Value" />
                    <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
                    <asp:QueryStringParameter Name="NewsID" QueryStringField="nid" Type="Int32" />
                    <asp:ControlParameter ControlID="ddlCat" Name="Category" PropertyName="SelectedValue" />
                    <asp:ControlParameter ControlID="hdnFile" Name="FileUploaded" PropertyName="Value" />
                    <asp:ControlParameter ControlID="txtCategory" Name="NewsType" PropertyName="Text" />
                </UpdateParameters>
            </asp:SqlDataSource>






        </div>
    </div>


</asp:Content>
