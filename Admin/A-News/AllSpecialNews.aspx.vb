﻿
Partial Class Admin_A_News_AllSpacialNews
    Inherits System.Web.UI.Page
    Protected Sub btnAddNew_Click(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("News.aspx?cat=1")
    End Sub

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            If Not Request.QueryString("cat") Is Nothing Then
                If Request.QueryString("cat").ToString() = "1" Then
                    sdsNews.SelectParameters.Item(1).DefaultValue = "Featured"
                Else
                    sdsNews.SelectParameters.Item(1).DefaultValue = "Press"
                End If
            End If
        End If
    End Sub

    Protected Sub ddlCat_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCat.SelectedIndexChanged
        sdsNews.SelectCommand = "SELECT List_News.NewsID, List_News.Title, List_News.Category, List_News.NewsType, List_News.SmallDetails, List_News.BigDetails, List_News.SmallImage, List_News.BigImage, List_News.Link, List_News.Featured, List_News.SortIndex, List_News.Status, List_News.ReportDate, List_News.MasterID, List_News.Lang  FROM List_News INNER JOIN Languages ON List_News.Lang = Languages.Lang where List_News.Lang=@Lang and Category=@Category ORDER BY List_News.MasterID DESC, Languages.SortIndex "
        sdsNews.SelectParameters.Clear()
        sdsNews.SelectParameters.Add("Lang", ddlLang.SelectedValue)
        sdsNews.SelectParameters.Add("Category", ddlCat.SelectedValue)
        GridView1.DataBind()
    End Sub
End Class
