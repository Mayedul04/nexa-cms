﻿
Partial Class AllPromotion
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load

        If Request.Cookies("backurlAdmin") Is Nothing Then
            Dim backurlAdmin = New HttpCookie("backurlAdmin", Request.Url.ToString())
            Response.Cookies.Add(backurlAdmin)
        Else
            Response.Cookies("backurlAdmin").Value = Request.Url.ToString()
        End If
    End Sub
    Protected Sub btnAddNew_Click(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("Promotion.aspx")
    End Sub

    Public Function GetUrl(PromotionId As Object) As String
        Dim url As String = "Promotion.aspx?PromotionId=" & PromotionId.ToString() & ""
        Return url
    End Function

End Class
