﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    ViewStateEncryptionMode="Never" EnableViewStateMac="false" CodeFile="AllPromotion.aspx.vb"
    Inherits="AllPromotion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">
        All Promotion</h1>
    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary"> <i class="icon-save"></i> Add New</button>
        <%--<asp:LinkButton ID="btnDownload" runat="server" CssClass="btn btn-primary" ><i class='icon-save'></i> Export</asp:LinkButton>--%>
        <div class="btn-group">
        </div>
    </div>
    <h2><asp:Label ID="lblTabTitle" runat="server" Text="Promotion List"></asp:Label></h2>
    <div>
        <div class="well">

            <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSourcePromotions" DataKeyNames="PromotionID">
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="ListIDLabel" runat="server" Text='<%# Eval("PromotionID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <asp:Image ID="imgThum" runat="server" Width="100px" ImageUrl='<%#"~/Admin/"+ Eval("SmallImage") %>' />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>
                        <%--<td>
                            <asp:CheckBox ID="FeaturedCheckBox" runat="server" Checked='<%# Eval("Featured") %>' Enabled="false" />
                        </td>--%>
                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" Checked='<%# Eval("Status") %>'
                                Enabled="false" />
                        </td>
                        <td>
                            <%--<a href="../A-CommonGallery/AllCommonGallery.aspx?TableName=List_Promotion&TableMasterID=<%# Eval("MasterID") %>&BigImageWidth=205&BigImageHeight=126&t=<%#  Server.UrlEncode(Eval("Title").ToString()) %>">Add/Edit Images</a>--%>
                            <a href="../A-Download-Files/AllMediaFiles.aspx?TName=List_Promotion&TID=<%# Eval("PromotionID")%>&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>" >Download Files<br/></a>
                        </td>
                        <td>
                            <asp:Label ID="MasterIDLabel" runat="server" Text='<%# Eval("MasterID") %>' />
                        </td>
                        <td>
                            <asp:Label ID="LangLabel" runat="server" Text='<%# Eval("Lang") %>' />
                        </td>
                        <td>
                            <a href='<%#  "Promotion.aspx?PromotionId=" & Eval("PromotionID")   %>' title="Edit"><i class="icon-pencil">
                            </i></a>&nbsp
                            <%--<a href='<%# "Promotions.aspx?PromotionId=" & Eval("PromotionID") & "&new=1"  %>' title="New Lang" style='<%# if(Eval("Lang")="en","","display:none;") %>'><i class="icon-plus-sign"></i> </a>&nbsp --%>
                            <a href='<%# "#" & Eval("PromotionID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("PromotionID") %>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">
                                        Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">
                                    ID
                                </th>
                                
                                <th id="Th3" runat="server">
                                    Title
                                </th>
                                <th id="Th4" runat="server">
                                    Small Image
                                </th>
                                <th id="Th5" runat="server">
                                    Sort Order
                                </th>
                                <%--<th id="Th6" runat="server">
                                    Featured
                                </th>--%>
                                <th id="Th7" runat="server">
                                    Status
                                </th>
                                <th>
                                    Links
                                </th>
                                <th id="Th8" runat="server">
                                    MasterID
                                </th>
                                <th id="Th9" runat="server">
                                    Lang
                                </th>
                                <th>
                                    Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
            </asp:ListView>


        </div>
    </div>


    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourcePromotions" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [List_Promotion] WHERE [PromotionID] = @PromotionID" SelectCommand="SELECT List_Promotion.*  FROM [List_Promotion] INNER JOIN Languages ON List_Promotion.Lang = Languages.Lang order by List_Promotion.LastUpdated Desc, List_Promotion.SortIndex">
        <DeleteParameters>
            <asp:Parameter Name="PromotionID" Type="Int32" />
        </DeleteParameters>
    </asp:SqlDataSource>
</asp:Content>
