﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false" CodeFile="AllLeisure.aspx.vb" Inherits="Admin_A_YourHome_AllYourHome" %>

<%@ Register Src="~/Admin/A-HTML/HTMLs.ascx" TagPrefix="uc1" TagName="HTMLs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    <h1 class="page-title">All Your Leisure</h1>

    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary"><i class="icon-save"></i> Add New</button>

        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Your Leisure"></asp:Label></h2>
    <p>
        <label>Language</label>
        <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true"
            DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang">
        </asp:DropDownList>
        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
    </p>
   
<div>
        <div class="well">

            <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSourceEvents" DataKeyNames="ListID">
                <ItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Label ID="ListIDLabel" runat="server" Text='<%# Eval("ListID")%>' />
                        </td>
                        <td>
                            <asp:Label ID="TitleLabel" runat="server" Text='<%# Eval("Title") %>' />
                        </td>
                        <td>
                            <asp:Image ID="imgThum" runat="server" Width="100px" ImageUrl='<%#"~/Admin/"+ Eval("SmallImage") %>' />
                        </td>
                        <td>
                            <asp:Label ID="SortIndexLabel" runat="server" Text='<%# Eval("SortIndex") %>' />
                        </td>

                        <td>
                            <asp:CheckBox ID="StatusCheckBox" runat="server" Checked='<%# Eval("Status") %>'
                                Enabled="false" />
                        </td>
                        <td>
                            <a style='<%# If(IsDBNull(Eval("GalleryID")) = false, "display:block", "display:none")%>' href="../A-Gallery/AllGalleryItem.aspx?galleryId=<%# Eval("GalleryID") %>&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>&BigImageWidth=275&BigImageHeight=183"> Gallery</a>

                        </td>


                        <td>
                            <a href="../A-Banner/AllTopBanner.aspx?TName=List_Leisure&TID=<%# Eval("ListID")%>&t=<%# Server.UrlEncode(Eval("Title").toString()) %>&BigImageWidth=1079&BigImageHeight=434" >Banners<br/></a>
                            <a href="../A-Content/AllContents.aspx?TName=List_Leisure&TID=<%# Eval("ListID")%>&t=<%# Server.UrlEncode(Eval("Title").toString()) & if(Eval("MasterID")="4" or Eval("MasterID")="5" or Eval("MasterID")="6","&ImageWidth=312&ImageHeight=208&Title=1&Image=1&Text=1&link=1" & if(Eval("MasterID")="6","&category=1","") , "&ImageWidth=478&ImageHeight=160&Title=0&Image=1&Text=0&link=0") %>" style='<%# if(Eval("MasterID")="4" or Eval("MasterID")="5" or Eval("MasterID")="6","","display:none;") %>' >Contents<br/></a>
                            <a href="../A-Download-Files/AllMediaFiles.aspx?TName=List_Leisure&TID=<%# Eval("ListID")%>&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>"  style='<%# if(Eval("MasterID")="4" or Eval("MasterID")="5" or Eval("MasterID")="6","display:none;","") %>' >Download Files<br/></a>
                            <a href="../A-Contact-Address/AllLocations.aspx?TName=List_Leisure&TID=<%# Eval("ListID")%>&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>"  style='<%# if(Eval("MasterID")="4" or Eval("MasterID")="5" or Eval("MasterID")="6","display:none;","") %>'>Quick Contact<br /></a>
                            <a href="../A-Testimonial/AllTestimonial.aspx?TName=List_Leisure&TID=<%# Eval("ListID")%>&image=0&t=<%# Utility.EncodeTitle(Eval("Title"),"-") %>"  style='<%# if(Eval("MasterID")="4" or Eval("MasterID")="5" or Eval("MasterID")="6","display:none;","") %>'>Testimonial</a>
                        </td>
                        <td>
                            <a href='<%# "Leisure.aspx?lId=" & Eval("ListID")%>' title="Edit"><i class="icon-pencil"></i></a>&nbsp
                            
                            <a href='<%# "#" & Eval("ListID") %>' data-toggle="modal"><i class="icon-remove"></i></a>
                            <div class="modal small hide fade" id='<%# Eval("ListID")%>' tabindex="-1" role="dialog"
                                aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        ×</button>
                                    <h3 id="myModalLabel">Delete Confirmation</h3>
                                </div>
                                <div class="modal-body">
                                    <p class="error-text">
                                        <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?
                                    </p>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn" data-dismiss="modal" aria-hidden="true">
                                        Cancel</button>
                                    <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                        Text="Delete" />
                                </div>
                            </div>
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table id="itemPlaceholderContainer" class="table">
                        <thead>
                            <tr id="Tr1" runat="server" style="">
                                <th id="Th1" runat="server">ID
                                </th>

                                <th id="Th3" runat="server">Title
                                </th>
                                <th id="Th4" runat="server">Small Image
                                </th>
                                <th id="Th5" runat="server">Sort Order
                                </th>

                                <th id="Th7" runat="server">Status
                                </th>
                                <th>Gallery
                                </th>
                                <th id="Th9" runat="server">Others
                                </th>
                                <th>Actions
                                </th>
                            </tr>
                        </thead>
                        <tr id="itemPlaceholder" runat="server">
                        </tr>
                        <div class="paginationNew pull-right">
                            <asp:DataPager ID="DataPager1" runat="server">
                                <Fields>
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowFirstPageButton="False" ShowNextPageButton="False"
                                        ShowPreviousPageButton="True" />
                                    <asp:NumericPagerField />
                                    <asp:NextPreviousPagerField ButtonType="Link" ShowLastPageButton="False" ShowNextPageButton="True"
                                        ShowPreviousPageButton="False" />
                                </Fields>
                            </asp:DataPager>
                        </div>
                    </table>
                </LayoutTemplate>
            </asp:ListView>


        </div>
    </div>


    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourceEvents" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [List_Leisure] WHERE [ListID] = @ListID" SelectCommand="SELECT List_Leisure.*  FROM [List_Leisure] where Lang=@Lang order by List_Leisure.SortIndex">
        <DeleteParameters>
            <asp:Parameter Name="ListID" Type="Int32" />
        </DeleteParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="ddlLang" Name="Lang" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
<uc1:HTMLs runat="server" ID="HTMLs" />
</asp:Content>


