﻿Imports System.Data.SqlClient
Imports System.Drawing.Imaging

Partial Class GalleryItemEdit
    Inherits System.Web.UI.Page

    Protected smallImageWidth As String = "285", smallImageHeight As String = "211", bigImageWidth As String = "Any", bigImageHeight As String = "Any", videoWidth As String = "", videoHeight As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Utility.GetDimentionSetting("GalleryItem", "", smallImageWidth, smallImageHeight, bigImageWidth, bigImageHeight, "", "")
        smallImageWidth = Request.QueryString("SmallImageWidth")
        smallImageHeight = Request.QueryString("SmallImageHeight")

        If IsPostBack = False Then

            If Request.QueryString("galleryItemId") <> "" Then
                rfvBigImage.Enabled = False
                rfvSmallImage.Enabled = False
                Dim strConn = ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString
                Dim sqlConn As SqlConnection = New SqlConnection(strConn)
                sqlConn.Open()
                Dim sqlcomm As SqlCommand = New SqlCommand()
                sqlcomm.CommandText = "SELECT * FROM GalleryItem where GalleryItemID=" & Request.QueryString("galleryItemId")
                sqlcomm.Connection = sqlConn
                sqlcomm.CommandType = Data.CommandType.Text
                Dim reader As SqlDataReader = sqlcomm.ExecuteReader()
                reader.Read()
                txtTitle.Text = reader("Title").ToString()
                txtArTitle.Text = reader("ArTitle").ToString()
                ddlType.SelectedValue = reader("ItemType").ToString()
                If ddlType.SelectedValue = "Online Video" Then
                    pnlVideo.Visible = True
                    pnlImage.Visible = False
                    pnlUploadVideo.Visible = False
                End If
                If ddlType.SelectedValue = "Uploaded Video" Then
                    pnlVideo.Visible = False
                    pnlImage.Visible = False
                    pnlUploadVideo.Visible = True
                End If
                If ddlType.SelectedValue = "Image" Then
                    pnlVideo.Visible = False
                    pnlImage.Visible = True
                    pnlUploadVideo.Visible = False
                    imgBigImage.Visible = True
                End If
                txtSmallDetails.Text = reader("SmallDetails").ToString()
                imgSmallImage.ImageUrl = "~/Admin/" + reader("SmallImage").ToString()
                hdnSmallImage.Value = reader("SmallImage").ToString()
                imgBigImage.ImageUrl = "~/Admin/" + reader("BigImage").ToString()
                hdnBigImage.Value = reader("BigImage").ToString()
                txtSortIndex.Text = reader("SortIndex").ToString()
                chkStatus.Checked = reader("Status").ToString()
                chkFeatured.Checked = reader("Featured").ToString()
                lbLastUpdated.Text = reader("LastUpdated").ToString()
                If IsDBNull(reader("VideoOriginalURL")) = False Then
                    txtVideoOriginalURL.Text = reader("VideoOriginalURL").ToString()
                End If
                If IsDBNull(reader("VideoImageURL")) = False Then
                    hdnVideoImageURL.Value = reader("VideoImageURL").ToString()
                End If
                If IsDBNull(reader("VideoEmbedCode")) = False Then
                    hdnVideoEmbedCode.Value = reader("VideoEmbedCode").ToString()
                    If hdnVideoEmbedCode.Value.ToString.Contains(".mp4") Then
                        hdnVideoFile.Value = hdnVideoEmbedCode.Value
                        hdnVideoEmbedCode.Value = ""
                        lblVideolink.Text = hdnVideoFile.Value
                    End If
                End If
                If IsDBNull(reader("VideoVCode")) = False Then
                    hdnVideoVCode.Value = reader("VideoVCode").ToString()
                End If
                sqlConn.Close()
                'imgSmallImage.Visible = True
                'imgBigImage.Visible = True
                'rfvSmallImage.Enabled = False
                'rfvBigImage.Enabled = False
                btnSubmit.InnerHtml = "<i class='icon-save'></i> Update"
            End If

        End If
    End Sub
    Public Function GetVcode(ByVal url As String) As String
        'http://www.youtube.com/watch?v=YVOquldmUXE&feature=player_embedded
        Dim vCode As String = ""
        If url.Contains("v=") Then
            Dim vIndex As Integer = url.IndexOf("v=")
            Dim ampIndex As Integer = url.IndexOf("&", vIndex)
            If (ampIndex > 0) Then
                vCode = url.Substring(vIndex + 2, ampIndex - (vIndex + 2))
            Else
                vCode = url.Substring(vIndex + 2)
            End If
        End If

        Return vCode
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.ServerClick
        '  Try
        lbLastUpdated.Text = DateTime.Now()

        If Not String.IsNullOrEmpty(txtVideoOriginalURL.Text) Then
            Dim video As VideoModule = New VideoModule()
            video.ProcessVideoURL(txtVideoOriginalURL.Text, videoWidth, videoHeight)
            hdnVideoEmbedCode.Value = video.VideoIFrame
            hdnVideoImageURL.Value = video.BigImage

            hdnVideoVCode.Value = GetVcode(txtVideoOriginalURL.Text)
        End If
        If hdnVideoImageURL.Value <> "" Then
            imgVideoImageURL.Visible = True
            imgVideoImageURL.ImageUrl = hdnVideoImageURL.Value
        End If
        If fuSmallImage.FileName <> "" Then
            hdnSmallImage.Value = Utility.AddImage(fuSmallImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Small", Utility.EncodeTitle(txtTitle.Text, "-") & "-Small"), Server)
            imgSmallImage.ImageUrl = "~/Admin/" + hdnSmallImage.Value
            imgSmallImage.Visible = True
        End If
        If fuBigImage.FileName <> "" Then
            hdnBigImage.Value = Utility.AddImage(fuBigImage, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Big", Utility.EncodeTitle(txtTitle.Text, "-") & "-Big"), Server)
            imgBigImage.ImageUrl = "~/Admin/" + hdnBigImage.Value
            imgBigImage.Visible = True
        End If
        If fuVideo.FileName <> "" Then

            hdnVideoFile.Value = Utility.UploadFile(fuVideo, If(txtImgAlt.Text <> "", Utility.EncodeTitle(txtImgAlt.Text, "-") & "-Video", Utility.EncodeTitle(txtTitle.Text, "-") & "-Video"), Server)
            lblVideolink.Text = hdnVideoFile.Value

        End If

        Dim a As Integer

        If Request.QueryString("galleryItemId") <> "" Then
            
            If ddlType.SelectedValue = "Uploaded Video" Then
                SqlDataSourceGalleryItem.UpdateParameters("VideoEmbedCode").DefaultValue = hdnVideoFile.Value
            End If
            a = SqlDataSourceGalleryItem.Update()
        Else
            If ddlType.SelectedValue = "Uploaded Video" Then
                SqlDataSourceGalleryItem.InsertParameters("VideoEmbedCode").DefaultValue = hdnVideoFile.Value
            End If
            a = SqlDataSourceGalleryItem.Insert()
            Response.Redirect("AllGalleryItemEdit.aspx?galleryId=" + Request.QueryString("galleryId") + "")
        End If

        If a > 0 Then
            divSuccess.Visible = True
            divError.Visible = False
        Else
            divSuccess.Visible = False
            divError.Visible = True
        End If
        '   Catch ex As Exception
        '  divSuccess.Visible = False
        'divError.Visible = True
        '    End Try
    End Sub



    Protected Sub ddlType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlType.SelectedIndexChanged
        If ddlType.SelectedValue = "Online Video" Then
            pnlVideo.Visible = True
            pnlImage.Visible = False
            pnlUploadVideo.Visible = False
        End If
        If ddlType.SelectedValue = "Uploaded Video" Then
            pnlVideo.Visible = False
            pnlImage.Visible = False
            pnlUploadVideo.Visible = True
        End If
    End Sub


End Class
