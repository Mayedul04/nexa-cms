﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/Main.master" AutoEventWireup="false"
    ViewStateEncryptionMode="Never" EnableViewStateMac="false" CodeFile="AllGallery.aspx.vb"
    Inherits="AllGallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <h1 class="page-title">Gallery</h1>

    <div class="btn-toolbar">
        <button runat="server" id="btnAddNew" class="btn btn-primary"><i class="icon-save"></i>Add New</button>
        <div class="btn-group">
            <input type="button" value="Back" class="btn btn-primary" onclick="window.history.go(-1); return false;" />
        </div>
    </div>

    <!-- content -->

    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Gallery Album"></asp:Label></h2>
    <p>
        <label>
            Language</label>
        <asp:DropDownList ID="ddlLang" runat="server" CssClass="input-xlarge" AutoPostBack="true"
            DataSourceID="sdsLang" DataTextField="LangFullName" DataValueField="Lang">
        </asp:DropDownList>
        <asp:SqlDataSource ID="sdsLang" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            SelectCommand="SELECT [Lang], [LangFullName] FROM [Languages] ORDER BY [SortIndex]"></asp:SqlDataSource>
    </p>
    <div>




        <div class="well">




            <asp:ListView ID="ListView1" runat="server" DataKeyNames="GalleryID"
                DataSourceID="SqlDataSourceGallery">

                <EmptyDataTemplate>
                    No data was returned.
                </EmptyDataTemplate>

                <ItemTemplate>
                    <li class="thumbnail">
                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Title") %>' />
                        <img style="width: 120px; height: 90px" src='<%# If(IsDBNull(Eval("SmallImage")), "../Content/Noimages.jpg", "../" & Eval("SmallImage"))%>' />

                        <br />
                        <br />
                        <a href='<%# "Gallery.aspx?galleryId=" & Eval("GalleryID") %>' title="Edit"><i class="icon-pencil"></i></a>
                        
                        &nbsp
                            <a href='<%# "AllGalleryItem.aspx?galleryId=" & Eval("GalleryID") %>' title="View/Add Gallery Item"><i class="icon-list"></i></a>
                        &nbsp
                                <a href='<%# "#" & Eval("GalleryID") %>' data-toggle="modal"><i class="icon-remove"></i></a>

                        <div class="modal small hide fade" id='<%# Eval("GalleryID") %>' tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                <h3 id="myModalLabel">Delete Confirmation</h3>
                            </div>
                            <div class="modal-body">

                                <p class="error-text"><i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?</p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
                                <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server" Text="Delete" />

                            </div>
                        </div>
                    </li>





                </ItemTemplate>
                <LayoutTemplate>
                    <ul id="itemPlaceholderContainer" runat="server" class="thumbnails gallery">
                        <li runat="server" id="itemPlaceholder" />
                    </ul>
                    <div style="">
                    </div>
                </LayoutTemplate>

            </asp:ListView>

        </div>



        <!-- Eof content -->
        <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [Gallery] WHERE [GalleryID] = @GalleryID"
            SelectCommand="SELECT Gallery.* FROM [Gallery] INNER JOIN Languages ON Gallery.Lang = Languages.Lang where ParentGalleryID=@PGalleryID and Gallery.Lang=@Lang ORDER BY Gallery.MasterID DESC, Languages.SortIndex"
            ProviderName="System.Data.SqlClient">
            <DeleteParameters>
                <asp:Parameter Name="GalleryID" Type="Int32" />
            </DeleteParameters>
            <SelectParameters>
                <asp:QueryStringParameter Name="PGalleryID" QueryStringField="pgid" DefaultValue="0" />
                <asp:ControlParameter ControlID="ddlLang" DefaultValue="en" Name="Lang" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>


    </div>




    <!-- Eof content -->



</asp:Content>
