﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false" CodeFile="AllGalleryItemEdit.aspx.vb" Inherits="Admin_A_Gallery_AllGalleryItemEdit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<script>
    function UploadComplete(sender, args) {
        window.location.assign("<%= Request.Url.ToString %>")
    }

    function uploadError(sender, args) {
        document.getElementById('<%= lblStatus.ClientID%>').innerHTML = "<br />" + args.get_fileName() + "<span style='color:red;'>" + args.get_errorMessage() + "</span>";
    }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <h1 class="page-title">Gallery Items</h1>
    <div class="btn-toolbar">
        <button runat="server" id="Button1" class="btn btn-primary" style="display:none;">
            <i class="icon-step-backward"></i> Back</button>
        <button runat="server" id="btnAddNew" class="btn btn-primary">
            <i class="icon-save"></i> Add New</button>
      
        <div class="btn-group">
        </div>
    </div>
    <!-- content -->
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="All Gallery Item"></asp:Label></h2>
    <div>
        <div class="well">
            <p>
                <label>Multiple image upload (Big Image  Width=793; Big Image Height=459)</label>
                <asp:Literal ID="lblStatus" runat="server"></asp:Literal>
            </p>
            <asp:AjaxFileUpload ID="AjaxFileUploadBigImage" runat="server" ThrobberID="MyThrobber" AllowedFileTypes="jpg,jpeg,gif,png" OnClientUploadError="uploadError" OnClientUploadCompleteAll="UploadComplete"  />
            <asp:Image
                ID="MyThrobber"
                ImageUrl="ajax-loader.gif"
                Style="display: None"
                runat="server" />
            <asp:Label ID="lblError" runat="server"></asp:Label>
            <asp:HiddenField ID="hdnImageName" runat="server" />
            <asp:HiddenField ID="hdnTitle" runat="server" />
            <asp:HiddenField ID="hdnLastUpdated" runat="server" />
           
            <p>&nbsp;</p>


            <asp:ListView ID="ListView1" runat="server" DataKeyNames="GalleryItemID" DataSourceID="SqlDataSourceGalleryItem">
                <EmptyDataTemplate>
                    No data was returned.
                </EmptyDataTemplate>
                <ItemTemplate>
                    <li class="thumbnail">

                        <asp:Label ID="Label1" runat="server" Text='<%# Eval("Title")& " -"& Eval("ItemType") %>' />
                        <img class="grayscale" src='<%# "../"& Eval("SmallImage") %>' width="140px" alt='<%# Eval("Title")  %>'>

                        <a href='<%# "GalleryItem.aspx?galleryItemId=" & Eval("GalleryItemID") & "&galleryId=" & Request.QueryString("galleryId") %>&SmallImageWidth=275&SmallImageHeight=183'
                            title="Edit"><i class="icon-pencil"></i></a>&nbsp <a href='<%# "#" & Eval("GalleryItemID") %>'
                                data-toggle="modal"><i class="icon-remove"></i></a>
                        <div class="modal small hide fade" id='<%# Eval("GalleryItemID") %>' tabindex="-1"
                            role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    ×</button>
                                <h3 id="myModalLabel">Delete Confirmation</h3>
                            </div>
                            <div class="modal-body">
                                <p class="error-text">
                                    <i class="icon-warning-sign modal-icon"></i>Are you sure you want to delete?
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button class="btn" data-dismiss="modal" aria-hidden="true">
                                    Cancel</button>
                                <asp:LinkButton ID="cmdDelete" CssClass="btn btn-danger" CommandName="Delete" runat="server"
                                    Text="Delete" />
                            </div>
                        </div>
                    </li>
                </ItemTemplate>
                <LayoutTemplate>
                    <ul id="itemPlaceholderContainer" runat="server" class="thumbnails gallery">
                        <li runat="server" id="itemPlaceholder" />
                    </ul>
                    <div style="">
                    </div>
                </LayoutTemplate>
            </asp:ListView>
        </div>
        <asp:SqlDataSource ID="SqlDataSourceGalleryItem" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
            DeleteCommand="DELETE FROM [GalleryItem] WHERE [GalleryItemID] = @GalleryItemID"
            InsertCommand="INSERT INTO [GalleryItem] ([GalleryID], [Title], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status],  [LastUpdated],ItemType) VALUES (@GalleryID, @Title,  @SmallImage, @BigImage, @ImageAltText, @SortIndex, @Status, @LastUpdated,'Image')"
            SelectCommand="SELECT * FROM [GalleryItem] where GalleryID=@GalleryID  order by SortIndex">
            <DeleteParameters>
                <asp:Parameter Name="GalleryItemID" Type="Int32" />
            </DeleteParameters>
            <SelectParameters>
                <asp:QueryStringParameter Name="GalleryID" QueryStringField="galleryId" Type="Int32" />
            </SelectParameters>
            <InsertParameters>
               
                <asp:ControlParameter ControlID="hdnTitle" Name="Title" PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnImageName" Name="SmallImage"
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnImageName" Name="BigImage"
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnTitle" Name="ImageAltText"
                    PropertyName="Value" />
                <asp:ControlParameter ControlID="hdnLastUpdated" Name="LastUpdated"
                    PropertyName="Value" />
                <asp:Parameter DefaultValue="1" Name="SortIndex" Type="Int32" />
                <asp:Parameter DefaultValue="True" Name="Status" />


                <asp:CookieParameter CookieName="GalID" Name="GalleryID" />


            </InsertParameters>
        </asp:SqlDataSource>
    </div>
    <!-- Eof content -->
</asp:Content>
