﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master" AutoEventWireup="false" CodeFile="GalleryCategory.aspx.vb" Inherits="Admin_A_Gallery_GalleryCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div class="block">
        <div class="block-heading">
            Gallery Category
        </div>
        <div class="block-body">
        
                <asp:Label ID="lblTabTitle" runat="server" Text="Edit Gallery Category"></asp:Label>
            <div class="success-details" visible="false" id="divSuccess" runat="server">
                <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
                <div class="corners">
                    <span class="success-left-top"></span><span class="success-right-top"></span><span
                        class="success-left-bot"></span><span class="success-right-bot"></span>
                </div>
            </div>
            <div class="error-details" id="divError" visible="false" runat="server">
                <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
                <div class="corners">
                    <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
                </div>
            </div>
            <!-- content -->
            <div class="well">
                <div id="myTabContent" class="tab-content">
                    <p>
                        <label>
                           Display Title</label>
                        <asp:TextBox ID="txtDisplay" runat="server" CssClass="input-xlarge"></asp:TextBox>
                        <label>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                                ControlToValidate="txtDisplay" ErrorMessage="* Required"></asp:RequiredFieldValidator>
                        </label>
                    </p>
                    <p>
                        <label>
                           Display Arabic Title</label>
                        <asp:TextBox ID="txtArDisplay" runat="server" CssClass="input-xlarge"></asp:TextBox>
                        
                    </p>
                    <p>
                        <label>
                            Thumb Image (Width=504; Height=336)</label>
                        <asp:Image ID="imgSmallImage" runat="server" />
                    </p>
                    <p>
                        <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                        <label>
                            <asp:RequiredFieldValidator ID="rfvSmallImage" runat="server" Display="Dynamic" ControlToValidate="fuSmallImage"
                                ErrorMessage="* Required"></asp:RequiredFieldValidator>
                        </label>
                        <asp:HiddenField ID="hdnSmallImage" runat="server" />
                    </p>
                    
                    
                </div>

                <div class="btn-toolbar">
                    <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                        <i class="icon-save"></i>Add New</button>

                    <div class="btn-group">
                    </div>
                </div>
            </div>
            <!-- Eof content -->
            <asp:SqlDataSource ID="SqlDataSourceGallery" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                UpdateCommand="UPDATE [GCategory] SET [DisplayName] = @DisplayName, [ArDisplayName] = @ArDisplayName, [ThumbImage] = @ThumbImage WHERE [CatID] = @CatID">
                
                <UpdateParameters>
                    <asp:ControlParameter ControlID="txtDisplay" Name="DisplayName" PropertyName="Text" Type="String" />
                    <asp:ControlParameter ControlID="hdnSmallImage" Name="ThumbImage" PropertyName="Value" Type="String" />
                    <asp:QueryStringParameter Name="CatID" QueryStringField="cid" Type="Int32" />
                    <asp:ControlParameter ControlID="txtArDisplay" Name="ArDisplayName" PropertyName="Text" />
                </UpdateParameters>
            </asp:SqlDataSource>
           
            
        </div>
    </div>
</asp:Content>

