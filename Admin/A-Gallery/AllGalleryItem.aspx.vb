﻿
Partial Class AllGalleryItem
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        If IsPostBack = False Then
           

            If Request.QueryString("contextKey") = "" Then
                Response.Cookies("GalID").Value = Request.QueryString("galleryId")
                Response.Cookies("GalID").Expires = Date.Now.AddHours(+1)
                If IsDipestGallery() = False Then
                    Response.Redirect("AllGallery.aspx?pgid=" & Request.QueryString("galleryid"))
                End If
            End If
        End If

    End Sub

    Public Function IsDipestGallery() As Boolean
        Dim conn As New Data.SqlClient.SqlConnection(ConfigurationManager.ConnectionStrings("ConnectionString").ToString)
        conn.Open()
        Dim selectString = "SELECT GalleryID  FROM   Gallery Where ParentGalleryID=@PGalleryID"
        Dim cmd As Data.SqlClient.SqlCommand = New Data.SqlClient.SqlCommand(selectString, conn)
        cmd.Parameters.Add("PGalleryID", Data.SqlDbType.Int, 32).Value = Request.QueryString("galleryid")

        Dim reader As Data.SqlClient.SqlDataReader = cmd.ExecuteReader()
        If reader.HasRows Then
            conn.Close()
            Return False
        Else
            conn.Close()
            Return True
        End If
    End Function
    Protected Sub btnAddNew_Click(sender As Object, e As System.EventArgs) Handles btnAddNew.ServerClick
        Response.Redirect("GalleryItem.aspx?galleryId=" & Request.QueryString("galleryId") & "")
    End Sub
    Protected Sub Button1_Click(sender As Object, e As System.EventArgs) Handles Button1.ServerClick
        Response.Redirect(Request.Cookies("backurlAdmin").Value)
    End Sub
    Protected Sub AjaxFileUploadBigImage_UploadComplete(sender As Object, e As AjaxControlToolkit.AjaxFileUploadEventArgs) Handles AjaxFileUploadBigImage.UploadComplete

        Dim filePath As String = "~/Admin/Content/" & e.FileName
        lblError.Text &= filePath & "<br />"
        ' Save upload file to the file system
        AjaxFileUploadBigImage.SaveAs(Server.MapPath(filePath))
        e.DeleteTemporaryData()
        hdnImageName.Value = "Content/" & e.FileName
        hdnTitle.Value = e.FileName.Remove(e.FileName.LastIndexOf("."))
        hdnLastUpdated.Value = DateTime.Now

        If SqlDataSourceGalleryItem.Insert() > 0 Then
            ListView1.DataBind()
        End If

    End Sub

    Protected Sub AjaxFileUploadBigImage_UploadCompleteAll(sender As Object, e As AjaxControlToolkit.AjaxFileUploadCompleteAllEventArgs) Handles AjaxFileUploadBigImage.UploadCompleteAll
        Response.Cookies("GalID").Value = ""
        Response.Cookies("GalID").Expires = Date.Now.AddDays(-1)
        ListView1.DataBind()
    End Sub
End Class
