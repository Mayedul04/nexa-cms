﻿<%@ Page Title="" Language="VB" MasterPageFile="~/Admin/MasterPage/MainEdit.master"
    AutoEventWireup="false" CodeFile="GalleryItemEdit.aspx.vb" Inherits="GalleryItemEdit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <h1 class="page-title">Gallery Item</h1>
    <div class="btn-toolbar">

        <a href='<%= "AllGalleryItemEdit.aspx?galleryId=" & Request.QueryString("galleryId")  %>&BigImageWidth=<%=Request.QueryString("SmallImageWidth")%>&BigImageHeight=<%=Request.QueryString("SmallImageHeight")%>'
            data-toggle="modal" class="btn btn-primary">Back</a>
        <div class="btn-group">
        </div>
    </div>
    <h2>
        <asp:Label ID="lblTabTitle" runat="server" Text="Add Gallery Item"></asp:Label></h2>
    <div class="success-details" visible="false" id="divSuccess" runat="server">
        <asp:Label ID="lblSuccMessage" runat="server" Text="Operation is done"></asp:Label>
        <div class="corners">
            <span class="success-left-top"></span><span class="success-right-top"></span><span
                class="success-left-bot"></span><span class="success-right-bot"></span>
        </div>
    </div>
    <div class="error-details" id="divError" visible="false" runat="server">
        <asp:Label ID="lblErrMessage" runat="server" Text="There is an error, Please try again later"></asp:Label>
        <div class="corners">
            <span class="error-left-top"></span><span class="error-right-top"></span><span class="error-left-bot"></span><span class="error-right-bot"></span>
        </div>
    </div>
    <!-- content -->
    <div class="well">
        <div id="myTabContent" class="tab-content">
            <p>
                <label>
                    Gallery Item Type
                </label>
                <asp:DropDownList ID="ddlType" AutoPostBack="true" runat="server">
                    <asp:ListItem>Image</asp:ListItem>
                    <asp:ListItem>Uploaded Video</asp:ListItem>
                    <asp:ListItem>Online Video</asp:ListItem>
                </asp:DropDownList>
            </p>
            <p>
                <label>
                    Title</label>
                <asp:TextBox ID="txtTitle" runat="server" CssClass="input-xlarge"></asp:TextBox>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ControlToValidate="txtTitle"
                    validationgroup="form"    ErrorMessage="* Required"></asp:RequiredFieldValidator>
            </p>
            <p>
                <label>
                   Arabic Title</label>
                <asp:TextBox ID="txtArTitle" runat="server" CssClass="input-xlarge"></asp:TextBox>
                 
            </p>
            <p style="display:none;">
                <label>
                    Small Details</label>
                <asp:TextBox ID="txtSmallDetails" runat="server" Height="100" Width="550" TextMode="MultiLine"
                    CssClass="input-xlarge"></asp:TextBox>
            </p>

            <p>
                <label>
                    Small Image (Width=504; Height=336)</label>
                <asp:Image ID="imgSmallImage" runat="server" />
            </p>
            <p>
                <asp:FileUpload ID="fuSmallImage" runat="server" CssClass="input-xlarge" />
                <label>
                    <asp:RequiredFieldValidator ID="rfvSmallImage" runat="server"  Display="Dynamic" ControlToValidate="fuSmallImage"
                        ErrorMessage="* Required"></asp:RequiredFieldValidator>
                </label>
                <asp:HiddenField ID="hdnSmallImage" runat="server" />
            </p>
            <asp:Panel ID="pnlImage" runat="server">
                <p>
                    <label>
                        Big Image (Width=793; Height=459)</label>
                    <asp:Image ID="imgBigImage" Visible="false" runat="server" />
                </p>
                <p>
                    <asp:FileUpload ID="fuBigImage" runat="server" CssClass="input-xlarge" />
                    <label>
                        <asp:RequiredFieldValidator ID="rfvBigImage" runat="server" validationgroup="form" Display="Dynamic" ControlToValidate="fuBigImage"
                            ErrorMessage="* Required"></asp:RequiredFieldValidator>
                    </label>
                    <asp:HiddenField ID="hdnBigImage" runat="server" />
                </p>
            </asp:Panel>
            <asp:Panel ID="pnlVideo" Visible="false" runat="server">
                <p>
                    <asp:Image ID="imgVideoImageURL" runat="server" Width="100px" />
                    <label>
                        Video:(Vimeo/Youtube)(Width=<%= videoWidth%>; Height=<%= videoHeight%>)
                    </label>
                    <asp:TextBox ID="txtVideoOriginalURL" runat="server" CssClass="txt"></asp:TextBox>

                    <asp:HiddenField ID="hdnVideoEmbedCode" runat="server" />
                    <asp:HiddenField ID="hdnVideoImageURL" runat="server" />
                    <asp:HiddenField ID="hdnVideoVCode" runat="server" />
                    <label class="red">
                        <asp:RegularExpressionValidator Display="Dynamic" ID="RegularExpressionValidator2"
                            ValidationGroup="form" ControlToValidate="txtVideoOriginalURL"
                            SetFocusOnError="true" runat="server" ErrorMessage="* Invalid URL"
                            ValidationExpression="http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&amp;=]*)?"></asp:RegularExpressionValidator>
                    </label>

                </p>
            </asp:Panel>
            
            <asp:Panel ID="pnlUploadVideo" Visible="false"  runat="server">
                 <label>
                    Upload Video (mp4 format only)</label>
                <asp:FileUpload ID="fuVideo" runat="server" CssClass="input-xlarge" />
                <asp:Label ID="lblVideolink" runat="server" Text=""></asp:Label>
                    <asp:HiddenField ID="hdnVideoFile" runat="server" />
            </asp:Panel>
            <p style="display:none;">
                <label>
                    Image Alt Text</label>
                <asp:TextBox ID="txtImgAlt" runat="server" CssClass="input-xlarge"></asp:TextBox>
            </p>
            <p>
                <label>
                    Sort Order:
                </label>
                <asp:TextBox ID="txtSortIndex" CssClass="input-xlarge" runat="server"></asp:TextBox>
                <label class="red">
                    <asp:RangeValidator ID="RangeValidator1" ControlToValidate="txtSortIndex" validationgroup="form" Display="Dynamic"
                        SetFocusOnError="true" MinimumValue="1" MaximumValue="999999" runat="server"
                        ErrorMessage="* range from 1 to 999999"></asp:RangeValidator>
                </label>
            </p>
             <p>
                <label>
                    Featured:</label>
                <asp:CheckBox ID="chkFeatured" CssClass="chkbox" runat="server"  />
            </p>
            <p>
                <label>
                    Status:</label>
                <asp:CheckBox ID="chkStatus" CssClass="chkbox" runat="server" Checked="true" />
            </p>
            <p>
                <label>
                    Last Updated :</label>
                <asp:Label ID="lbLastUpdated" runat="server"></asp:Label>
            </p>
        </div>

        <div class="btn-toolbar">
            <button runat="server" id="btnSubmit" validationgroup="form" class="btn btn-primary">
                <i class="icon-save"></i>Add New</button>

            <div class="btn-group">
            </div>
        </div>
    </div>
    <!-- Eof content -->
    <asp:SqlDataSource ID="SqlDataSourceGalleryItem" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
        DeleteCommand="DELETE FROM [GalleryItem] WHERE [GalleryItemID] = @GalleryItemID"
        InsertCommand="INSERT INTO [GalleryItem] ([GalleryID],[ItemType], [Title], [ArTitle], [SmallDetails], [SmallImage], [BigImage], [ImageAltText], [SortIndex], [Status],  [LastUpdated], [VideoOriginalURL], [VideoImageURL], [VideoEmbedCode],[VideoVCode],[Featured]) VALUES (@GalleryID, @ItemType, @Title,@ArTitle, @SmallDetails, @SmallImage, @BigImage, @ImageAltText, @SortIndex, @Status, @LastUpdated, @VideoOriginalURL, @VideoImageURL, @VideoEmbedCode,@VideoVCode,@Featured)"
        SelectCommand="SELECT * FROM [GalleryItem]" UpdateCommand="UPDATE [GalleryItem] SET [Title] = @Title, [ArTitle]=@ArTitle, [SmallDetails] = @SmallDetails, [SmallImage] = @SmallImage, [BigImage] = @BigImage, [ImageAltText]=@ImageAltText, [SortIndex] = @SortIndex, [Status] = @Status, ItemType=@ItemType,  [LastUpdated] = @LastUpdated, [VideoOriginalURL]=@VideoOriginalURL, [VideoImageURL]=@VideoImageURL, [VideoEmbedCode]=@VideoEmbedCode,[VideoVCode]=@VideoVCode,[Featured]=@Featured WHERE [GalleryItemID] = @GalleryItemID">
        <DeleteParameters>
            <asp:Parameter Name="GalleryItemID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:QueryStringParameter Name="GalleryID" QueryStringField="galleryId" Type="Int32" />
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />
            <asp:ControlParameter ControlID="ddlType" Name="ItemType" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="txtVideoOriginalURL" Name="VideoOriginalURL" PropertyName="Text" />
            <asp:ControlParameter ControlID="hdnVideoImageURL" Name="VideoImageURL" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnVideoEmbedCode" Name="VideoEmbedCode" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnVideoVCode" Name="VideoVCode" PropertyName="Value" />
            <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked" />
            <asp:ControlParameter ControlID="txtArTitle" Name="ArTitle" PropertyName="Text" />
        </InsertParameters>
        <UpdateParameters>
            <asp:ControlParameter ControlID="txtTitle" Name="Title" PropertyName="Text" Type="String" />
            <asp:ControlParameter ControlID="txtSmallDetails" Name="SmallDetails" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="hdnSmallImage" Name="SmallImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="hdnBigImage" Name="BigImage" PropertyName="Value"
                Type="String" />
            <asp:ControlParameter ControlID="txtImgAlt" Name="ImageAltText" PropertyName="Text"
                Type="String" />
            <asp:ControlParameter ControlID="txtSortIndex" Name="SortIndex" PropertyName="Text"
                Type="Int32" />
            <asp:ControlParameter ControlID="chkStatus" Name="Status" PropertyName="Checked"
                Type="Boolean" />
            <asp:ControlParameter ControlID="lbLastUpdated" Name="LastUpdated" PropertyName="Text"
                Type="DateTime" />
             <asp:ControlParameter ControlID="ddlType" Name="ItemType" PropertyName="SelectedValue" />
            <asp:ControlParameter ControlID="txtVideoOriginalURL" Name="VideoOriginalURL" PropertyName="Text" />
            <asp:ControlParameter ControlID="hdnVideoImageURL" Name="VideoImageURL" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnVideoEmbedCode" Name="VideoEmbedCode" PropertyName="Value" />
            <asp:ControlParameter ControlID="hdnVideoVCode" Name="VideoVCode" PropertyName="Value" />
            <asp:ControlParameter ControlID="chkFeatured" Name="Featured" PropertyName="Checked" />
            <asp:ControlParameter ControlID="txtArTitle" Name="ArTitle" PropertyName="Text" />
            <asp:QueryStringParameter Name="GalleryItemID" QueryStringField="galleryItemId" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>
