﻿
Partial Class Admin_MasterPage_MainEdit
    Inherits System.Web.UI.MasterPage

    Protected Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        If Not IsPostBack Then
            If Not Utility.isLoggedIn(Request) Then
                Response.Cookies("userName").Value = ""
                Response.Cookies("userName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userFullName").Value = ""
                Response.Cookies("userFullName").Expires = Date.Now.AddDays(-2)
                Response.Cookies("userpass").Value = ""
                Response.Cookies("userpass").Expires = Date.Now.AddDays(-2)

                If Request.Url.AbsolutePath.ToLower().Contains("default.aspx") Then
                    Response.Redirect("A-Login/login.aspx")
                End If
                Response.Redirect("../A-Login/login.aspx")
            End If
            'ltUser.Text = Request.Cookies("userFullName").Value
        End If
    End Sub


End Class

