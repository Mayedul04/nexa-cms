﻿Imports Microsoft.VisualBasic

Public Class NewWindow
    Public Shared Sub Opennewwindow(ByVal Opener As System.Web.UI.WebControls.WebControl, ByVal PagePath As String)

        Dim Clientscript As String

        Clientscript = "window.open('" & PagePath & "')"

        Opener.Attributes.Add("Onclick", Clientscript)

    End Sub
End Class
